<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inicio de sesión</title>

    <!-- Global stylesheets -->
    <link href="{{ asset('/assets/fonts/font.css') }}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/core.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('/assets/css/colors.css')}}" rel="stylesheet" type="text/css">


    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{ asset('/assets/js/core/libraries/jquery-ui.js') }}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{asset('/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('/assets/js/pages/login.js')}}"></script>


    <!-- /theme JS files -->

</head>



<body class="login-container login-cover">


<div class="page-container">


    <div class="page-content">

        <div class="content-wrapper">

            <div class="content pb-20">
                @if($errors->any())
                    @foreach($errors->all() as $error)
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="alert alert-danger alert-styled-left alert-arrow-left alert-bordered">
                                    <button type="button" class="close" data-dismiss="alert"><span>×</span><span
                                                class="sr-only">Close</span>
                                    </button>
                                    <div class="text-left">
                                        <span class="text-semibold">{{$error}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach

                @endif
                <form class="form-horizontal" id="login" role="form" method="POST" action="{{ url('/login') }}">
                    {{ csrf_field() }}

                    <div class="panel panel-body login-form">
                        <div class="text-center">

                            <h5 class="content-group" style="color: deeppink"><img
                                        src="{{asset('/assets/img/logo_semovi.png')}}"
                                        alt="SEMOVI" width="250px"><br><br>Cs</h5>
                        </div>
                        <br>
                        <div class="form-group has-feedback has-feedback-left ">
                            <input id="email" type="text" class="form-control alfa_Numerico" name="rfc"
                                   placeholder="RFC"
                                   autofocus autocomplete="false" maxlength="13">
                            @if ($errors->has('email'))
                                <span class="text-danger">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                            @endif

                            <div class="form-control-feedback">
                                <i class="icon-user text-muted"></i>
                            </div>
                        </div>

                        <div class="form-group has-feedback has-feedback-left ">
                            <input id="password" type="password" class="form-control" name="password"
                                   placeholder="Contraseña">

                            @if ($errors->has('password'))
                                <span class="text-danger">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                            @endif

                            <div class="form-control-feedback">
                                <i class="icon-lock2 text-muted"></i>
                            </div>
                        </div>



                        <div class="form-group">
                            <br>

                            <button type="submit" class="btn bg-pink-400 btn-block">Entrar <i
                                        class="icon-circle-right2 position-right"></i></button>
                        </div>

                    </div>
                </form>


            </div>


        </div>


    </div>

</div>

<script>


</script>
</body>


</html>
