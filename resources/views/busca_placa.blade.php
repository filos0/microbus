@extends('layouts.app')

@section('content')



    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {

                    $('[ name = qr_dif]').focus();
                });
            </script>
        @endforeach

    @endif


    <div class="panel panel-flat">
        <div class="panel-heading text-left">
            <h2>
                <b>Impresion tarjeta de circulacion</b><br>
            </h2>
            <label class="text-pink" style="font-size: 18px">Buscar vehiculo por placa o concesion</label>
            <br>
            <label style="font-size: 18px"></label>
        </div>
        <div class="panel-body">
            <form action="{{url('/buscar/placa')}}" id="validacion" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <br>
                <div class="row ">
                    <div class="col-md-12">
                        <div class="col-md-4 col-md-offset-4 text-center">

                            <h2>Inserte Placa <br></h2>
                            <input type="text" class="form-control folio_text text-center  input-xlg"
                                   maxlength="9" name="placa" autofocus required >

                        </div>

                    </div>
                </div>
                <br>
                <br>
                <div class="row">
                    <div class="text-center">
                        <button class="btn bg-pink btn-xlg">
                            Buscar<i style="margin-left: 10px" class="icon "></i>
                        </button>
                    </div>
                </div>


            </form>

        </div>


    </div>





@endsection
