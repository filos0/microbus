@extends('layouts.app')

@section('content')


    @if($resultado == 'Linea de captura valida')
        <iframe id="printf_vale" name="printf_vale" src="{{url('/'.$pdf_holograma)}}" onfocus="window.close()" hidden></iframe>

        <div class="panel panel-flat">


            <div class="panel-heading text-left">
                <h2>

                    @if($pdf_holograma == 'no')
                        <b>Fin de {{$imp_de}}</b><br>

                    @else
                    <b>Impresion de {{$imp_de}}</b><br>
                        @endif
                </h2>
            </div>
            <div class="panel-body">
                <div class="col-md-12">

                    <div class="col-md-5 col-md-offset-1 ">

                        <div class="table-responsive">
                            <table class="table table-borderless" style="width: 100%">
                                <tbody style="font-size: 20px">
                                <tr>

                                    @if($pdf_holograma == 'no')
                                        <td style="color: #009688"><b>Imprime tarjeta de circulacion</b></td>

                                    @else
                                        <td style="color: #009688"><b>Imprime {{$imp_de}}</b></td>
                                    @endif
                                </tr>
                                <tr>
                                    <td><b>Placa</b></td>
                                    <td>{{$placa}}</td>
                                </tr>

                                <tr>
                                    <td><b>Fecha</b></td>
                                    <td>@php echo date('d-m-Y');@endphp</td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>


                    <div class="col-md-3">


                        <div class="text-center" style="font-size: 22px">
                            <br>
                            <b>Imprimir</b>


                        </div>


                        <div class="text-center">
                            <br> <br>

                            @if($pdf_holograma == 'no')
                                <a href="{{url('/buscar')}}">
                                    <button type="button" class="btn bg-pink btn-xlg">
                                       Ir a Imprimir TC
                                    </button>
                                </a>
                            @else
                            <button id="btn_imprimir" onclick="imprimir_constancia()" style="font-size: 15px" type="button"
                                    class="btn btn-xlg bg-teal">
                                IMPRIMIR <i style="margin-left: 10px" class="icon icon-printer2"></i>
                            </button>
                            @endif
                            <br> <br>

                        </div>


                    </div>
                </div>
                <br>
                <br>
                <div class="text-center">
                    <button id="btn_salir" onclick="window.location.replace('{{url('/')}}')"
                            style="font-size: 15px" type="button"
                            class="btn btn-xlg bg-pink">
                        SALIR <i style="margin-left: 10px" class="icon icon-home"></i>
                    </button>
                </div>
            </div>


        </div>

        <div id="modal_impresion" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">

                    <input type="hidden" name="placa" value="{{$placa}}">
                    <input type="hidden" name="tramite" value="{{$tramite}}">

                    <div class="modal-header bg-pink text-center">

                        <h2 class="modal-title">Impresión</h2>
                    </div>

                    <div class="modal-body">
                        <h3>¿Se imprimió correctamente?</h3>

                        <div class="row col-md-12 text-center">
                            <div class="col-md-2 col-md-offset-2">
                                <span style="font-size: 35px">SI</span>
                            </div>
                            <div class="col-md-2">
                                <input type="radio" style="width: 3em;height: 3em;" name="imp_estatus" value="SI">
                            </div>
                            <div class="col-md-2">
                                <span style="font-size: 35px">NO</span>
                            </div>
                            <div class="col-md-2">
                                <input type="radio" style="width: 3em;height: 3em;" name="imp_estatus" value="NO">
                            </div>

                        </div>
                        <br>
                        <br>

                        <div class="row col-md-12">
                            <h3>Ingresa el folio de la tarjeta</h3>
                            <div class="col-md-8 col-md-offset-2">
                                <input type="text" style="font-size: 35px" name="folio_holograma" maxlength="7"
                                       class="form-control text-center input-xlg alfa_Numerico">
                            </div>
                        </div>
                    </div>


                    <div class="modal-footer">
                        <button type="button" id="btn_modal_aceptar" class="btn btn-xlg bg-pink"
                                onclick="validar_impresion()">Aceptar
                        </button>
                    </div>

                </div>
            </div>
        </div>
        <script type="text/javascript">
            $('[id= btn_salir]').hide();


            function imprimir_constancia() {
                window.frames["printf_vale"].focus();
                window.frames["printf_vale"].print();
                $('#btn_salir').show();
                swal({
                    text: ":)",
                    title: "No olvide la tarjeta de circulacion",
                    type: "",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {

                });
            }


            $('[name = imp_estatus]').on('change', function (x) {
                $('[name = folio_holograma]').val('').prop("disabled", false).focus();
                $('[id= btn_modal_aceptar]').prop("disabled", false);

            });

        /*    function repeatString(n, string) {
                var repeat = [];
                repeat.length = n + 1;
                return repeat.join(string);
            }*/

          /*  $('[name = folio_holograma]').on('change', function (x) {
                var folio = $(this).val().toString();
                var tam_folio = $(this).val().length;
                var dif = 6 - tam_folio;
                var zeros = repeatString(dif, '0');
                $(this).val(zeros + folio)
                $(this).focus();
            });*/


        </script>





    @else


        @if($errors->any())

            @foreach($errors->all() as $error)
                <script>
                    swal({
                        title: "{{$errors->all()[1]}}",
                        text: "{{$errors->all()[2]}}",
                        type: "{{$errors->all()[0]}}",
                        showCancelButton: false,
                        confirmButtonColor: "#ff0005",
                        allowOutsideClick: false,
                        allowEscapeKey: false,
                        confirmButtonText: "Aceptar"
                    }).then(function () {

                    });
                </script>
            @endforeach

        @endif


        <script type="text/javascript">
            $('[id= btn_salir]').hide();

            function imprimir() {
                window.frames["printf"].focus();
                window.frames["printf"].print();
            }

        </script>

    @endif




@endsection
