<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <script type="text/javascript" src="{{url('/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <style type="text/css">

        html {
            margin: 0;
            padding: 0;
            font-family: 'Open Sans', sans-serif;
        }

        .header {
            border: black 0px solid;
            height: 100px;
            width: 100%;
        }

        table, th, td {
            text-align: center;
            border: 0px solid black;
        }

        .div_vale {

            border: red 0px solid;
            height: 420px;
            width: 100%;
        }

        .div_hr {

            height: .5cm;
            width: 100%;
        }


        .text-center {
            text-align: center;
        }

        div.folio {

            border-radius: 25px;
            border: solid 2px black;
            width: 230px;
            height: 100px;
            font-size: 18px;
        }


    </style>

</head>
<body>
<div class="header">

    <div style="width: 50%;display: inline-block;text-align: left">
        <img style="height: 75px;margin-top: 35px;margin-left: 30px;" src="{{asset('/assets/img/semovi')}}" alt="">
    </div>
    <div style="width: 50%;display: inline-block;text-align: right;">
        <img style="height: 50px;margin-top: 25px;margin-right: 30px;" src="{{asset('/assets/img/Logo-cdmx.png')}}"
             alt="">
    </div>

</div>
<div class="div_vale">
    <div style="margin-top: 30px;margin-left: 15%;border: 2px solid black;width:70%;height: 10.4cm">
        <div class="text-center" style="margin-top: 20px">
            <label style="font-size: 20px"><b>Vale de Tarjeta de Circulación</b></label>
        </div>
        <div class="text-center" style="margin-top: 20px">
            <table style="width: 100%">
                <tr>
                    <td style="width: 50%">
                        <label><b>Módulo:</b> {{$modulo}}</label>
                    </td>
                    <td style="width: 50%">
                        <label><b>Fecha:</b> {{date('d-m-Y')}}</label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="text-center" style="margin-top: 20px">
            <table style="width: 100%;margin-left: 25px">
                <tr>
                    <td style="width: 50%">
                        <div class="folio">
                            <br>
                            <label><b>Placa Actual</b></label>
                            <br>
                            <br>
                            <label style="text-decoration: underline"><b>{{$placa}}</b></label>
                        </div>
                    </td>
                    <td style="width: 50%">
                        <div class="folio">
                            <br>
                            <label><b>Folio Físico</b></label>
                            <br>
                            <br>
                            <label><b>___________</b></label>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="text-center" style="margin-top: 20px">
            <table style="width: 100%">
                <tr>
                    <td style="width: 50%">
                        <label><b>Serie Vehicular</b></label>
                        <br>
                        <label>{{$serie_vehicular}}</label>
                    </td>
                    <td style="width: 50%">
                        <label><b>Folio Trámite</b></label>
                        <br>
                        <label>{{$folio_tramite}}</label>
                    </td>
                </tr>
            </table>
        </div>

        <div class="text-center" style="margin-top: 20px">
            <table style="width: 100%">
                <tr>
                    <td style="width: 50%">
                        <label></label>
                        <br>
                        <label><b>_________________</b></label>
                        <br>
                        <label><b>Firma Almacén</b></label>
                    </td>
                    <td style="width: 50%">
                        <label>{{$operador}}</label>
                        <br>
                        <label><b>_________________</b></label>
                        <br>
                        <label><b>Nombre del Revisor</b></label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<div class="div_hr">
    <div style="width: 100%">
        <hr style="border: 1px dotted red;"/>
    </div>
</div>
<div class="div_vale">
    <div style="margin-top: 30px;margin-left: 15%;border: 2px solid black;width:70%;height: 10.4cm">
        <div class="text-center" style="margin-top: 20px">
            <label style="font-size: 20px"><b>Acuse de Impresión y Entrega de Tarjeta de Circulación</b></label>
        </div>
        <div class="text-center" style="margin-top: 20px">
            <table style="width: 100%">
                <tr>
                    <td style="width: 50%">
                        <label><b>Módulo:</b> {{$modulo}}</label>
                    </td>
                    <td style="width: 50%">
                        <label><b>Fecha:</b> {{date('d-m-Y')}}</label>
                    </td>
                </tr>
            </table>
        </div>
        <div class="text-center" style="margin-top: 20px">
            <table style="width: 100%;margin-left: 25px">
                <tr>
                    <td style="width: 50%">
                        <div class="folio">
                            <br>
                            <label><b>Placa Actual</b></label>
                            <br>
                            <br>
                            <label style="text-decoration: underline"><b>{{$placa}}</b></label>
                        </div>
                    </td>
                    <td style="width: 50%">
                        <div class="folio">
                            <br>
                            <label><b>Folio Físico</b></label>
                            <br>
                            <br>
                            <label><b>___________</b></label>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="text-center" style="margin-top: 20px">
            <table style="width: 100%">
                <tr>
                    <td style="width: 50%">
                        <label><b>Serie Vehicular</b></label>
                        <br>
                        <label>{{$serie_vehicular}}</label>
                    </td>
                    <td style="width: 50%">
                        <label><b>Folio Trámite</b></label>
                        <br>
                        <label>{{$folio_tramite}}</label>
                    </td>
                </tr>
            </table>
        </div>

        <div class="text-center" style="margin-top: 20px">
            <table style="width: 100%">
                <tr>
                    <td style="width: 50%">
                        <label></label>
                        <br>
                        <label><b>_________________</b></label>
                        <br>
                        <label><b>Nombre y Firma quien Recibe</b></label>
                    </td>
                    <td style="width: 50%">
                        <label>{{$operador}}</label>
                        <br>
                        <label><b>_________________</b></label>
                        <br>
                        <label><b>Revisor que entrega</b></label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
</body>
</html>