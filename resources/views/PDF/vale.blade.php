<html>
<head>
    <html>
    <head>
        <style>
            .text-center{
                text-align: center;
            }
            .negrita{
                font-weight: bold;
            }
            body{
                font-family: sans-serif;
            }
            @page {
                margin: 180px 50px;
            }
            header { position: fixed;
                left: 0px;
                top: -160px;
                right: 0px;
                height: 100px;
                background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAlYAAABLCAIAAAA9EF4kAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAI+ZJREFUeNrsXQtYVNXaxktCmoKmglYMFwXKFC8knpMKlkQXlex4LRVJBet0Eq3fyylD7aJ2Mbt4Cq2U0k7hDW/npGiBdv68cRTNBLww4O8FbwwKBWb5v+wvV6u9Z/bsmdkzDON6H5559qy91rfW2nvY736/tb61Gly7ds1LQEBAQEDgxkNDcQkEBAQEBAQFCggICAgICAoUEBAQEBAQFCggICAgICAoUEBAQEBAQFCggICAgICAoEABAQEBAQFBgQICAgICAvUGjcUlEBBwH1RXV58pK6ur2gP8/X18fMRdEBAUKCAg4FLk5x/YuWt3WdnZum1GeHhYdPQ9QQaDuCMCNwIaiAXSBATqFiZTxZeZK+uc/GREmDBogFCEAoICf0NOTk5ubk5hYdGRI0V10tDu3buHhYU//vjIdu3aa8lfWVVVVnYuNCTI8ar379+flZWFT5PJlJuby9J9fX27du3q5+cXKwHHtlrGVbW1CNWoyyU1Go3Lli2zdDYoKGjs2LEOXjRLZ+mKKZjAhFIqNtFxWy8y3TUdDTKUl5tattThRpw5U5bx6fKamhocGwyB0dE9I8LD6uRfrLq6Gv/g+/MPlJSU4qu/f9vEMaMECwrc6BSYl5c3e/as06dPu0mLR4wYmZyc3Lx5c2dTIBhi4cKFeI6XlJRoyW8wGB599NHU1FSQh9ar36CBHQ0j6kVd9vEuT8D9+vVTyfDNN98oiUo7Vefn51s6m5aWNmvWLLPUyL9kKDsOStN+eZ3awWPHjY6/YIF1wH/Qf97e3g/Gx0VGdnGHf7Fdu3Zv3rKVKBksKJ6SAjcuBW7cuGH27Nnsazt//6ZNm9ZJQ0+eOl1dU03HHTuGpaenq7OgIxQI3QAmy8jIsK+piYmJ4E4tWs0+CuQRGRmJpton16wyBEgd7wF2WEb3J0+erJLBEgWiOnBnRUWFpYIxMTEapTNuIkypvL5MmjQJ7bT7yutCgbm5O3K378DBmDFPuNXwG2PBhEED3ISYBQScgmuWgXfkqKge9Jc0ZkzB4cPX6hRLPvig9733UntGjhypnvlyZeXRY8V21LJ27VqoDUt8g0dwQkICnp4xEizlRDrsWK1Lr5uI6pYuXWprT3F/rVoGV9lqtri42NJl0WIW1029rMae4h6pvzqUl5c78mu079clw7z5b86e82rWuvXX3A/LMj5D2z5MX3JNQMBzYVEFXr58edCggZWVlThOGTd+/MSJ7kDYF86fTxqbeFqaNT5hQjKgrwqEnFKKP6g68jqaFXYQLllZWcuWLVP6/azKQaYC8US2qkhoWGu/BLPiBpSMZujoJySA0rTbBHCt1q1bZ5UCzapALRbAr7jm6iLbatf27dvniA9ZFxVYUFiUmbkKB8/+7a9+fr7u9nLs5s0TEHCuCkxPTye9NXXKFLcibYhR0oKxsTGXLl3SSwVCE0DeKX1l2rUCRBVIyCa1wbOXrUrr7bffNihcZxrVp3YVaGvbrGo4LeISV0xdR+JOqRc3qDoV7ZC2zlCBOTnb3VxmoXn4KzYahVYQ8FQ0VhkFxKePt8/UGTPcirPDIyIe6H//+k2boFBzc3MGDBioi1noP155gLqgqGwSCpCJEB8Qc9A3bDQL0hASyqZJHFoAa6kS0EhUx0Qh6h08ePDSpUsdmcypGK/KRS1aDNIYquM1QuGhRnTEUgbcKShviEVLI5EqQ4C4syoC1PVQn3L5zruLVEZGHUdkZOeEQRb/g/z925aVnS0xloowQfuQl5e3dWvtkOrQoUNDQkLEBXFDNLTkBaUpoJGd7761dWt3a/SwESPZL0wXg3hw8/yXmJgIMrPPUQZTKIvnLO/W05f/ZMwNfpWNeyUlJalEO9jXKZXQAgaejB0ELhrugnrHzTYJV4OfwKVUyfpeGWcjIWGAU+3H9O1rNz0LqKO8vHzYsGHz589v2bKl4L96RoFFRUXXXwP93bDREIKQpzg4deqU49agJ9555x2e//CUdCT2DtwJFiSnKFlz6tVAU6F7oPxkLKgeY2cTIESsyjtUx19Gx4FOqfgz0SSzwlRdrYKkHRwCdDEgvwyGQKfxXx8xyOc8rFy5EsyXnZ2tMmVBwE0p0P1xW/t2utiBkuAfmuT/1IWWwIKgJZdpDvRCxoIQUlqkm0ZkZGSoRyPo6Hpl11D96kG4y5oEhlMJRsRLiS5+WlcLwUEDnWHW29s7OvoeJ7U5Ly8vJSWlVatWDRo0CA0NxfHx48fZ2QaWwfMHJBSzAC0FUcXOxsXFmS2OIswzZCkPsHXrVpU2oC5WEQxS4uLFi2V9NGsfTUXLKQOYDxVFRUUhJ19QxaZAPabAiopLM1+alTr5udVr19tnofTE/6EsPl3Zf37cztfXV2VBE/toyZV9kbFgSUmJvoNeKt2BYlPhHrsRGxurHtvAu0M9zAXKvQr4Qq7pbvbB+Dgn+Tnx3MdDH893Ii2QH47BBNqHLUCZ4AlwCbMwffp0WOB51JLwQtVWs2kEamd8RkN6VoGqqeU62hRwKnRbJnvKc8+3b9/+5MmTX/zz878MHqS94M5de3Jyty/+8B+1Rp6fGnjH7S7rvNFo5H13IAznDdq5jAXB4mxcE72zabUadRCnKmkVl9F5E0xgWWWBHmoSxZOov3AgT/29uZBrRp0GWRmtOi/gHXSFB32PHj0yMzNDQkLwoAcrIAXqCiksGzLMmzdPWRzZSB4hw7Rp01q2bImvoA2wC6gxOzubz8xPq8ZZ4l188pZlRcgyS0RriZtZChu345mMGmB2SI+1gbifujB06FBLJK3FpoDrYHae6N69eykiYk7aSyrTSefOf3NpxvKS0hO1kcipU4YMHQ6DT46bYHUeqslUgYIowpoRHByyOXub9pmsw4cOQfOSkyc4EhSRlpbGGmAwGFw/H5f30ellUxZRkJiY6GBQhAzFxfKrqgwmsQqbwhL27dunbg0d4W+lGV+iahBF3QZFLMv4zG3ni1N0PNppx68aJMRSwGT804aO+/fvryx78eJFxkM4ZumMUfBowleUNfv44i1byiODSjZ2StkjSwVlicqeqtsUcKOgCC04UlQ4Y9rzOACZde3WPUBawBpakM+z7NMVlZWVgwY+QvKuouLSm28teOXl2USW+DtRWiq9gm0uKjryQP/7XEn/vGfMrSbKO/aC7wflx1yCkFAmk0mvlbVJbPEjcLzodBK6du0KhlNxcqJJKjNR668LVIb0xR/pspuEeiCE44BuI3cfxBY98edJ0FKWiSTSfywdX+kUlBbMqhvRRVdBn5GjMjk5mVyy+GRcbl8DHLEp4CQ4NBY4YcIEOvjh0KH9+/4LOqwwmVYs/wy0x/LMmfXS+qy148eNJ/4bPyEZ/Ae9CAUJ8vvk4yVHjx7Bqe927n7m6RRX9pxfYwVPSUtBZvURoEAmBCsqKuzYj0IFFCZIx3oFAmpxh/JxJkp3qPqLjo5vAHWI+Pg4x414e3urB0I4DpoAmZeXFxcX16pVq5SUFJWxMSVJ0IHMkQjakwlKFfq0ypE2kTG6Q41Bj9RHGUFp5FOVST1HbAq4NQX2ir4H+q+1FDj4zsIFcXHxEHPFxcdBe2A7ytPvvvtTnnoa6Yd+OHy4oHDVyi+Dg0PwCb5EIo6R547AwF7XJ6cdPHDga4Xv3hngZ76A/zzjKcmEIM/odszxMRgMKuuzsDBB9UBAFdJyULJrBy1u5xm3NchgCHd4H6Ve0T2dHQgBlmIERiNzw4YNCw0NlU2HUU7LnD9/PsvDS0AVxHFALcQusiAEWS2y+ZnqFAg9B0JlBs1yOWsA+kiDoJYkr3abAvWDAquqqs6eOnH+/HkoOQg7Xz+/J8dNABGCBbt16wbOQ55vvt525MgRorovvvjCq3Yw6fe3Hhz3iLpn+IgRLOWSyTQzLe3z5cud3XNeG9m9Y47bgu+RHQGCQUFBKp5hChNUDwRMSEjQl3vIHWorkTuyF4Q7CsEH4iDj7C6O1xrnBUIwgL0yMzP37t2LpzxzCULrgCT4wAZLZW2qaysHKm6rBbPIk8AUbQ8JODAbxsAaQL2z5AW1yaZAPaDA4uLiZ56aeOT48eFDhz4xajSEXYXJNGHCBF9JToHbBg4YMG58Mg6+3pr92rzX8fWHQ4fAkTgLvnxv0Yf4Kyk9sTLzC34I8F7JS/P2Owvnvfra1atXnddzfhug+j4RVJ0C7QtXAMkplzxlyMjIUGE4Jw2/gZVVmmRWOHqSuJf0vS9knP0M6rRACCXwfE9PTz927Bi4kEQhHw/gZW46DOQj82HKJCMYVBm058VNRbl48SKoheadyoINZLVka3AysXZOnz6d6qX2oBnK0A5+XWUqa5bVbLIp4DLYOR2mtLT06YkTz1+8gOPwsLDZc2b16dv3zddf/1OvnnPnv5m3dw+xYPHHtYLv8VGjV69aCbWH408+XoJP8GWLFi0+/sj860+bNq1Pnjq1OmsNjp+fNrVx48bO6DnvwfM8FSgjdSg2O1ZFoVVSLa1RqeICdR73qDeJx6RJkzzvtnpJARL2FfTx8XbBfvRgIDzicQBOIkFGXEgEYFUFgiypOD55rmL8YUljoS4wKHEPGMXSaJxGqDgnaZqP2VPgYHSf5rwoxyztsyngjhRYXVOT9uILxH/SjzIUn1By+Jv50iyaIxocHEJDgzhOShy1as2606dPd+p0F8iPznbrbv2WgwVb+Po+/cxfxX2yAxBMbAd2+5aJIXeo+ua3SujuArWjSVo2n6qngIyLieljR5iga5a6ZtyTkpIyb948MBYNByoJDOnK2HCKBSQlFxcXB1IBt4HSiBdx1lK8nQo7KmsJkWDJDpuiAubmhxUpuh90pTK7FWbNTm9xxKaA21Hgx4sXf3/4MOejOMaOX54za8SI4QsWvH2itJSmehKOHDlC1Djn5dfCIyJWZn55yy23aKlrxecrHoiP79Cxg7hVdYLU1NSsrCxGpVbhgggENEl9Lwgvl6/L43rk5u4oKSnVnj8ysrNrKJBx2EoJMpHEExhNGZUVh/JDcbAFyrIRPqbzyNOoAmIgmV9RWQv4RmVmKSNsGd3iKzEZPi2JNmJWpWPTEZsCToXNY4EXLl5cvXo1n3L+/Hn+a6e77kxLe+nxUaM7dOgo/f7iX5yZ9vXW33waL838+/Chj/WIumfsmCfM2q+qqjp37neDV36+8o/333PqoKCAOmyiNBcMv6EKq/tRQCnquD6qW0p8G5ZM8/b2jn8gzmVtA8GAyfBwZzNTIA3TJWgpTrNpkJmxBXgFjHXs2DGNEYEODq0Rc/Ptl7GXygQWagAErkwLOmJTwL1U4JIPPrxcVcmnHC8urq6p8eEmqgXecft9/WIiwsOyszfTHzsVHBzyxltvq6ygtj0nB7THp+zas+fo0aMRERHibtkEvTggKChIPTKdwakuUILRaNQSiUj7SOi74qtbAZIOwi4//6CWzL2ie7p426P+Eiyd5ddFsgRlbINMLFpNz9YWW6XMxlaoUdIb33Kz9qdJ0N5TmU0Bd1eBkGj78+WrVZ2/eGHbli2yRLBgQWGR0kK/++5XX0F0y+bNocHBfAoYMWv1mnrBFm4FfiKogzsEqUemE1yzCAuITeMWsrStrgf/62oMkMB9sUkyCggICrSICxfLjxUXK9NXrcxU+iqhAt9b9OGLM2tjuWj5UOBEaSmLmleioKDglmbNlFUUFPwAoalvz/m96HTcWs9NwId8eEnB8g4atEpvLnCBLly4UPuopJflbXU9AxB2WgIknL3proBAvYZtjtDd331nNv37w4dXZWaOePxxPvHOiPDmzW+ZnDpZ4sjaiaCTUqe8s3DBB+lLpk99TmkEJPrlihVmn1mmS5dAnD5t2+jYcwgjNqSUk5PjYRPo+cB/m2LpVC6XijvUBS5QvKbYuo4r9CJape/6cG4FyDtDkJUNdV0zC0ZA4IZQgRWW36kXL15cWvqHKWq+vi327P0vGwgMDg4B/+FgxrTnd+7ao7Tw2bKM+Ice2n/QzPDGuXPna/RWgTzneZ7HjO+RXuxuyR3qbi5QHlCNnhodQQjw9y8xllr6E/wnIKAnBf5seWbm5arKicnJZ8vK+MR7orqz4+Li46vWrCO/6JIlS2TFt27eXFV5OWvNmuqaaqVx2QQZ3SkwPz9f5jms10Bf+N0bdNRnZqnOBS5Q9e3g1ccpUdaTbq4MPj4+xpKS3O07lH/V1dXiAScgoCcFquPchfMzpk29wMVIdLrrzri4eFogFBjyWEJoh46TUqd88vESXgjmbNv2r02bOnbsuC03x6zl1q1u1b3nXbt25YcDPUkr8ESFPjo4F0Z20WQLdbrGBaq+HXxOTo7KnoU0O9SD/4fN7nxUuyOEmAUjIKAvBd7dxcpO0wcOHRo9ehSvBVOeehr6j7FgUuKokydP8kIQ/Pf+okVPPfPMK3PnWjLr7ePtjM7zT0bQhmdoBfSCp3Pd90Hk3aEucIGaTCZ1iiUNii6rbG2Rm5vrMftBKuHn5xvdU75q2oMuXA5Uhri4ONnmDLRlEh8qp8zDoFzMhZ0KDQ2VnUIKO8un01b1qJdqxzGZLS8vp8SoqCg+P9LJCG03wZrHN8DsFhOynFr6zoC6KI8sKNAmIwIupcDWbdo0b2ZlVZdz58+PTUw8fOgQfe1/X6zXH3eHyNu7B4wIIbhl69cfvv8++G/uvHnPPzfFrAuUEGwIatasqe6dl+2r55qt75wNfswMEtAZAojRnmtcoCqB8EyDqm9tAUBHet68XwYIPj5Awt+/bWRkF/dpHq2RBrpyMABctvKLpc32wBZgEdqT1uv68txIQXrLli0pYBFl+QVL2UI2Di4uqr3v/KLhStZ30gUUcJQCW7RoHh7W0Wq2cxfOj09O/teGDZJQaEHjfwyDEh7Nyc0dMnT436f+z783f7V4yUdzX3311JkzKga79YjybdHCCa/Pfjzt6R5J5voZ+aABPmzASd5dcoe6wAWak5Ojsh+TTIOqb23h5dGrpkHwxXJuT10213Uc2dfBgtynT58uI61sBdSXgOHZwixzoAriCVqeGwbxybYlwllGcvz6bcyUyhqk+vadrx3HZrlcywUUcCkFtm3TJu7Bh7XkvHKlJm3O7FkzZ3rVbi4/njlCCYF33D5+bGLf3veu27Ax7cUXDv5wSMXUbe3b9+zVy0n7ReC5yY8I4impl1YA/8XGxrrysQs+4MfMnEpR4FpnT6PFBVS/ekoNihQVd2h+fr4Hu0Ojo3tS38PDw9xkImj/6wAJ0TLQsv2S+DwM6hv+meUtXibSbkrgPGIOGMQnY1acZRSrZFOc0mW7QS19l10Hs/tIaLmAAi6lQPBQ5y6RnTSvVbbpq38PePihHw7kHz9+bNWa3+YoHj70/StpaV7Xfn0xbdaTiYk79+5RN/Jg/EPtAgKc1H88Q3klUVFRAd5ynAVhISgoCM/cjIwM17AguDwpKYl9jYyMdEGgglOB66bFBcrjBneHUhS8K5cD1Q62bJhVp58lsPU/SQaxjSb4PR8YPaA6nsxoKyXWAGJB1hLmMtVLAlrtO22o5CWtA0fttEpsjl9AAR0oEAgOChyTNK7JTU005i87d27mrLRnJk68+86waVOn9f7znzuGhjz97CTfli2Txo5R139AVLdusfff7+vbwnmXAJzHz3J0nAXBPbDABuTAgk6NzjYajaiOdxiSh7Be7xYLicnHdcigMg3HqjsUxOmpS8ZA/I0Z84Sfn697No8NwtlXnPETsQXjDJ63mJNQSWYsBXlkW/iyJjmJApV9Z41nu2dYGtfU8QIK6EOBEIKd7r579KjRNpXalbd31OhR277e+sYbb8x55dVF7737t0nPqo//eUmxEENHPN4hNMTZVwHSITExkWfBbt262eE0o+mLUGN8EPfSpUudtPQMyA9SKTg4mB//oyABHQMhXA87XKCysyruUChLD3aHun8svGzXXNnUR7OzLpmSI7bgeUu294KWcTLkYcOBZIQMQhqqbCKob9+pRlSHStk4n0YPp9VthwWcS4GAf9s29z8QP+iRgXaU/f7gwREjhq3ftNFqTgjNCSkT8RNx0iig8rnJs6CX5DQLCgrS6E7EUxsPVuTntQsexGvXrtXdEQrmQ6vAtSA/SEz+VGRkJM7Wa/4joaayEIzVMU6r7lAoZg9eNc3N4chgGz+ZkwhDNoFTi3HkYWwHI0x+6T4X1FLf8ySQBCTqZbN1nH0BBcyIOvuKdewQkvCXv+Bg/aYNocHByoWtIeC8fbxPnjolS0//6LdwwJZ+fk2bNlVmYPyXkjLx3t69neoCVbIgnp78jBIoBki61NTURyWAWpCBL7JfglmvHY3GaWQj8JZVaYKKQLT4tEQPaWlpHqBv1NfC1hiJaHWnX5r3VK99xfUO9NyXTfi0aasgaL6UlBQvaWIkG73jmQPGmZtU5tVkGosagLPz588H+VnazNZ5fWctmS6Bl6fqG+eavYACdUOBwF13RnhJLHjoh+9lLAj+696t+5Ztavt1jX9y3JYtW+6KuCv7661K+pwg8Z+/rutiawEohKZx8hMxQDkZEthTGI9O9V1bwUZ4Cmt/wsKalg35LAH6lTRoff85Wn0V0D7GiZzBf9x1S3bBPXtDQXcD+IZIyxGxRb5QUAjxltkdaIlUUB0/uRRV00xRRnU4SylkinySrum7isMTpyw1Q5cLKKAnBTZu3JhY0BAc3Agv7++/i8TmzW4JD+sY2qHj2uuqCGqv3Nzsg/cWLXpxxozMVavGjBqTt3f3oYICSgebjk+e2LlLF9fzHwEUSAusAGZ5rkKCO7CRwWCANgXXegD5MXGmcm1jYmK0h3lY3emXwkCdHdp4I4PNXYR8IWYCJ8k2wlXObwyRYMkmCEAljJ32lwdboMa4uDjUhRSSeiShcJaME0GycTUtvMLmoPKUbGvfmd81PT2dvxRRUVE4ha5R8IP2CyhQZxTIWPDmm5tWV/80Y+o0o7GkR8+e69es/nJlJq/23ljwlrJsdU31K3PnvvH665s2buzdu++YpHH/2b4d6XEPPRwcHFRX/Md70gAoCfWpia5no8jISFQRK6G+j/kpJbjjLlCZQdw+lfW1wbh43XFzd6jJVJGff8BUYQrw94+O7ll7bPr9LcEQFFhiLPXz842M7FJdXb1LWno3JqYPjpHTWFIqlbrnTFkZsuHAx8cH6ThLy6c5dREZ5dwWPN9ltGE2D7/xugzMF+plwXWJ4rSWCmiD5SSAPHiC4Z2oWiiQaFXGx7b23ZLfFV+JHfkJn1ouoEBdUiCxYMcOIadOnw568KE9O3e+8MIM2tWoU0TEHXcEfpW9Zcf27ffHxMrWv+58Vyc/X9+bmzbDS3qHkJAmTZpsyd7y5tsLTSZTaEhws2bN3EeU0GSWHAk0FMfO4tFJo4NgI/uYz6aBEGfoXac2YJYE5+XXAg+IAsz4dDmUMd4A8vMPVlfXGEuAUoPh920Cc7fv8Pb2BpkVFhbhmCgQpcrKziLbzl279+cfuK9fDE4RU361OTsiIox41DXrqJEDkykwx03RUJ8lMoDAInqDigId0qJoFCbPZ2MuRzbX1AV9Jwmr4sI1OylGxwsooDMFEtq3a4fPvv1i+2+/r1Gjxv4BAd9+ux381+SmJpcrLz8xevTZc+coBBB0WFBU9ONPP3bp3Pmmm27y9u7V1j+gsOBwQsKjt7ZqGXjH7e55mUh1iZ+LgOtB/Bcb0+fMmbLamD/JN0+7QPh4+4D2cID3zoLCogLpGABNgv8SBg0Aw+H4009X/Prrr6BJHHv7+CBzeHj4rl27ndTg7OxsXfJYelPMzMzkv06TICtCi6qom02WoKV52t8Urfbr4sWLZtPBbXX7QiwoUAc0bNjwxbS0qqofly9b2qZ1mytXrvzpT/euXLXyb5Oebd7slpHDRpw4UQq9GNu3747//c/lyqpjx44aAgPDOnYY+cQTV3+56j7iT0DAfQAmg25bt34jHVMiWM2r1v0eSIGA4MjCWhSB58BwJcbazav9/f29rkcKQvNB+RUU1HIk8kSEhzmPAgUEblwKBHx9Wwx8rHam6C+//PLPzz7t27tPoMHQJzb29ttuq/qxas/uPadPngw2BAcFGUaNHduoUSP8Q7oy8kFAoB4Bym9//gFaAnvzlq05uTto8ZeXZv6dMuTm1no+ayltd+0oYNfILjjwD6glv7KysoAAf5KGKIXE/PyD+IuM7CwurICA/hTIQP7Mq1evjhqbBIbzuU5y+HzkkYchEysuXWrWrOmtrVqJGyAgoAJQF1iwpKSU1rsBpdFe8BmfLqcMJPIiJeYLDw+jSS5gRH//thCOoE8UxzGdIo0YHh6unYCpDeJGCAgKtN1048bKsT0kggiF7BMQ8JE2giaasZzHJyV5fH7+AcpPc174VdAMQYExXn1AjfEP9Cfxh6/4TBwzSpr5WYPMNBHUS9pH12SqCJKm0nS1NhEGOWlqm6+gQAFBge4G5Xo0Aq6B0Wj0mDDEugXt0gWaMZaUqKztCREWw+0FqJzDSWWjo3vyX8F5LMVsWatzQYl3verDuqMCAnbD/BqhbLpwcbHRDRv93bff0kFYWLi4ha5EVlZW165dPXWnBRcD0s3fvy0ONm/Odre2VVdX75Tmy4iBQwHPRiNLkVg5OTkXL144e+5cTN++rVu3dqtGz5/7Gi0umpg41pIiufLzz1VVP7Zq6e6LQNJKNLjafn5+AQEBy5YtI4JBIoVhUAYc404hG75KT8/fN1CkxWgovhsZYIHWgEYKub8IFOa/f/9+PN3oorGqCcwIpYPqqDiOafwJB/PmzevVqxcyNG7c+JNPPqE2o8FIR3HYxzEZp+pIMpIdluIZEf3l5SbHf12tW9+an3+wqqrKVGGKCHeX9zncbikY8RKOhw8byv+KBAQ8DdcsYMOG9VFRPfA3fOiQa+6Ef2/cSA0bOHCASrbLlZVHjxVfc2+sXbtWetGOpMkO33zzTUxMTFpaGg7YrWHHlBOQuD+Rzi5dupT/irIwBSO+Evbt28fqQqLBYKBTMFJeXk6WY66DZaZ0mGL2WZP4zAkJCTAIO/gKg1QFlcIpykxtRh7qI1/7tXoOvX5dWevWz57zKv6WZXwGWq3zfhUbjR+mL6Em7d+ff01AwKNhcSxwwICBn3/+zyNHio4VFz/z1MTZL79yqxtowa82bXp17lw6njLlufr+/pGamgq2oMWa8Wk1+p7kIHIOHjyYNq9AChglIyODLUwKjUUL2ZBw5FeCHjt2LFJwCnlQNVv4xmxds2fPhgVkk/kGeFkJO7SkKp9OS8qBI3EWp1AdclIepECGoghSZJZvWCQMqt10DFqwpKT03fcWhYeHBfj7G4ICXa78asrOlBUUFpaVnb3esAGuWTtGQKAOoTYdBo/LlJTkysrKXXv3jhgxYviQIYOHDKkrIvzu229XrFiOllxn6AEesFxLSUkJW/dS+3rNyAktRXv95OfnFxcXE6nwPm2cQjazi0TTKbZyWIMGDUixybgQ3NyvXz/wK7+EJp8Z6agxKSkJGpT3bcIySlEKsTKNIFIKHYjRRBkLgvZycnfU1NQUFhbVLviyvS7bg19XfHxcRHiYuDUCNzQFhoWFpacvJhY0VZjSP/4If+7QaPBfWtosD7j6tMM7cTmYQzZIRgNpSDf8cUoebRmIzESftCUQ5KBsWJeoyGy9vOIkD6dyqWjaPVEm1/jMtEsw2sbG/CgPDkDMlGKS4HV9s0MUpHVWxWpzMkRH96SAh4LCIlwiJsVcCYMh0M/PN8hgEOJPQFDg7yy4fv2GBQve2rhxozs0t127dlOmPOcxD1BQyOTJk40ScnNzaeDNS1qSFOwIrUbyju07v0wCKIdk1uDBg1GEuAeZiRFpyz3aIYHGGhlAt7SXLB6yOCByYuIP/MTPLcJX5S4KfGZiXPAZuWSZrCT3LFpOTUJOlKL5NegINVJ4QZXw8fEB9wj6ERBwJRpoXJv18uXLubk5eEstKipUyXb16tWfa2qaePs0atwIhqt/+snbx7thw4YOtrJ58+ZhYeE9JGgs8lN1dXm5qX27ADe/AVkSwEYgDGII2nqCpmXikw3IMZGHDMhJ0zhZIlELTjGWQh6e0mhAzuu6IxSn8JXffojlp3TecpAEPvNjjz22Zs0a2KEBP5wiFqTG0/gf+T9pAynKQ8OBnrHB4bHjxtCQIPEEERC4IShQQEBA9o51s4gWEBAQFCggICAgIFAf0VBcAgEBAQEBQYECAgICAgKCAgUEBAQEBAQFCggICAgICAoUEBAQEBDwHPy/AAMAFTWB/dKwgtoAAAAASUVORK5CYII=') no-repeat center center fixed;
                text-align: center;

            }

        </style>
<body>
<header></header>
<div id="content">
    <div class="text-center">
        <h2>Vale de salida de almacén</h2>

    </div>
    <br>
    <table border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td class="negrita">Modulo:</td>
            <td>{{$data_vale['modulo'] }}</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="negrita">Fecha:&nbsp;</td>
            <td>{{ $date }}</td>
        </tr>
        </tbody>
    </table>
    <div class="text-center">
        <h2>Placa: {{ $data_vale['matricula'] }}</h2>
    </div>
    <table style="height: 115px;" border="0" width="604" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td class="negrita">Serie Vehicular</td>
            <td>&nbsp;</td>
            <td >&nbsp;</td>
            <td class="negrita">Folio de la T.C.</td>
        </tr>
        <tr>
            <td>&nbsp;{{$data_vale['serie_vehicular'] }}</td>
            <td >&nbsp;</td>
            <td >&nbsp;</td>
            <td >&nbsp;{{ $data_vale['folio_tarjeta'] }}</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>{{$data_vale['revisor'] }}</td>
        </tr>
        <tr>
            <td>_________________________</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>_________________________</td>
        </tr>
        <tr>
            <td class="negrita ">Firma Almacén</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="negrita ">Nombre del revisor</td>
        </tr>
        </tbody>
    </table>
<br>
    <hr style="border: 1px dotted red; width: 700px;" />

    <div id="details">
        <div class="text-center">
            <h2>Acuse de recibo de entrega de placas, tarjeta de circulacion y engomado PERMANENTE</h2>
        </div>

        <br></div>
    <table border="0" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td class="negrita">Nombre:</td>
            <td>{{$data_vale['nombre'] }}</td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td class="negrita">Fecha:&nbsp;</td>
            <td>{{ $date }}</td>
        </tr>
        </tbody>
    </table>
    <div class="text-center">
        <h2>Placa: {{ $data_vale['matricula'] }}</h2>
    </div>
    <table style="height: 115px;" border="0" width="610" cellspacing="0" cellpadding="0">
        <tbody>
        <tr>
            <td class="negrita">Serie Vehicular</td>
            <td>&nbsp;</td>
            <td class="negrita">&nbsp;</td>
            <td class="negrita">Folio de la T.C.</td>
        </tr>
        <tr>
            <td>&nbsp;{{$data_vale['serie_vehicular'] }}</td>
            <td >&nbsp;</td>
            <td >&nbsp;</td>
            <td >&nbsp;{{ $data_vale['folio_tarjeta'] }}</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td >&nbsp;</td>
            <td >&nbsp;</td>
            <td>{{$data_vale['revisor'] }}</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>
            </td>
        </tr>
        <tr>
            <td>_________________________</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>_________________________</td>
        </tr>
        <tr>
            <td class="negrita ">Recibe</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="negrita ">Revisor que entrega</td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>