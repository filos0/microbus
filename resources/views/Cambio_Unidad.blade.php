@extends('layouts.app')

@section('content')

    @if($errors->any())
        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                });
            </script>
        @endforeach
    @endif
    <script type="text/javascript">
        var cp = '{{url("/API/CodigoPostal")}}';
        var tipovehiculo = '{{url("/API/TipoVehiculo")}}';
        var serie_vehicular = '{{url("/SOAP/webServiceFinanzas")}}';
        var serie_vehicular_db = '{{url("/API/ClaveVehicular")}}';
        var buscar_prop = '{{url("/API/buscar_prop")}}';
        var buscar_porqueria = '{{url("/API/buscar_porqueria")}}';
        var actualizar_porqueria = '{{url("/API/actualizar_porqueria")}}';

    </script>
    <div class="panel ">
        <div class="panel-heading bg-pink">
            <h5 class="panel-title text-center ">
                SUSTITUCION DE UNIDAD
            </h5>
        </div>

        <div class="panel-body">
            <form id="form_alta" action="{{url('/cambiar/unidad')}}" method="POST" novalidate>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="id_mega" value="{{$mega->id_megamicro ?? 0}}">
                <input type="hidden" name="serie" value="{{$serie}}">
                <input type="hidden" name="linea_captura" value="{{$linea}}">


                <div id="vista_vehiculo">
                    <ul id="form_alta-header" class="stepy-header">
                        <li id="form_alta-head-0" class="stepy-active" style="cursor: default;">
                            <div>--</div>
                            <span><span class="text-blue-800">DATOS VEHÍCULO</span></span></li>
                        <li id="form_alta-head-1" style="cursor: default;" class="">
                            <div>--</div>
                            <span><p class="text-blue-800">FACTURACIÓN</p></span></li>
                        <li id="form_alta-head-2" style="cursor: default;" class="">
                            <div>--</div>

                            <span><p class="text-blue-800">SOLICITANTE</p></span></li>
                    </ul>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Clave Vehícular</b></label>
                                <input name="clave_vehicular" type="text" maxlength="7"
                                       class="form-control text-center placa_text"
                                       value="" autofocus required>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Marca</b></label>

                                <input name="marca" class="form-control" readonly>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Línea</b></label>

                                <input name="linea" class="form-control" readonly>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Versión</b></label>

                                <input name="version" class="form-control" readonly>

                            </div>
                        </div>
                    </div>
                    <div class="row">


                        <div class="col-md-3">
                            <label><b>Úso de Vehículo</b></label>
                            <div class="form-group">
                                <select name="uso_vehiculo" class="form-control" required>
                                    <option></option>
                                    @foreach($usos as $uso)
                                        <option value="{{$uso->id_uso_vehiculo}}">{{$uso->uso_vehiculo}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label><b>Clase de Vehículo</b></label>
                            <div class="form-group">
                                <select name="clase_vehiculo" class="form-control" required>
                                    <option></option>
                                    @foreach($clases as $clase)
                                        <option value="{{$clase->clave_clase}}">{{$clase->clase}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label><b>Tipo de Vehículo</b></label>
                            <div class="form-group">
                                <select name="clase_tipo_vehiculo_id" class="form-control" required>

                                </select>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <label><b>Año de Vehículo</b></label>
                            <div class="form-group">
                                <select name="ano" class="form-control" required>

                                    <option class="form-control"></option>

                                    @php
                                        $anoactual = date("Y", strtotime('+1 years'));
                                        $anofinal = date("Y", strtotime('-80 years'));

                                    @endphp
                                    @for ($i =  $anoactual; $i > $anofinal; $i--)
                                        <option class="form-control" value="{{$i}}">{{$i}}</option>
                                    @endfor

                                </select>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Combustible</b></label>
                                <select name="combustible" class="form-control" required>
                                    <option value=""></option>
                                    @foreach($combustibles as $combustible)
                                        <option value="{{$combustible->id_tipo_combustible}}">{{$combustible->tipo_combustible}}</option>
                                    @endforeach

                                </select>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Número de Motor</b></label>
                                <input type="text" name="numero_motor" class="form-control text-center placa_text"
                                       maxlength="10" required>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <label><b>País Orígen de Motor</b></label>
                            <div class="form-group">
                                <select name="origen_motor" class="form-control" required>
                                    <option value="SIN DATO">SIN DATO</option>
                                    <option value="NO DEFINIDO">NO DEFINIDO</option>
                                    @foreach($paises as $pais)
                                        <option value="{{$pais->id_pais}}">{{$pais->pais}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Número REPUVE</b></label>
                                <input type="text" name="repuve" class="form-control text-center numeros"
                                       maxlength="8">
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Puertas</b></label>
                                <input type="text" name="puertas" class="form-control numeros text-center" required>

                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Pasajeros</b></label>
                                <input type="text" name="pasajeros" class="form-control numeros text-center"
                                       required>
                            </div>
                        </div>


                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Número de Cilindros</b></label>
                                <input type="text" name="cilindros" class="form-control numeros text-center"
                                       required>

                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label><b>Capacidad de Combustible (Litros)</b></label>

                                <input type="text" name="lit" class="form-control numeros text-center"
                                       required>

                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="btn-group" style="float: right;">
                            <button type="button" class="btn bg-pink " onclick="ir_paso2();">
                                SIGUIENTE <i class="icon icon-arrow-right15"></i>
                            </button>
                        </div>

                    </div>

                </div>
                <div id="vista_facturacion">
                    <ul id="form_alta-header" class="stepy-header">
                        <li id="form_alta-head-0" class="" style="cursor: default;">
                            <div>--</div>
                            <span><span class="text-blue-800">DATOS VEHÍCULO</span></span></li>
                        <li id="form_alta-head-1" style="cursor: default;" class="stepy-active">
                            <div>--</div>
                            <span><p class="text-blue-800">FACTURACIÓN</p></span></li>
                        <li id="form_alta-head-2" style="cursor: default;">
                            <div>--</div>

                            <span><p class="text-blue-800">SOLICITANTE</p></span></li>
                    </ul>
                    <div class="row">
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label><b>Procendencia</b></label>
                                <select name="procedencia" class="form-control" id="procedencia" required>
                                    <option></option>
                                    <option value="1">Nacional</option>
                                    <option value="2">Extranjero</option>

                                </select>
                            </div>
                        </div>

                    </div>
                    <div class="row" id="vehiculo_extranjero" hidden>
                        <div class="col-md-4">
                            <label><b>País</b></label>
                            <div class="form-group">
                                <select name="pais" class="form-control" id="pais">
                                    <option></option>
                                    @foreach($paises as $pais)
                                        <option value="{{ $pais->id_pais }}">{{ $pais->pais }} </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label><b>Fecha Legaliza</b></label>
                            <div class="form-group">
                                <input type="date" class="form-control " name="fecha_legalizacion">
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="form-group">
                                <label><b>Folio Legaliza</b></label>
                                <input type="text" name="folio_legalizacion" class="form-control placa_text"
                                       maxlength="25">
                            </div>
                        </div>
                    </div>
                    <br>
                    <br>

                    <div class="row">
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label><b>Aseguradora</b></label>
                                <input type="text" name="aseguradora" class="form-control" maxlength="20">
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <div class="form-group">
                                <label><b>Poliza</b></label>
                                <input type="text" name="poliza" class="form-control" maxlength="20">
                            </div>
                        </div>
                        <div class="col-md-4 ">
                            <label><b>Distribuidora</b></label>
                            <div class="form-group">
                                <input type="text" name="distribuidora" class="form-control alfaNumerico_espacio"
                                       maxlength="45" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-4 ">
                            <label><b>Fecha de Factura </b></label>
                            <div class="form-group">
                                <input type="date" class="form-control " name="fecha_factura" required>
                            </div>
                        </div>


                        <div class="col-md-4">
                            <label><b> Valor Factura</b></label>
                            <div class="form-group">
                                <input type="text" name="valor_factura" class="form-control text-center" maxlength="13"
                                       placeholder="$$$" required>
                            </div>
                        </div>

                        <div class="col-md-4 ">
                            <label><b>Número de Factura</b></label>
                            <div class="form-group">
                                <input type="text" name="numero_factura" class="form-control" required
                                       maxlength="25">
                            </div>
                        </div>
                    </div>

                    <div class="row " style="float: right;">
                        <div class="btn-group">
                            <button type="button" class="btn bg-teal" onclick="regresar_paso_1()">
                                <i class="icon icon-arrow-left15"></i> ATRAS

                            </button>
                            <button type="button" class="btn bg-pink" onclick="ir_paso3()">
                                SIGUIENTE <i class="icon icon-arrow-right15"></i>
                            </button>


                        </div>

                    </div>

                </div>


                <div id="vista_solicitante">
                    <ul id="form_alta-header" class="stepy-header">
                        <li id="form_alta-head-0" class="" style="cursor: default;">
                            <div>--</div>
                            <span><span class="text-blue-800">DATOS VEHÍCULO</span></span></li>
                        <li id="form_alta-head-1" style="cursor: default;" class="">
                            <div>--</div>
                            <span><p class="text-blue-800">FACTURACIÓN</p></span></li>
                        <li id="form_alta-head-2" style="cursor: default;" class="">
                            <div>--</div>

                            <span><p class="text-blue-800">SOLICITANTE</p></span></li>
                    </ul>
                    <div class="row text-center">
                        <h4><label>Información del Solicitante</label></h4>
                    </div>
                    <div class="row">

                        <div class="col-md-4 ">
                            <label><b>Nombre(s)</b></label>
                            <div class="form-group">
                                <input name="nombre_solicitante" class="form-control soloLetras_espacio" maxlength="18"
                                       required>
                            </div>
                        </div>

                        <div class="col-md-4 ">
                            <label><b>Primer Apellido</b></label>
                            <div class="form-group">
                                <input name="primer_apellido_solicitante" class="form-control soloLetras_espacio"
                                       maxlength="18"
                                       required>
                            </div>
                        </div>

                        <div class="col-md-4 ">
                            <label><b>Segundo Apellido</b></label>
                            <div class="form-group">
                                <input name="segundo_apellido_solicitante" class="form-control soloLetras_espacio"
                                       maxlength="18"
                                       required>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4 ">
                            <label><b>CURP/RFC</b></label>
                            <div class="form-group">
                                <input name="curp_solicitante" class="form-control" maxlength="18"
                                       required>
                            </div>
                        </div>

                        <div class="col-md-4 ">
                            <label><b>Tipo de Identificación </b></label>
                            <div class="form-group">
                                <select id="doc_solicitante" name="doc_solicitante" class="form-control" disabled
                                        required>
                                    <option value=""></option>
                                    @foreach($identificaciones as $identificacion)
                                        <option value="{{$identificacion->id_tipo_identificacion}}">{{$identificacion->tipo_identificacion}}</option>
                                    @endforeach
                                </select>


                            </div>
                        </div>

                        <div class="col-md-4">
                            <label><b>Folio de Identificación</b></label>
                            <div class="form-group">
                                <input type="text" name="folio_doc_solicitante" class="form-control" maxlength="20"
                                       readonly
                                       required>
                            </div>
                        </div>

                    </div>

                    <div class=" row text-center">

                        <br>
                        <button
                                type="button"
                                name="datos"
                                onclick="dd_ajax();"
                                class="btn bg-success-800"
                                disabled

                        ><i style="margin-right:5px" class="icon icon-printer"></i>IMPRIMIR HOJA DE DATOS
                        </button>

                    </div>


                    <div class="row " style="float: right;">
                        <div class="btn-group">
                            <button type="button" class="btn bg-teal" onclick="regresar_paso_3()">
                                <i class="icon icon-arrow-left15"></i> ATRAS

                            </button>
                        </div>

                    </div>
                </div>


            </form>
        </div>

        <div class="panel-footer">
            <div class="text-center">
                <br>
                <button
                        type="button"
                        class="btn bg-indigo btn-block confirmar_salir ">
                    Cancelar Trámite
                </button>
                <br>
                <br>

            </div>
        </div>

    </div>
    <div id="modal_confirmar" class="modal fade" >
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h1 class=""><b>¿Está seguro que desea finalizar?</b></h1>
                </div>

                <div class="modal-body text-center">
                    <label style="font-size: 18px">Los datos ya no podran ser modificados.</label>
                </div>

                <div class="modal-footer text-center">
                    <button type="button" class="btn bg-danger-800" data-dismiss="modal" onclick="cancelar();">
                        Cancelar <i class="icon icon-cross"></i>
                    </button>
                    <button type="button" class="btn bg-teal" onclick="aceptar();">
                        Confirmar <i class="icon icon-check2"></i>
                    </button>


                </div>
            </div>
        </div>
    </div>




    <script type="text/javascript">


        var vista_vehiculo = $('[id = vista_vehiculo]');
        var vista_facturacion = $('[id = vista_facturacion ]');
        var vista_propietario = $('[id = vista_propietario]');
        var vista_solicitante = $('[id = vista_solicitante]');
        vista_vehiculo.show();
        vista_facturacion.hide();
        vista_propietario.hide();
        vista_solicitante.hide();

        function aceptar() {
            $('#form_alta')[0].submit();
        }
        function cancelar() {
            $('[name = datos]').prop("disabled", true);
            $('[name= folio_doc_solicitante]').val('').focus();
        }

        function ir_paso2() {

            vista_vehiculo.hide("fast");
            vista_facturacion.show("fast");
            $('[name =procedencia]').focus();

        }

        function ir_paso3() {


            vista_facturacion.hide("fast");
            vista_solicitante.show("fast");
            $('[name =nombre_solicitante]').focus();


        }

        function ir_paso4() {


            vista_propietario.hide("fast");
            vista_facturacion.hide("fast");
            vista_solicitante.show("fast");


        }

        function regresar_paso_1() {
            vista_facturacion.hide("fast");
            vista_vehiculo.show("fast");
            $('[name =clave_vehicular]').focus();
        }

        function regresar_paso_2() {
            vista_propietario.hide("fast");
            vista_solicitante.hide("fast");
            vista_facturacion.show("fast");
            $('[name =procedencia]').focus();
        }

        function regresar_paso_3() {
            vista_solicitante.hide("fast");
            vista_facturacion.show("fast");
            $('[name =aseguradora]').focus();
        }

        function dd_ajax() {

            var inputs = $("#form_alta").serialize();
            $.ajax({
                url: "{{url('/API/PDF/Confirmar_Datos')}}",
                type: "POST",
                data: {
                    "data": inputs
                },
                beforeSend: function () {
                    $('#modal_large').modal('show');
                },
                complete: function () {
                    $('#modal_large').modal('hide');
                },
                success: function (pdf) {
                    var appBaseUrl = '{{ url('/') }}';
                    var url = appBaseUrl + pdf;

                    printJS({printable: url, type: 'pdf', modalMessage: 'Cargando PDF...'})

                }

            });
            setTimeout(datos_correctos, 3000);

        }

        function datos_correctos() {
            $('#modal_confirmar').modal({
                backdrop: 'static',
                keyboard: false
            });


        }

        $('.confirmar_salir').click(function () {
            swal({
                title: "¿Esta seguro que quiere salir?",
                text: "Se perdera todo el avance",
                type: "error",
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: "#dd0e00",
                confirmButtonText: "Si, Salir!"
                // closeOnConfirm: false
            }).then(function () {
                window.location.href = {!! json_encode(url('/')) !!}
                setInterval(fecha_actual, 1000);
            });

        });

    </script>

@endsection
