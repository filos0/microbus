@extends('layouts.app')

@section('content')


    @if(!isset($origen))

        <div class="panel panel-flat">
            <div class="panel-heading ">
                <h3 class="panel-title">Búsqueda de datos</h3>
            </div>
            <div class="panel-body">
                @if($errors->any())
                    @foreach($errors->all() as $error)

                        <script>
                            swal({
                                title: "{{$error}}",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#E91E63",
                                confirmButtonText: "ACEPTAR"

                            });
                        </script>

                    @endforeach
                @endif
                <div class="text-center">
                    <h5><label>Número de Placa </label></h5>
                </div>
                <div class="row ">
                    <form action="{{url('/Buscar/Placa')}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="tipo_movimiento" value="Busqueda de datos">
                        <div class="input-group content-group col-md-10">
                            <div class="has-feedback has-feedback-left col-md-offset-3">
                                <input type="text" name="placa"
                                       class="form-control text-center input-xlg text-uppercase alfa_Numerico"
                                       placeholder="0000ZZZ" maxlength="7" required autofocus>
                                <div class="form-control-feedback">
                                    <i class="icon-search4 text-muted text-size-base"></i>
                                </div>
                            </div>
                            <div class="input-group-btn text-center">

                                <button type="submit" class="btn bg-pink btn-xlg">
                                    Buscar
                                </button>

                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>

    @else
        <div class="panel panel-flat">
            <div class="panel-heading ">
                <h3 class="panel-title">Búsqueda de datos</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel ">
                            <div class="panel-heading text-center " style="background-color: #e91e63;color: white;">
                                <h2 class="panel-title">Vehiculo</h2>
                            </div>

                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless table-responsive" style="font-size: 17px">
                                        <tbody>
                                        <tr>
                                            <td><b>Serie Vehicular</b></td>
                                            <td>{{$vehiculo->serie_vehicular}}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><b>Estado del Vehiculo</b></td>
                                            <td>  @if($vehiculo->estatus_id == 1)
                                                    <span style="font-size: 18px" class="label  label-success">Activo</span>
                                                @elseif($vehiculo->estatus_id == 2)
                                                    <span style="font-size: 18px" class="label label-danger">Baja</span>
                                                @else
                                                    -
                                                @endif</td>
                                        </tr>
                                        <tr>
                                            <td><b>Clase</b></td>
                                            <td>{{$vehiculo->clasetipo->clase}}</td>
                                            <td><b>Tipo</b></td>
                                            <td>{{$vehiculo->clasetipo->tipo}}</td>
                                            <td><b>Marca</b></td>
                                            <td>{{$vehiculo->clave_vehicular->marca}}</td>
                                            <td><b>Linea</b></td>
                                            <td>{{$vehiculo->clave_vehicular->linea}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Año</b></td>
                                            <td>{{$vehiculo->modelo}}</td>
                                            <td><b>Uso</b></td>
                                            <td>{{$vehiculo->uso->uso_vehiculo}}</td>
                                            <td><b>Tipo Servicio</b></td>
                                            <td>{{$vehiculo->tiposervicio->tipo_servicio}}</td>
                                            <td><b>Combustible</b></td>
                                            <td>{{$vehiculo->combustible->tipo_combustible}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Número de motor</b></td>
                                            <td>{{$vehiculo->numero_motor}}</td>
                                            <td><b>REPUVE</b></td>
                                            <td>@if($vehiculo->numero_repuve == '')
                                                    -
                                                @else
                                                    {{$vehiculo->numero_repuve}}
                                                @endif
                                            </td>
                                            <td><b>Puertas</b></td>
                                            <td>{{$vehiculo->numero_puertas}}</td>
                                            <td><b>Pasajeros</b></td>
                                            <td>{{$vehiculo->numero_personas}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Cilindros</b></td>
                                            <td>{{$vehiculo->numero_cilindros}}</td>
                                            <td><b>Capacidad de Litros</b></td>
                                            <td>{{$vehiculo->capacidad_litros}}</td>
                                            <td><b>Capacidad de Khw</b></td>
                                            <td>
                                                @if($vehiculo->capacidad == '')
                                                    -
                                                @else
                                                    {{$vehiculo->capacidad}}
                                                @endif</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel">
                            <div class="panel-heading text-center " style="background-color: #e91e63;color: white;">
                                <h2 class="panel-title">Propietario</h2>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless" style="font-size: 15px">
                                        <tbody>
                                        <tr>
                                            <td><b>CURP/RFC</b></td>
                                            <td>{{$propietario->clave_unica}}</td>

                                            <td><b>Tipo Propietario</b></td>
                                            <td>@if($propietario->tipo_propietario == 1)
                                                    Fisca
                                                @elseif($propietario->tipo_propietario == 2)
                                                    Moral
                                                @else
                                                    -
                                                @endif</td>
                                        </tr>
                                        <tr>
                                            <td><b>Nombre</b></td>
                                            <td>{{$propietario->nombre_razon_social}}</td>
                                            <td><b>Apellido Paterno</b></td>
                                            <td>{{$propietario->primer_apellido}}</td>
                                            <td><b>Apellido Materno</b></td>
                                            <td>{{$propietario->segundo_apellido}}</td>

                                        </tr>
                                        <tr>
                                            <td><b>Nacionalidad</b></td>
                                            <td>{{$propietario->pais->nacionalidad}}</td>
                                            <td><b>Fecha Nacimiento</b></td>
                                            <td>{{$propietario->fecha_nacimiento}}</td>
                                            <td><b>Sexo</b></td>
                                            <td>
                                                @if($propietario->sexo == 'F')
                                                    Femenino
                                                @elseif($propietario->sexo == 'M')
                                                    Masculino
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel">
                            <div class="panel-heading text-center " style="background-color: #e91e63;color: white;">
                                <h2 class="panel-title">Domicilio</h2>
                            </div>

                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless" style="font-size: 15px">
                                        <tbody>
                                        <tr>
                                            <td><b>Entidad</b></td>
                                            <td>Ciudad de Mexico</td>
                                            <td><b>Delegación</b></td>
                                            <td>{{$propietario->colonia->delegacion}}</td>
                                            <td><b>Colonia</b></td>
                                            <td>{{$propietario->colonia->colonia}}</td>

                                        </tr>
                                        <tr>
                                            <td><b>Código Postal</b></td>
                                            <td>{{$propietario->colonia->cp}}</td>
                                            <td><b>Calle</b></td>
                                            <td>
                                                @if($propietario->calle == "")
                                                    Por Actualizar
                                                @else
                                                    {{$propietario->calle}}
                                                @endif</td>
                                            <td><b>Número Exterior</b></td>
                                            <td>
                                                @if($propietario->numero_exterior == "")
                                                    Por Actualizar
                                                @else
                                                    {{$propietario->numero_exterior}}
                                                @endif</td>

                                        </tr>
                                        <tr>
                                            <td><b>Número Interior</b></td>
                                            <td>@if($propietario->numero_interior == "")
                                                    Por Actualizar
                                                @else
                                                    {{$propietario->numero_interior}}
                                                @endif</td>
                                            <td><b>Telefono</b></td>
                                            <td>@if($propietario->telefono == "")
                                                    Por Actualizar
                                                @else
                                                    {{$propietario->telefono}}
                                                @endif</td>


                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel-footer text-center">
                <button type="submit" class="btn bg-teal  btn-xlg" onclick="imprimer()">
                    Imprimir datos
                </button>

                <a href="{{url('/Buscar')}}">
                    <button type="button" class="btn bg-pink btn-xlg">
                        Regresar
                    </button>
                </a>

            </div>
        </div>


        <iframe id="printf" name="printf" src="{{$pdf}}" onfocus="window.close()" hidden></iframe>


        <script>
            function imprimer() {
                window.frames["printf"].focus();
                window.frames["printf"].print();

            }
        </script>

    @endif




@endsection
