<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<title>Control Vehicular de Transporte</title>
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{url('/assets/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('/assets/css/bootstrap.css')}}" rel="stylesheet" type="text/css">

    <link href="{{url('/assets/css/components.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('/assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('/assets/css/sweetalert2.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('/assets/css/printJS.css')}}" rel="stylesheet" type="text/css">
    <link href="{{url('/assets/css/core.css')}}" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="{{url('/assets/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/plugins/loaders/blockui.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/sweetalert2.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/printJS.js')}}"></script>
    <script type="text/javascript" src="{{url('/assets/js/core.js')}}"></script>

</head>

<body>


<div class="navbar navbar-default header-highlight">
    <div class="navbar-header">
        <a style="color: white" class="navbar-brand" href=""><img src="" alt=""></a>

        <ul class="nav navbar-nav visible-xs-block">

            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li>

                <b><a id="hora" class="navbar-brand"></a></b>
            </li>
        </ul>
        <ul class="nav navbar-nav navbar-left">
            <li>

                <a class="navbar-brand" style="padding: 0px;">
                    <img style="  height: 100%;padding: 5px;width: auto;"
                         src="{{asset('/assets/img/logo_semovi.png')}}" alt="">
                </a>

            </li>
        </ul>


    </div>
</div>

<div class="page-container">


    <div class="page-content">


        <div class="sidebar sidebar-main">
            <div class="sidebar-content">


                <div class="sidebar-user" style="cursor: pointer">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img
                                        src="{{asset('/assets/img/angel.jpeg')}}"
                                        class="img-circle img-lg" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold">Usuario Activo</span>
                                <div class="text-size-mini text-muted">
                                    Moódulo Popotla
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">


                            <li><a href="{{url('/')}}"><i class="icon-home4"></i>
                                    <span>Inicio</span></a></li>
                            <li><a href="{{url('/Linea_Captura/Baja')}}"><i class="icon icon-bus"></i>
                                    <span>Baja</span></a></li>
                            <li><a href="{{url('/Linea_Captura/Cambio')}}"><i class="icon icon-bus"></i>
                                    <span>Sustitucion de Unidad</span></a></li>

<!--
                            <li class="">
                                <a href="#" class="has-ul legitRipple"><i class="icon-database-remove"></i>
                                    <span>Baja de Unidad</span></a>
                                <ul class="hidden-ul" style="display: none;">
                                    <li><a href="{{url('/Linea_Captura')}}" id="layout1" class="legitRipple"><i
                                                    class="icon icon-folder-download3"></i>Baja General</a></li>

                                    <li><a href="{{url('/Baja/Robo')}}" id="layout1" class="legitRipple"><i
                                                    class="icon  icon-alert"></i>Baja Robo</a></li>


                                    <li><a href="{{url('/Linea_Captura')}}" id="layout1" class="legitRipple"><i
                                                    class="icon   icon-hammer"></i>Baja Destrucción</a></li>


                                </ul>
                            </li>

                            <li><a href="{{url('/Linea_Captura')}}"><i class="icon icon-users2"></i>
                                    <span>Cesión de Derecho</span></a></li>
-->
                            <li class="">
                                <a href="{{ url('/Linea_Captura') }}" class="legitRipple">
                                    <i class="icon  icon-pencil6 "></i>
                                    <span>Actualización de Datos</span></a>
                            </li>
                            <li class="">
                                <a href="{{ url('/Validar/LineaPago') }}" class="legitRipple">
                                    <i class="icon-credit-card "></i>
                                    <span>Reposición/Renovación TC</span></a>
                            </li>
                            <li class="">
                                <a href="{{ url('/buscar') }}" class="legitRipple"><i
                                            class="icon-printer"></i>
                                    <span>Impresion de Tarjetas</span></a>
                            </li>


                          <!--  <li class="">
                                <a href="{{ url('/modelos') }}" class="legitRipple"><i
                                            class="icon icon-code"></i>
                                    <span>Modelos</span></a>
                            </li>-->

                        </ul>
                    </div>
                </div>


            </div>
        </div>

        <div class="content-wrapper">


            <div class="content">


                @yield('content')

            </div>


        </div>


    </div>


</div>
</body>
</html>

