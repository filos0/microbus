@extends('layouts.app')

@section('content')


    @if(!isset($todo))
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="panel-title"><p><h4>Baja de Automovíl</h4>
                    <p/></div>

                @if( ! empty($linea_pago))
                    <script>
                        swal({
                            title: "La Linea de Captura es valida",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#E91E63",
                            confirmButtonText: "ACEPTAR"

                        }).then(function () {
                            $('[name = serie]').focus();

                        });
                    </script>
                @endif

                @if($errors->any())
                    @foreach($errors->all() as $error)

                        <script>
                            swal({
                                title: "{{$error}}",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#E91E63",
                                confirmButtonText: "ACEPTAR"

                            });
                        </script>

                    @endforeach
                @endif
            </div>

        </div>



    @else
        <div class="panel panel-flat">
            <div class="panel-heading ">
                <h3 class="panel-title">Baja de Vehiculo</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel ">
                            <div class="panel-heading text-center " style="background-color: #e91e63;color: white;">
                                <h2 class="panel-title">Vehiculo</h2>
                            </div>

                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless" style="font-size: 17px">
                                        <tbody>
                                        <tr>
                                            <td><b>Serie Vehicular</b></td>
                                            <td>{{$todo->serie_vehicular}}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><b>Estado del Vehiculo</b></td>
                                            <td>  @if($todo->estatus_id == 1)
                                                    <span style="font-size: 15px"
                                                          class="label  label-success">Activo</span>
                                                @elseif($todo->estatus_id == 2)
                                                    <span style="font-size: 15px" class="label label-danger">Baja</span>
                                                @else
                                                    @if($todo->estatus_vehiculo == 'A')
                                                        <span style="font-size: 15px"
                                                              class="label  label-success">Activo</span>
                                                    @elseif($todo->estatus_vehiculo == 'B')
                                                        <span style="font-size: 15px"
                                                              class="label label-danger">Baja</span>

                                                    @else
                                                        -
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                        <tr>
                                            <td><b>Clase</b></td>
                                            <td>{{$todo->clase}}</td>
                                            <td><b>Tipo</b></td>
                                            <td>{{$todo->tipo}}</td>
                                            <td><b>Marca</b></td>
                                            <td>{{$todo->marca}}</td>
                                            <td><b>Linea</b></td>
                                            <td>{{$todo->linea}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Año</b></td>
                                            <td>{{$todo->modelo}}</td>
                                            <td><b>Uso</b></td>
                                            <td>{{$todo->uso}}</td>
                                            <td><b>Tipo Servicio</b></td>
                                            <td>{{$todo->tipo_servicio}}</td>
                                            <td><b>Combustible</b></td>
                                            <td>{{$todo->combustible}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Número de motor</b></td>
                                            <td>{{$todo->numero_motor}}</td>
                                            <td><b>REPUVE</b></td>
                                            <td>{{$todo->numero_repuve ?? '-'}}</td>
                                            <td><b>Puertas</b></td>
                                            <td>{{$todo->numero_puertas ?? '-'}}</td>
                                            <td><b>Pasajeros</b></td>
                                            <td>{{$todo->personas ?? '-'}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Cilindros</b></td>
                                            <td>{{$todo->numero_cilindros}}</td>
                                            <td><b>Capacidad de Litros</b></td>
                                            <td>{{$todo->capacidad_litros}}</td>
                                            <td><b>Capacidad de Khw</b></td>
                                            <td>
                                                @if($todo->capacidad == '')
                                                    -
                                                @else
                                                    {{$todo->capacidad}}
                                                @endif</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel">
                            <div class="panel-heading text-center " style="background-color: #e91e63;color: white;">
                                <h2 class="panel-title">Propietario</h2>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless " style="font-size: 15px">
                                        <tbody>
                                        <tr>
                                            <td><b>RFC</b></td>
                                            <td>{{$todo->rfc}}</td>

                                            <td><b>Tipo Propietario</b></td>
                                            <td>@if($todo->tipo_propietario == 1)
                                                    Fisca
                                                @elseif($todo->tipo_propietario == 2)
                                                    Moral
                                                @else
                                                    -
                                                @endif</td>
                                        </tr>
                                        <tr>
                                            <td><b>Nombre</b></td>
                                            <td>{{$todo->nombre ?? $todo->razsoc}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Apellido Paterno</b></td>
                                            <td>{{$todo->paterno ?? '-'}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Apellido Materno</b></td>
                                            <td>{{$todo->materno ?? '-'}}</td>

                                        </tr>


                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel ">
                            <div class="panel-heading text-center " style="background-color: #e91e63;color: white;">
                                <h2 class="panel-title">Domicilio</h2>
                            </div>

                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless" style="font-size: 15px">
                                        <tbody>
                                        <tr>
                                            <td><b>Entidad</b></td>
                                            <td>Ciudad de Mexico</td>
                                            <td><b>Delegación</b></td>
                                            <td>{{$todo->colonia}}</td>
                                            <td><b>Colonia</b></td>
                                            <td>{{$todo->colonia}}</td>

                                        </tr>
                                        <tr>
                                            <td><b>Código Postal</b></td>
                                            <td>{{$todo->colonia}}</td>
                                            <td><b>Calle</b></td>
                                            <td>
                                                @if($todo->calle == "")
                                                    Por Actualizar
                                                @else
                                                    {{$todo->calle}}
                                                @endif</td>
                                            <td><b>Número Exterior</b></td>
                                            <td>
                                                @if($todo->numero_exterior == "")
                                                    Por Actualizar
                                                @else
                                                    {{$todo->numero_exterior}}
                                                @endif</td>

                                        </tr>
                                        <tr>
                                            <td><b>Número Interior</b></td>
                                            <td>@if($todo->numero_interior == "")
                                                    Por Actualizar
                                                @else
                                                    {{$todo->numero_interior}}
                                                @endif</td>
                                            <td><b>Telefono</b></td>
                                            <td>@if($todo->telefono == "")
                                                    Por Actualizar
                                                @else
                                                    {{$todo->telefono}}
                                                @endif</td>


                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel-footer text-center">
                <button type="submit" class="btn bg-teal  btn-xlg" onclick="solicitante()">
                    Dar de Baja
                </button>

                <a href="{{url('/Linea_Captura/Baja')}}">
                    <button type="button" class="btn bg-pink btn-xlg">
                        Regresar
                    </button>
                </a>

            </div>
        </div>

        <div id="modal_baja" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-pink">

                        <h4 class="modal-title">Baja General de Vehículo </h4>
                    </div>

                    <!--  -->
                    <form id="baja_form" action="{{url('/Baja/Vehiculo')}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="serie" value="{{$todo->serie_vehicular}}">
                        <input type="hidden" name="todo" value="{{$todo}}">
                        <input name="linea_captura" type="hidden" readonly value="{{$linea_pago}}">
                        <input name="cat_monto" type="hidden" readonly value="39">
                        <input type="hidden" name="tipo_movimiento" value="Baja General">
                        <input type="hidden" name="motivo" value="03">
                        <div class="modal-body">
                            <div class="form-group">
                                <div id="1er" class="form-group">
                                    <div class="row text-center">
                                        <h5><label>Placa Y Tarjeta Circulación Anteriores</label></h5>
                                    </div>
                                    <div class="row">
<!------------------>
                                        <div class="col-md-4 col-md-offset-1 ">
                                            <label for="">¿Cuantas placas regresó el ciudadano?</label>
                                            <div class="form-group">
                                                <div name="focus_tc_0">
                                                <span>
                                                    <input name="num_placas" required value="0" type="radio"
                                                           class="styled"
                                                           id="num_placa_0"/>
                                                </span>
                                                    <span for="">Ninguna</span>

                                                </div>
                                                <div name="focus_tc_1">
                                                <span>
                                                    <input name="num_placas" required value="1" type="radio"
                                                           class="styled"
                                                           id="num_placa_1"/>
                                                </span>
                                                    <span>1 Placa</span>

                                                </div>
                                                <div name="focus_tc_2">
                                                <span>
                                                    <input name="num_placas" required value="2" type="radio"
                                                           class="styled"
                                                           id="num_placa_2"/>
                                                </span>
                                                    <span>2 Placas</span>

                                                </div>

                                            </div>
                                        </div>
                                        <!------------------>


                                        <div class="col-md-4 col-md-offset-2 ">
                                            <label for="">¿Presentó la Tarjeta de Circulación?</label>
                                            <div class="form-group">

                                                <div class="radio" name="focus_presento_tc">
                                                    <label for="">
                                                        <input name="tc" type="radio" class="styled"
                                                               id="presento_tc" value="1" required>Si
                                                    </label>

                                                </div>
                                                <div class="radio" name="focus_no_presento_tc">
                                                    <label>
                                                        <input name="tc" type="radio" class="styled"
                                                               id="no_presento_tc" value="0" required>No
                                                    </label>


                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <hr>

                                </div>
                                <div id="2do" class="form-group">

                                    <div class="row text-center">
                                        <h5><label>Folio de la Agencia del Ministerio Público</label></h5>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-7 col-md-offset-2">

                                            <div class="input-group">

                                                <input name="folio_mp" class="form-control text-center"
                                                       maxlength="18"
                                                       required
                                                       disabled>
                                                <span class="input-group-btn">
                                            <button type="button" class="btn bg-pink "
                                                    onclick="validar_folio_mp()">
                                                Validar
                                            </button>
                                                 </span>

                                            </div>
                                        </div>


                                    </div>


                                </div>
                                <hr>
                                <div id="3ro" class="form-group">
                                    <div class="row text-center">
                                        <h5><label>Datos del Solicitante</label></h5>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-4 ">
                                            <label>Nombre(s)</label>
                                            <div class="form-group">
                                                <input name="nombre_solicitante" class="form-control soloLetras_espacio"
                                                       maxlength="18"
                                                       required>
                                            </div>
                                        </div>

                                        <div class="col-md-4 ">
                                            <label>Primer Apellido</label>
                                            <div class="form-group">
                                                <input name="primer_apellido_solicitante"
                                                       class="form-control soloLetras_espacio"
                                                       maxlength="18"
                                                       required>
                                            </div>
                                        </div>

                                        <div class="col-md-4 ">
                                            <label>Segundo Apellido</label>
                                            <div class="form-group">
                                                <input name="segundo_apellido_solicitante"
                                                       class="form-control soloLetras_espacio"
                                                       maxlength="18"
                                                       required>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 ">
                                            <label>CURP/RFC </label>
                                            <div class="form-group">
                                                <input name="curp_solicitante" class="form-control" maxlength="18"
                                                       required>
                                            </div>
                                        </div>

                                        <input type="hidden" name="tramite_mega" value="{{$todo->id_megamicro}}">

                                        <div class="col-md-4 ">
                                            <label>Tipo de Identificación </label>
                                            <div class="form-group">
                                                <select name="doc_solicitante" class="form-control" disabled required>
                                                    <option></option>
                                                    @foreach($tipoidentificacion as $valor => $clase)
                                                        <option value="{{ $clase['id_tipo_identificacion'] }}">{{ $clase['tipo_identificacion'] }} </option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Folio de Identificación</label>
                                            <div class="form-group">
                                                <input type="text" name="folio_doc_solicitante" class="form-control"
                                                       maxlength="20"
                                                       readonly required>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>


                            <div class="modal-footer text-right">


                                <button
                                        onclick="dd_ajax()"
                                        type="button"
                                        disabled
                                        name="datos"
                                        class="btn btn-xlg bg-pink"

                                >Imprimir datos
                                </button>

                                <button type="button" class="btn btn-xlg bg-teal  ">Salir
                                </button>


                            </div>

                    </form>
                </div>
            </div>
        </div>



    @endif

    <script type="text/javascript">

        $('#2do').hide();
        $('#3ro').hide();

        $('div .modal-footer').hide();
        $("[name='num_placas'],[name='tc']").change(function () {
            var num_placa = $('[name = num_placas]:checked').val();
            var num_tc = $('[name = tc]:checked').val();

            /*if ($("[name='num_placas']").is(':checked') && $("[name='tc']").is(':checked')) {

                if (num_placa == 2 && num_tc == 1) {
                    $('#1ro').hide();
                    $('#2do').hide();
                    $('[name = folio_mp]').val('').prop("disabled", true);
                    $('#3ro').show();
                    $('div .modal-footer').show();
                    $('[name = nombre_solicitante]').focus();
                }

                else {
                    $('#1ro').show();
                    $('#2do').show();
                    $('#3ro').hide();
                    $('[name = folio_mp]').val('').prop("disabled", false).focus();
                    $('div .modal-footer').hide();
                }
            }*/

            $('#1ro').hide();
            $('#2do').hide();
            $('[name = folio_mp]').val('').prop("disabled", true);
            $('#3ro').show();
            $('div .modal-footer').show();
            $('[name = nombre_solicitante]').focus();

        });

        function solicitante() {

            $('#modal_baja').modal({
                backdrop: 'static',
                keyboard: false
            });
            $('#modal_baja [name=nombre]').val();
            $('#modal_baja [name=curp]').val();
            $('#modal_baja [name=rfc]').val();
            $('#modal_baja [name=serie]').val();
            $('#modal_baja [name=placa]').val();
            $('#modal_baja [name=vehiculo_id]').val();
            $('#modal_baja').on('shown.bs.modal', function () {

                $('[ id = num_placa_0]').focus();
            })
            $('#modal_baja').modal();

        }

        function validar_folio_mp() {

            var folio_mp = $('[name = folio_mp]').val();
            if (folio_mp == "") {
                swal({
                    title: "",
                    type: "error",
                    text: "Inserte un Folio de MP",
                    confirmButtonText: "Aceptar"
                }).then(function () {
                    $("[name = folio_mp]").focus();
                });
            }
            else {
                $.ajax({
                    url: " {{url('/API/validar_foli_mp/')}} ",
                    type: "post",
                    data: {"folio_mp": folio_mp},
                    success: function (data) {
                        //  console.log(data);

                        if (data != "") {
                            swal({
                                title: "",
                                type: "error",
                                text: "El folio del acta del MP ya existe",
                                confirmButtonText: "Aceptar"
                            }).then(function () {
                                $("[name = folio_mp]").focus();
                            });
                        }

                        else {

                            $('div .modal-footer').show();
                            $('#2do').show();
                            $('#1ro').show();
                            $('#3ro').show();
                            $('[name = nombre_solicitante]').focus();
                        }

                    }

                });
            }

        }

        function datos_correctos() {
            swal({
                title: "¿Los datos son correctos?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#dd2100",
                confirmButtonText: "Si, Dar de baja",
                cancelButtonText: "No, hubo un error",
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(function () {

                swal({
                    title: "¿Esta seguro que desea dar de baja el vehículo?",
                    text: ".",
                    type: "success",
                    showCancelButton: true,
                    cancelButtonText: "Cancelar",
                    confirmButtonColor: "#dd0e00",
                    confirmButtonText: "Si",
                    allowEscapeKey: false,
                    allowOutsideClick: false
                }).then(function () {
                    $('#baja_form').submit();
                }, function (dismiss) {

                    if (dismiss === 'cancel') {
                        swal({
                            title: "Cancelado",
                            text: "..",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#dd0e00",
                            confirmButtonText: "Si",
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        }).then(function () {
                            $('[name = datos]').prop("disabled", true);
                            $('[name= folio_doc_solicitante]').val('').focus();
                        });
                    }
                });

            }, function (dismiss) {

                if (dismiss === 'cancel') {
                    swal({
                        title: "Cancelado",
                        text: ".",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#dd0e00",
                        confirmButtonText: "Si",
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then(function () {
                        $('[name = datos]').prop("disabled", true);
                        $('[name= folio_doc_solicitante]').val('').focus();
                    });
                }
            });
        }

        function dd_ajax() {

            var inputs = $("#baja_form").serialize();
            $.ajax({
                url: "{{url('/API/PDF/Baja')}}",
                type: "POST",
                data: {
                    "data": inputs
                },
                success: function (pdf) {
                    var appBaseUrl = '{{ url('/') }}';
                    var url = appBaseUrl + pdf;
                    printJS({printable: url, type: 'pdf', modalMessage: 'Cargando PDF...'})
                }

            });
            setTimeout(datos_correctos, 3000);

        }


    </script>
@endsection
