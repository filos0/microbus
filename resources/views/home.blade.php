@extends('layouts.app')

@section('content')
    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                });
            </script>
        @endforeach

    @endif
    <div class="panel">
        <div class=" text-center" style="font-size: 32px">
            <br>

            <label for=""><b>Control Vehicular Micro</b></label>
        </div>
        <div class="panel-body text-center">
            <img src="{{asset('/assets/img/home.png')}}" class="img-responsive">
        </div>
    </div>

@endsection
