@extends('layouts.app')

@section('content')


    <div class="panel panel-flat">
        <div class="panel-heading text-left text-pink" >
            <h1>
                <b>CORECCIÓN DE DATOS</b>
            </h1>

        </div>

        <form id="baja_form" action="{{url('/Actualizar/Datos')}}" method="POST">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id_mega" value="{{$vehiculo->id_megamicro ?? 0}}">
            <input type="hidden" name="serie" value="{{$vehiculo->serie_vehicular}}">
            <input type="hidden" name="placa" value="{{$placa}}">
            <input type="hidden" name="concesion" value="{{$consecion}}">
            <input type="hidden" name="clave_vehicular" value="{{$vehiculo->clave_vehicular}}">
            <input type="hidden" name="marca" value="{{$vehiculo->marca}}">
            <input type="hidden" name="linea" value="{{$vehiculo->linea}}">
            <input type="hidden" name="version" value="{{$vehiculo->version}}">

        <div class="panel-body">
            <div class="panel-body">
                <div class="tabbable">
                    <ul class="nav nav-pills nav-pills-bordered nav-justified">
                        <li class="active"><a href="#bordered-justified-pill1" data-toggle="tab"><b style="font-size: 18px">DATOS VEHÍCULO</b></a></li>
                        <li><a href="#bordered-justified-pill2" data-toggle="tab"><b style="font-size: 18px">DATOS PROPIETARIO</b></a></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="bordered-justified-pill1">

                            <div class="row">
                                <div class="col-md-3 col-md-offset-1">
                                    <div class="form-group text-center">
                                        <label><b>Serie Vehicular</b></label>
                                        <input disabled type="text" class="form-control text-center" name="serie"
                                               value="{{$vehiculo->serie_vehicular}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Placa</b></label>
                                        <input disabled type="text" class="form-control text-center" name="placa"
                                               value="{{$placa}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Concesión</b></label>
                                        <input disabled type="text" class="form-control text-center"name="concesion"
                                               value="{{$consecion}}">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Clave Vehicular</b></label>
                                        <input disabled type="text" class="form-control text-center"
                                               name="clave_vehicular"
                                               value="{{$vehiculo->clave_vehicular}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Marca</b></label>
                                        <input disabled type="text" class="form-control text-center" name="marca"
                                               value="{{$vehiculo->marca}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Linea</b></label>
                                        <input disabled type="text" class="form-control text-center" name="linea"
                                               value="{{$vehiculo->linea}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Versión</b></label>
                                        <input disabled type="text" class="form-control text-center" name="version"
                                               value="{{$vehiculo->version}}">
                                    </div>
                                </div>
                            </div>

                            <br>
                            <br>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Tipo de Servicio</b></label>
                                        <input type="text" class="form-control text-center" name="tipo"
                                               value="{{$vehiculo->tipo_servicio->tipo_servicio ?? $vehiculo->tipo_servicio}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Úso de Vehículo</b></label>
                                        <input type="text" class="form-control text-center" name="uso"
                                               value="{{$vehiculo->uso->uso_vehiculo ?? $vehiculo->uso}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Clase de Vehículo</b></label>
                                        <input type="text" class="form-control text-center" name="clase"
                                               value="{{$vehiculo->clasetipo->clase ?? $vehiculo->clase}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Tipo de Vehículo</b></label>
                                        <input type="text" class="form-control text-center" name="tipo_vehiculo"
                                               value="{{$vehiculo->clasetipo->tipo ?? $vehiculo->tipo}}">
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Número de Motor</b></label>
                                        <input type="text" class="form-control text-center" name="numero_motor"
                                               value="{{$vehiculo->numero_motor}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Número de Cilindros</b></label>
                                        <input type="number" class="form-control text-center" name="num_cilindros"
                                               value="{{$vehiculo->numero_cilindros}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Capacidad de Litros</b></label>
                                        <input type="number" class="form-control text-center"
                                               value="{{$vehiculo->capacidad_litros}}">
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Tipo de Combustible</b></label>
                                        <input type="text" class="form-control text-center" name="num_personas"
                                               value="{{$vehiculo->tipo_combustible->tipo_combustible ?? ''}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Número de Personas</b></label>
                                        <input type="number" class="form-control text-center" name="num_personas"
                                               value="{{$vehiculo->numero_personas ?? $vehiculo->personas}}" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Número de Puertas</b></label>
                                        <input type="number" class="form-control text-center" name="num_puertas"
                                               value="{{$vehiculo->numero_puertas ?? ''}}" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>REPUVE</b></label>
                                        <input type="text" class="form-control text-center" name="numero_repuve"
                                               value="{{$vehiculo->numero_repuve ?? '' }}">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Distribuidora</b></label>
                                        <input type="text" class="form-control text-center" name="distribuidora"
                                               value="{{$vehiculo->distribuidora ??''}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Fecha de Factura</b></label>
                                        <input type="date" class="form-control text-center"
                                               value="{{$vehiculo->fecha_factura ?? ''}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Importe Factura</b></label>
                                        <input type="text" class="form-control text-center" name="valor_factura"
                                               value="{{$vehiculo->importe_factura ?? ''}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Número de Factura</b></label>
                                        <input type="text" class="form-control text-center" name="numero_factura"
                                               value="{{$vehiculo->numero_factura ??''}}">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Aseguradora</b></label>
                                        <input type="text" class="form-control text-center" name="distribuidora"
                                               value="{{$vehiculo->aseguradora ?? ''}}" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Número de Poliza</b></label>
                                        <input type="text" class="form-control text-center" name="distribuidora"
                                               value="{{$vehiculo->numero_poliza_seguro ?? ''}}" required>
                                    </div>
                                </div>

                            </div>

                        </div>


                        <div class="tab-pane" id="bordered-justified-pill2">

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Nombre/Razon Social</b></label>
                                        <input type="text" class="form-control text-center" name="nombre"
                                               value="{{$propietario->nombre_razon_social ?? $propietario->nombre.$propietario->razsoc}}" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Primer Apellido</b></label>
                                        <input type="text" class="form-control text-center" name="primer_apellido"
                                               value="{{$propietario->primer_apellido ?? $propietario->paterno}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Segundo Apellido</b></label>
                                        <input type="text" class="form-control text-center" name="segundo_apellido"
                                               value="{{$propietario->segundo_apellido ?? $propietario->materno}}" >
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Fecha de Nacimiento</b></label>
                                        <input type="date" class="form-control text-center" name="fecha_nacimiento"
                                               value="{{$propietario->fecha_nacimiento ?? $propietario->fecnac}}" >
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>RFC</b></label>
                                        <input type="text" class="form-control text-center" name="rfc"
                                              readonly value="{{$propietario->rfc}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Nacionalidad</b></label>
                                        <select name="nacionalidad" class="form-control" id="procedencia" required>
                                            <option></option>
                                            @foreach($pais as $valor => $clase)
                                                <option value="{{ $clase['id_pais'] }}">{{ $clase['nacionalidad'] }} </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Sexo</b></label>
                                        @php
                                            if($propietario->sexo == "M")
                                            $propietario->sexo = "MASCULINO";
                                            elseif ($propietario->sexo == "F")
                                            $propietario->sexo = "FEMENINO";
                                            else
                                            $propietario->sexo = "-";
                                        @endphp
                                        <input type="text" class="form-control text-center" name="sexo"
                                               value="{{$propietario->sexo}}" required>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Código Postal</b></label>
                                        <input type="text" class="form-control text-center" name="codigo_postal"
                                               value="FALTA CATALOGO">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Delegación</b></label>
                                        <input type="text" class="form-control text-center" name="delegacion"
                                               value="FALTA CATALOGO">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Colonia</b></label>
                                        <input type="text" class="form-control text-center" name="colonia"
                                               value="FALTA CATALOGO">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Telefono</b></label>
                                        <input type="tel" class="form-control text-center" name="telefono"
                                               value="{{$propietario->telefono ?? ''}}">
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Calle</b></label>
                                        <input type="text" class="form-control text-center" name="calle"
                                               value="{{$propietario->calle ?? ''}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Número Interior</b></label>
                                        <input type="text" class="form-control text-center" name="numero_interior"
                                               value="{{$propietario->numero_interior ?? ''}}">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group text-center">
                                        <label><b>Número Exterior</b></label>
                                        <input type="text" class="form-control text-center" name="numero_exterior"
                                               value="{{$propietario->numero_exterior ?? ''}}">
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>


        <div class="panel-footer">
            <div class="text-center">
                <button class="confirmar_salir btn btn-xlg bg-danger-800" type="button" > CANCELAR <i style="margin-left:5px"
                                                                                     class="icon icon-cross"></i>

                </button>
                <button onclick="dd_ajax()"
                        type="button"

                        name="datos"
                        class="btn btn-xlg bg-teal-800"> FINALIZAR <i style="margin-left:5px"
                                                                                    class="glyphicon glyphicon-floppy-saved"></i>
                </button>
            </div>
        </div>
        </form>
    </div>


    <script type="text/javascript">

        function dd_ajax() {

            var inputs = $("#baja_form").serialize();
            $.ajax({
                url: "{{url('/API/PDF/Confirmar_Datos')}}",
                type: "POST",
                data: {
                    "data": inputs
                },
                success: function (pdf) {
                    var appBaseUrl = '{{ url('/') }}';
                    var url = appBaseUrl + pdf;
                    printJS({printable: url, type: 'pdf', modalMessage: 'Cargando PDF...'})
                }

            });
            setTimeout(datos_correctos, 3000);

        }

        function datos_correctos() {
            swal({
                title: "¿Los datos son correctos?",
                text: "",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#dd2100",
                confirmButtonText: "Si, Actializar datos",
                cancelButtonText: "No, hubo un error",
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(function () {

                swal({
                    title: "¿Esta seguro que desea actualizr la informacion?",
                    text: ".",
                    type: "success",
                    showCancelButton: true,
                    cancelButtonText: "Cancelar",
                    confirmButtonColor: "#dd0e00",
                    confirmButtonText: "Si",
                    allowEscapeKey: false,
                    allowOutsideClick: false
                }).then(function () {
                    $('#baja_form').submit();
                }, function (dismiss) {

                    if (dismiss === 'cancel') {
                        swal({
                            title: "Cancelado",
                            text: "..",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#dd0e00",
                            confirmButtonText: "Si",
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        }).then(function () {
                            $('[name = datos]').prop("disabled", true);
                            $('[name= folio_doc_solicitante]').val('').focus();
                        });
                    }
                });

            }, function (dismiss) {

                if (dismiss === 'cancel') {
                    swal({
                        title: "Cancelado",
                        text: ".",
                        type: "error",
                        showCancelButton: false,
                        confirmButtonColor: "#dd0e00",
                        confirmButtonText: "Si",
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then(function () {
                        $('[name = datos]').prop("disabled", true);
                        $('[name= folio_doc_solicitante]').val('').focus();
                    });
                }
            });
        }

        $('.confirmar_salir').click(function () {
            swal({
                title: "¿Esta seguro que quiere salir?",
                text: "Se perdera todo el avance",
                type: "error",
                showCancelButton: true,
                cancelButtonText: "Cancelar",
                confirmButtonColor: "#dd0e00",
                confirmButtonText: "Si, Salir!"
                // closeOnConfirm: false
            }).then(function () {
                window.location.href = {!! json_encode(url('/')) !!}
                setInterval(fecha_actual, 1000);
            });

        });

    </script>

@endsection