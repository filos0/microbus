@extends('layouts.app')

@section('content')

    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {

                    $('[ name = linea_captura]').focus();
                });
            </script>
        @endforeach

    @endif

    <form action="{{url('/Correcion/Datos')}}" method="POST" id="form_lc" novalidate>
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id_concesion" value="">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h2>
                    <b>Comprobar Línea de Captura</b>
                </h2>
                <div class="text-left">
                    <label style="font-size: 18px" class="text-pink">Inserte una <b>Línea de Captura </b>previamente
                        pagada</label><br>
                </div>
            </div>

            <div class="panel-body">



                <div class="col-md-12" style="margin: 20px;font-size: 18px">
                    <div class="col-md-6 col-md-offset-3">

                        <div class="row">
                            <label class="text-bold">Linea de Captura</label>
                            <input type="text" name="linea_captura" required autofocus
                                   class="form-control text-center input-xlg placa_text"
                                   maxlength="20">
                        </div>
                        <br>
                        <div class="row">
                            <label class="text-bold">Número de Placa</label>
                            <input type="text" name="placa" required
                                   class="form-control text-center input-xlg placa_text"
                                   maxlength="12">
                        </div>
                    </div>
                </div>


            </div>

            <div class="panel-footer">
                <div class="text-center">
                    <button id="form_lc" type="submit" class="btn btn-xlg bg-pink">Comprobar</button>
                </div>
            </div>

        </div>
    </form>
    <div id="modal_conseciones" class="modal fade">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h2 class=""><b>SELECCIONA EL REGISTRO CORRECTO</b></h2>
                </div>

                <div class="modal-body">
                    <div>
                        <table id="tabla_conseciones" class='table table-responsive' style="font-size: 15px">

                            <tr class="sin_hover">
                                <th class='text-center'><b>PLACA</b></th>

                                <th class='text-center'><b>FOLIO CONCESIÓN</b></th>

                                <th class='text-center'><b>FECHA ALTA</b></th>

                                <th class='text-center'><b>RUTA</b></th>


                            </tr>

                            <tbody id="llenar_tabla" class="text-center">
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn bg-danger-800" data-dismiss="modal" onclick="limpiar_concesion();">
                        NO
                        SE ENCUENTRA
                    </button>

                </div>
            </div>
        </div>
    </div>

    <div id="modal_confirmar" class="modal fade">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h3 class=""><b>¿SEGURO DE UTILIZAR LA SIGUIENTE INFORMACIÓN?</b></h3>
                </div>

                <div class="modal-body text-center">
                    <div id="llenar_datos" style="font-size: 15px">

                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn  bg-danger-800" data-dismiss="modal" onclick="cancelar();">
                        Cancelar <i class="icon icon-cross"></i>
                    </button>
                    <button type="button" class="btn bg-teal" onclick="aceptar();">
                        Confirmar <i class="icon icon-check2"></i>
                    </button>


                </div>
            </div>
        </div>
    </div>


    <script>

        function cancelar() {
            $('#modal_confirmar').modal('toggle');
            $("#btn_comprobar").click();
        }

        function aceptar() {


            $("#form_lc")[0].submit();
        }

        function limpiar_concesion() {
            $('[name = concesion]').val('').focus();
        }

        $('#form_lc').on('submit', function (e) {
            e.preventDefault();

            $("#form_lc")[0].submit();

  /*          var concesion = $('[name = concesion]').val();
            $.ajax({
                url: '{{url("/API/concesionactiva")}}',
                type: "POST",
                data: {"concesion": concesion},

                success: function (data) {
                    $('#llenar_tabla').empty();
                    if (data == 0) {

                        swal({
                            title: "La concesion no existe",
                            html: "",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#cb0004",
                            confirmButtonText: "ACEPTAR",
                            allowOutsideClick: false,
                            allowEscapeKey: false

                        }).then(function () {


                            $('[name = concesion]').focus();
                        });

                    }
                    else {
                        if (typeof data == "object") {
                            var html = '';
                            $.each(data, function (i, concesiones) {


                                html = html + ( '<tr style="cursor: pointer;" onmouseover="$(this).css(\'background-color\', \'rgb(236,64,122)\').css(\'color\',\'white\');" onmouseleave="$(this).css(\'background-color\', \'transparent\').css(\'color\',\'black\');" onclick="aceptar_concesiones(\'' + concesiones.id_concesion + '\')" >' +
                                    '<td >' + concesiones.placa + '</td>' +
                                    '<td >' + concesiones.clave_concesion + '</td>' +
                                    '<td >' + concesiones.fecalta + '</td>' +
                                    '<td >' + concesiones.ruta + '</td>' +

                                    '</tr>'
                                )
                                ;

                            });

                            $('#llenar_tabla').append(html);
                            $('#modal_conseciones').modal({
                                backdrop: 'static',
                                keyboard: false
                            });

                        }
                        else {
                            $("[name = id_concesion]").val(data).focus();
                            $("#form_lc")[0].submit();
                        }

                    }

                }
            });
*/

        });

        function aceptar_concesiones(concesion) {
            $('#llenar_datos').empty();
            $('#modal_conseciones').modal('toggle');
            $.ajax({
                url: '{{url("/API/concesion")}}',
                type: "POST",
                data: {"concesion": concesion},

                success: function (data) {

                    var html;

                    html = '<b>Folio Concesion : </b>' + data.clave_concesion + '<br>' +
                        '<b>Placa:  </b>' + data.placa + '<br>' +
                        '<b>Fecha alta:  </b>' + data.fecalta + '<br><br>' +
                        '<b>Fecha expedicion:  </b>' + data.fecexped + '<br>' +
                        '<b>Fecha vencimiento:  </b>' + data.fecvenc + '<br>' +
                        '<b>Ruta:  </b>' + data.ruta + '<br>' +
                        '<b>Estatus:  </b>' + "Activa" + '<br>';

                    $('#llenar_datos').append(html);
                    $("[name = id_concesion]").val(data.id_concesion).focus();
                    $('#modal_confirmar').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                }


            });

        }

    </script>


@endsection
