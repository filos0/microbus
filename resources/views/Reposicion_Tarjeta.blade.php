@extends('layouts.app')

@section('content')

        <div class="panel panel-flat">
            <div class="panel-heading">
                <h2>Reposición de Tarjeta Circulación</h2>
            </div>
            <div class="panel-body">
                <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-flat">
                        <div class="panel-heading">
                            <h5 class=" text-center panel-title"><span class="heading-text "><i
                                            class=" icon-credit-card position-left"></i>Tarjeta de Circulación Encontrada</span>
                            </h5>
                        </div>

                        <br>

                        <div class="panel-body ">
                            <table style="width: 100%">
                                <tbody>
                                <tr>
                                    <td><label for="">Folio de Tarjeta Circulación</label></td>
                                    <td><input readonly class="form-control text-center" type="text" name="folio_tc"
                                               value="{{$folio_tc}}">
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="">Fecha de Expedición</label></td>
                                    <td><input readonly class="form-control text-center" type="text" name="exp_tc"
                                               value="{{$fecha_exp}}"></td>
                                </tr>
                                <tr>
                                    <td><label for="">Vigencia de Tarjeta de Circulación</label></td>
                                    <td><input readonly class="form-control text-center" type="text" name="vigencia_tc"
                                               value="{{$vigencia_tc}}">
                                    </td>
                                </tr>
                                <tr>
                                    <td><label for="">Nombre del Propietario</label></td>
                                    <td><input readonly class="form-control text-center" type="text" name="nombre"
                                               value="{{$propietario}}">
                                    </td>
                                </tr>


                                </tbody>
                            </table>


                        </div>
                        <div class="panel-footer  text-center">

                            <button type="button"
                                    class="btn bg-pink btn-xlg"
                                    onclick="solicitante();"
                                    formtarget="_blank">
                                Confirmar <i class="icon  arrow-circle-o-right"></i></button>


                            <a href="{{url('/Validar/LineaPago')}}">
                                <button type="button" class="btn bg-teal btn-xlg">
                                    Regresar<i class="icon  arrow-circle-o-right"></i>
                                </button>
                            </a>


                        </div>
                    </div>
                </div>


            </div>
        </div>
        <div id="modal_reposicion" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-pink">

                        <h4 class="modal-title">Reposición de Tarjeta de Circulación</h4>
                    </div>
                    <form action="{{url('/Reposicion/Tarjeta/Validar')}}" method="POST" id="reposicion_tc">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input name="linea_captura" type="hidden" readonly value="{{$linea}}">
                        @if($mega_id != 0)
                        <input name="mega_id" type="hidden" value="{{$mega_id}}">
                        @endif
                        <input name="serie" type="hidden" value="{{$vehiculo}}">
                        <input name="tipo_movimiento" type="hidden" value="Reposición de Tarjeta de Circulación">
                        <input name="tramite" type="hidden" value="{{$tramite}}">
                        <input name="folio_mp" type="hidden" value="{{$folio_mp}}">
                        <div class="modal-body">
                          <div class="form-group">


                                    <div class="row text-center">
                                        <h5><label for="">Información del Solicitante</label></h5>
                                    </div>
                                    <div class="row">

                                        <div class="col-md-4 ">
                                            <label>Nombre(s)</label>
                                            <div class="form-group">
                                                <input name="nombre_solicitante" class="form-control soloLetras_espacio"
                                                       maxlength="18"
                                                       required>
                                            </div>
                                        </div>

                                        <div class="col-md-4 ">
                                            <label>Primer Apellido</label>
                                            <div class="form-group">
                                                <input name="primer_apellido_solicitante"
                                                       class="form-control soloLetras_espacio"
                                                       maxlength="18"
                                                       required>
                                            </div>
                                        </div>

                                        <div class="col-md-4 ">
                                            <label>Segundo Apellido</label>
                                            <div class="form-group">
                                                <input name="segundo_apellido_solicitante"
                                                       class="form-control soloLetras_espacio"
                                                       maxlength="18"
                                                       required>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-4 ">
                                            <label>CURP/RFC </label>
                                            <div class="form-group">
                                                <input name="curp_solicitante" class="form-control" maxlength="18"
                                                       required>
                                            </div>
                                        </div>


                                        <div class="col-md-4 ">
                                            <label>Tipo de Identificación </label>
                                            <div class="form-group">
                                                <select name="doc_solicitante" class="form-control" disabled required>
                                                    <option></option>
                                                    @foreach($tipoidentificacion as $valor => $clase)
                                                        <option value="{{ $clase['id_tipo_identificacion'] }}">{{ $clase['tipo_identificacion'] }} </option>
                                                    @endforeach

                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <label>Folio de Identificación</label>
                                            <div class="form-group">
                                                <input type="text" name="folio_doc_solicitante"
                                                       class="form-control alfa_Numerico"
                                                       maxlength="20"
                                                       readonly required>
                                            </div>
                                        </div>

                                    </div>

                            </div>
                        </div>
                        <div class="modal-footer text-right">


                            <button
                                    onclick="dd_ajax();"
                                    type="button"
                                    name="datos"
                                    class="btn btn-xlg bg-pink"
                                    formtarget="_blank"
                                    disabled
                            >Confirmar Datos
                            </button>


                            <button type="button" class="btn btn-xlg bg-teal confirmar_salir ">Salir
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <script>


            function solicitante() {

                $('#modal_reposicion').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#modal_reposicion').modal();

                $('#modal_reposicion').on('shown.bs.modal', function () {
                    $('[name=nombre_solicitante]').focus();
                });

            }

            function datos_correctos() {
                swal({
                    title: "¿Los datos son correctos?",
                    text: "",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#dd2100",
                    confirmButtonText: "Si",
                    cancelButtonText: "No",
                    allowEscapeKey: false,
                    allowOutsideClick: false
                }).then(function () {

                    swal({
                        title: "¿Esta seguro que desea Finalizar el Trámite?",
                        text: ".",
                        type: "success",
                        showCancelButton: true,
                        cancelButtonText: "Cancelar",
                        confirmButtonColor: "#dd0e00",
                        confirmButtonText: "Si",
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then(function () {
                        $('#reposicion_tc').submit();
                    }, function (dismiss) {

                        if (dismiss === 'cancel') {
                            swal({
                                title: "Cancelado",
                                text: "Los datos se podran seguir modificando.",
                                type: "error",
                                showCancelButton: false,
                                confirmButtonColor: "#dd0e00",
                                confirmButtonText: "Si",
                                allowEscapeKey: false,
                                allowOutsideClick: false
                            }).then(function () {
                                $('[name = datos]').prop("disabled", true);
                                $('[name= folio_doc_solicitante]').val('').focus();
                            });
                        }
                    });

                }, function (dismiss) {
                    // dismiss can be 'cancel', 'overlay',
                    // 'close', and 'timer'
                    if (dismiss === 'cancel') {
                        swal({
                            title: "Datos Incorrectos",
                            text: "Los datos se podran seguir modificando.",
                            type: "error",
                            showCancelButton: false,
                            confirmButtonColor: "#dd0e00",
                            confirmButtonText: "Si",
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        }).then(function () {
                            $('[name = datos]').prop("disabled", true);
                            $('[name= folio_doc_solicitante]').val('').focus();
                        });
                    }
                });
            }

            $(' [name = folio_doc_solicitante ]').on('change', function () {
                var folio = $(this).val();


                if (folio.length > 8) {
                    $('[name = datos]').prop("disabled", false);
                    //   alert('si llego ??');
                }

                else {

                    swal({
                        title: "",
                        text: "El folio debe tener más de 8 caracteres.",
                        confirmButtonText: "Aceptar",
                        allowEscapeKey: false,
                        allowOutsideClick: false
                    }).then(function () {
                        $('[name = datos]').prop("disabled", true);
                        $(this).focus();
                    });


                }
            });

            function dd_ajax() {

                var inputs = $("#reposicion_tc").serialize();
                $.ajax({
                    url: "{{url('/API/PDF/Baja')}}",
                    type: "POST",
                    data: {
                        "data": inputs
                    },
                    success: function (pdf) {
                        var appBaseUrl = '{{ url('/') }}';
                        var url = appBaseUrl + pdf;
                        printJS({printable: url, type: 'pdf', modalMessage: 'Cargando PDF...'})
                    }

                });
                setTimeout(datos_correctos, 3000);

            }
        </script>

@endsection
