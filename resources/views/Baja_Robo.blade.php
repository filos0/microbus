@extends('layouts.app')

@section('content')

    @if($errors->any())

        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                }).then(function () {

                    $('[ name = serie]').focus();
                });
            </script>
        @endforeach

    @endif
    @if(!isset($vehiculo))
        <div class="panel panel-flat">
            <div class="panel-heading">
                <div class="panel-title"><p><h4>Baja de Automovíl por Robo</h4><p/></div>
            </div>
            <div class="panel-body">
                <form action="{{url('/Buscar/Baja/Robo')}}" method="POST">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <br><br>
                    <div class="input-group content-group col-md-6 col-md-offset-3">
                        <label style="font-size: 18px"><b>Número de Serie</b></label>
                        <input type="text" name="serie" required autofocus
                               class="form-control text-center input-xlg  placa_text"
                               maxlength="20">
                        <br><br>
                        <br><br>
                        <label style="font-size: 18px"><b>Folio del Ministerio Público</b></label>
                        <input type="text" name="folio_mp" required
                               class="form-control text-center input-xlg text-uppercase "
                               maxlength="30">


                    </div>
                    <div class="row text-center">
                        <button type="submit" class="btn btn-xlg bg-pink ">BUSCAR <i style="margin-left: 5px" class="icon  icon-search4"></i> </button>
                    </div>
                </form>

            </div>
        </div>



    @else
        <div class="panel panel-flat">
            <div class="panel-heading ">
                <h3 class="panel-title">Baja de Automovíl por Robo</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">

                        <div class="panel ">
                            <div class="panel-heading text-center " style="background-color: #e91e63;color: white;">
                                <h2 class="panel-title">Vehiculo</h2>
                            </div>

                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless" style="font-size: 17px">
                                        <tbody>
                                        <tr>
                                            <td><b>Serie Vehicular</b></td>
                                            <td>{{$vehiculo->serie_vehicular}}</td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><b>Estado del Vehiculo</b></td>
                                            <td>  @if($vehiculo->estatus_id == 1)
                                                    <span style="font-size: 15px"
                                                          class="label  label-success">Activo</span>
                                                @elseif($vehiculo->estatus_id == 2)
                                                    <span style="font-size: 15px" class="label label-danger">Baja</span>
                                                @else
                                                    -
                                                @endif</td>
                                        </tr>
                                        <tr>
                                            <td><b>Clase</b></td>
                                            <td>{{$vehiculo->clasetipo->clase}}</td>
                                            <td><b>Tipo</b></td>
                                            <td>{{$vehiculo->clasetipo->tipo}}</td>
                                            <td><b>Marca</b></td>
                                            <td>{{$vehiculo->clave_vehicular->marca}}</td>
                                            <td><b>Linea</b></td>
                                            <td>{{$vehiculo->clave_vehicular->linea}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Año</b></td>
                                            <td>{{$vehiculo->modelo}}</td>
                                            <td><b>Uso</b></td>
                                            <td>{{$vehiculo->uso->uso_vehiculo}}</td>
                                            <td><b>Tipo Servicio</b></td>
                                            <td>{{$vehiculo->tiposervicio->tipo_servicio}}</td>
                                            <td><b>Combustible</b></td>
                                            <td>{{$vehiculo->combustible->tipo_combustible}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Número de motor</b></td>
                                            <td>{{$vehiculo->numero_motor}}</td>
                                            <td><b>REPUVE</b></td>
                                            <td>{{$vehiculo->numero_repuve}}</td>
                                            <td><b>Puertas</b></td>
                                            <td>{{$vehiculo->numero_puertas}}</td>
                                            <td><b>Pasajeros</b></td>
                                            <td>{{$vehiculo->numero_personas}}</td>
                                        </tr>
                                        <tr>
                                            <td><b>Cilindros</b></td>
                                            <td>{{$vehiculo->numero_cilindros}}</td>
                                            <td><b>Capacidad de Litros</b></td>
                                            <td>{{$vehiculo->capacidad_litros}}</td>
                                            <td><b>Capacidad de Khw</b></td>
                                            <td>
                                                @if($vehiculo->capacidad == '')
                                                    -
                                                @else
                                                    {{$vehiculo->capacidad}}
                                                @endif</td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel">
                            <div class="panel-heading text-center " style="background-color: #e91e63;color: white;">
                                <h2 class="panel-title">Propietario</h2>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless " style="font-size: 15px">
                                        <tbody>
                                        <tr>
                                            <td><b>CURP/RFC</b></td>
                                            <td>{{$propietario->clave_unica}}</td>

                                            <td><b>Tipo Propietario</b></td>
                                            <td>@if($propietario->tipo_propietario == 1)
                                                    Fisca
                                                @elseif($propietario->tipo_propietario == 2)
                                                    Moral
                                                @else
                                                    -
                                                @endif</td>
                                        </tr>
                                        <tr>
                                            <td><b>Nombre</b></td>
                                            <td>{{$propietario->nombre_razon_social}}</td>
                                            <td><b>Apellido Paterno</b></td>
                                            <td>{{$propietario->primer_apellido}}</td>
                                            <td><b>Apellido Materno</b></td>
                                            <td>{{$propietario->segundo_apellido}}</td>

                                        </tr>
                                        <tr>
                                            <td><b>Nacionalidad</b></td>
                                            <td>{{$propietario->pais->nacionalidad}}</td>
                                            <td><b>Fecha Nacimiento</b></td>
                                            <td>{{$propietario->fecha_nacimiento}}</td>
                                            <td><b>Sexo</b></td>
                                            <td>
                                                @if($propietario->sexo == 'F')
                                                    Femenino
                                                @elseif($propietario->sexo == 'M')
                                                    Masculino
                                                @else
                                                    -
                                                @endif
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel ">
                            <div class="panel-heading text-center " style="background-color: #e91e63;color: white;">
                                <h2 class="panel-title">Domicilio</h2>
                            </div>

                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-borderless" style="font-size: 15px">
                                        <tbody>
                                        <tr>
                                            <td><b>Entidad</b></td>
                                            <td>Ciudad de Mexico</td>
                                            <td><b>Delegación</b></td>
                                            <td>{{$propietario->colonia->delegacion}}</td>
                                            <td><b>Colonia</b></td>
                                            <td>{{$propietario->colonia->colonia}}</td>

                                        </tr>
                                        <tr>
                                            <td><b>Código Postal</b></td>
                                            <td>{{$propietario->colonia->cp}}</td>
                                            <td><b>Calle</b></td>
                                            <td>
                                                @if($propietario->calle == "")
                                                    Por Actualizar
                                                @else
                                                    {{$propietario->calle}}
                                                @endif</td>
                                            <td><b>Número Exterior</b></td>
                                            <td>
                                                @if($propietario->numero_exterior == "")
                                                    Por Actualizar
                                                @else
                                                    {{$propietario->numero_exterior}}
                                                @endif</td>

                                        </tr>
                                        <tr>
                                            <td><b>Número Interior</b></td>
                                            <td>@if($propietario->numero_interior == "")
                                                    Por Actualizar
                                                @else
                                                    {{$propietario->numero_interior}}
                                                @endif</td>
                                            <td><b>Telefono</b></td>
                                            <td>@if($propietario->telefono == "")
                                                    Por Actualizar
                                                @else
                                                    {{$propietario->telefono}}
                                                @endif</td>


                                        </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="panel-footer text-center">
                <button type="submit" class="btn bg-teal  btn-xlg" onclick="solicitante()">
                    Dar de Baja
                </button>

                <a href="{{url('/Baja')}}">
                    <button type="button" class="btn bg-pink btn-xlg">
                        Regresar
                    </button>
                </a>

            </div>
        </div>

        <div id="modal_baja" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-pink">

                        <h4 class="modal-title">Baja de Vehículo por Robo</h4>
                    </div>

                    <!--  -->
                    <form id="baja_form" action="{{url('/Baja/Vehiculo/Robo')}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="serie" value="{{$vehiculo->serie_vehicular}}">
                        <input name="cat_monto" type="hidden" readonly value="39">
                        <input type="hidden" name="tipo_movimiento" value="Baja por Robo">
                        <input type="hidden" name="folio_mp" value="{{$folio_mp}}">
                        <input type="hidden" name="motivo" value="03">
                        <div class="modal-body">
                            <div class="row text-center">
                                <h5><label>Datos del Solicitante</label></h5>
                            </div>
                            <div class="row">

                                <div class="col-md-4 ">
                                    <label>Nombre(s)</label>
                                    <div class="form-group">
                                        <input name="nombre_solicitante" class="form-control soloLetras_espacio"
                                               maxlength="18" autofocus
                                               required>
                                    </div>
                                </div>

                                <div class="col-md-4 ">
                                    <label>Primer Apellido</label>
                                    <div class="form-group">
                                        <input name="primer_apellido_solicitante"
                                               class="form-control soloLetras_espacio"
                                               maxlength="18"
                                               required>
                                    </div>
                                </div>

                                <div class="col-md-4 ">
                                    <label>Segundo Apellido</label>
                                    <div class="form-group">
                                        <input name="segundo_apellido_solicitante"
                                               class="form-control soloLetras_espacio"
                                               maxlength="18"
                                               required>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 ">
                                    <label>CURP/RFC </label>
                                    <div class="form-group">
                                        <input name="curp_solicitante" class="form-control" maxlength="18"
                                               required>
                                    </div>
                                </div>

                                <input type="hidden" name="tramite_id" value="">

                                <div class="col-md-4 ">
                                    <label>Tipo de Identificación </label>
                                    <div class="form-group">
                                        <select name="doc_solicitante" class="form-control" disabled required>
                                            <option></option>
                                            @foreach($tipoidentificacion as $valor => $clase)
                                                <option value="{{ $clase['id_tipo_identificacion'] }}">{{ $clase['tipo_identificacion'] }} </option>
                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label>Folio de Identificación</label>
                                    <div class="form-group">
                                        <input type="text" name="folio_doc_solicitante"
                                               class="form-control alfa_Numerico"
                                               maxlength="20"
                                               readonly required>
                                    </div>
                                </div>

                            </div>
                            <br>
                            <br>
                            <div class="row text-right">


                                <button
                                        onclick="dd_ajax();"
                                        type="button"
                                        disabled
                                        name="datos"
                                        class="btn btn-xlg bg-pink"
                                        formtarget="_blank">

                                    Imprimir datos
                                </button>


                                <button type="button" class="btn btn-xlg bg-teal confirmar_salir ">
                                    Salir
                                </button>

                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>


    @endif


@endsection
