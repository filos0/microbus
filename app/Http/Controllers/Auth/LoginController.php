<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm(Request $request)
    {
//dd($request);
        $hora_sistema = date("H:i");
        $hora_sistema = strtotime($hora_sistema);

        $inicio_servicio = strtotime("8:00");
        $fin_servicio = strtotime("22:00");

        if ($hora_sistema > $fin_servicio || $hora_sistema < $inicio_servicio) {
            return view("errors/fuera_servicio");
        }

        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        if (strpos($user_agent, 'Opera') || strpos($user_agent, 'OPR/')) $res = 'Opera';
        elseif (strpos($user_agent, 'Edge')) $res = 'Edge';
        elseif (strpos($user_agent, 'Chrome')) $res = 'Chrome';
        elseif (strpos($user_agent, 'Safari')) $res = 'Safari';
        elseif (strpos($user_agent, 'Firefox')) $res = 'Firefox';
        elseif (strpos($user_agent, 'MSIE') || strpos($user_agent, 'Trident/7')) $res = 'Internet Explorer';
        else $res = 'Other';

        if ($res != 'Chrome') {

            return view("Navegador_No_Soportado");

        } else {
            $view = property_exists($this, 'loginView')
                ? $this->loginView : 'auth.authenticate';

            if (view()->exists($view)) {
                return view($view);
            }

            return view('auth.login');

        }


    }

    protected function sendFailedLoginResponse(Request $request)
    {
        //dd($this->username());

        throw ValidationException::withMessages([
            $this->username() => "Usuario y/o Contraseña Incorrecta",
        ]);
    }
    protected function validateLogin(Request $request)
    {

        $this->validate($request, [
            "rfc" => 'required|string',
            'password' => 'required|string',
        ],[
            "rfc.required" => 'El RFC es Necesario',
            "password.required" => 'La Contraseña es Necesaria'
        ]);
    }
}