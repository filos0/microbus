<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\VehiculosRepositorio as vehiculo;
use App\Repositories\PersonaRepositorio as persona;
use App\Repositories\TramiteRepositorio as tramite;
use App\Repositories\Catalogos\CatClaveVehicularRepositorio as clave;
use App\Http\Controllers\PDFController as PDF;

class AltasController extends Controller
{
    public function __construct(vehiculo $vehiculo, clave $clave,
                                persona $persona, tramite $tramite,PDF $PDF)
    {
        $this->vehiculo = $vehiculo;
        $this->persona = $persona;
        $this->tramite = $tramite;
        $this->PDF = $PDF;
        $this->clave = $clave;
    }


    public function Dardealta(){

    }
}
