<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Repositories\VehiculosRepositorio as vehiculo;
use App\Repositories\ConcesionRepositorio as concesion;
use App\Repositories\PersonaRepositorio as persona;
use App\Repositories\TramiteRepositorio as tramite;
use App\Repositories\Catalogos\CatClaveVehicularRepositorio as clave;
use App\Repositories\PersonaConcesionRepositorio as p_concesion;
use App\Repositories\PersonaVehiculoRepositorio as p_vehiculo;
use App\Repositories\MegamicroRerpositorio as mega;
use App\Http\Controllers\PDFController as PDF;
use App\Repositories\PlacaRerpositorio as placa;

class CambioUnidadController extends Controller
{
    public function __construct(vehiculo $vehiculo, clave $clave, p_concesion $p_concesion, p_vehiculo $p_vehiculo,
                                    concesion $concesion, persona $persona, tramite $tramite,PDF $PDF, mega $mega, placa $placa)
    {
        $this->vehiculo = $vehiculo;
        $this->persona = $persona;
        $this->concesion = $concesion;
        $this->tramite = $tramite;
        $this->PDF = $PDF;
        $this->clave = $clave;
        $this->p_concesion = $p_concesion;
        $this->p_vehiculo = $p_vehiculo;
        $this->mega = $mega;
        $this->placa = $placa;
    }

    public function CambiarUnidad(Request $request){
        //dd($request);
        try {

            DB::beginTransaction();

            $clave_vehicular = $this->clave->findBy('clave_vehicular', $request->clave_vehicular)->id_cat_clave_vehicular;
            $lit = $request->lit ?? '0';
            $bat = $request->bat ?? '0';

            $mega = $this->mega->findBy('id_megamicro', $request->id_mega);


            $vehiculo = $this->vehiculo->create(array(
                'serie_vehicular' => $request->serie,
                'fecha_alta' => date('Y-m-d H:i:s'),
                'modelo' => $request->ano,
                'numero_cilindros' => isset($request->cilindros) ? $request->cilindros : 0,
                'numero_motor' => $request->numero_motor,
                'origen_motor' => $request->origen_motor,
                'capacidad_litros' => $lit,
                'capacidad_kwh' => $bat,
                'numero_personas' => $request->pasajeros,
                'numero_puertas' => $request->puertas,
                'folio_documento_legalizacion' => $request->procedencia == 1 ? null : $request->folio_leg,
                'fecha_documento_legalizacion' => $request->procedencia == 1 ? 'null' : $request->fecha_leg,
                'fecha_factura' => $request->fecha_factura,
                'numero_factura' => $request->numero_factura,
                'importe_factura' => str_replace(',', '', $request->valor_factura),
                'numero_repuve' => $request->repuve,
                'distribuidora' => $request->distribuidora,
                //  'aseguradora' => ,
                // 'numero_poliza_seguro' => '',
                'tipo_servicio_id' => 3,
                'uso_vehiculo_id' => 63,/////////////////////////////////////////////
                'clase_tipo_vehiculo_id' => $request->clase_tipo_vehiculo_id,
                'tipo_combustible_id' => $request->combustible,
                'pais_id' => /*$request->procedencia == 1 ? 82 : $request->pais,*/82,
                'estatus_id' => 1,
                'clave_vehicular_id' => $clave_vehicular
            ));

            if ($mega->estatus_sistemas == 1)
                $persona = $this->persona->findBy('rfc', $mega->rfc);

            else
            $persona = $this->persona->create(array('nombre_razon_social' => $mega->nombre . $mega->razsoc,
                'primer_apellido' => $mega->paterno,
                'segundo_apellido' => $mega->materno,
                'curp' => '-',
                'rfc' => $mega->rfc,
                'tipo_identificacion_id' => 0,
                'pais_id' => 82,
                'fecha_nacimiento' => '0000-00-00',
                'telefono' => '-',
                'calle' => '-',
                'numero_exterior' => '-',
                'numero_interior' => '-',
                'id_colonia' => 1));

            if ($mega->estatus_sistemas == 1) {
                $placa = $this->placa->findBy('placa', $mega->placa);
                //   $concesion = $this->concesion->findBy('id_concesion', $placa->concesion_id);
            }

            else {
                $concesion = $this->concesion->create(array(
                    'modalidad' => 'MICRO',
                    'tipo' => $mega->tipo_servicio,
                    'fecha_alta' => '0000-00-00',
                    'fecha_expedicion' => '0000-00-00',
                    'fecha_vencimiento' => '0000-00-00',
                    'ruta' => $mega->ruta,
                    'modulo_id' => 54,
                    'tipo_concesion_id' => 1,
                    'estatus' => 1

                ));


                $placa = $this->placa->create(array(
                    'placa' => $mega->placa,
                    'concesion_id' => $concesion->id_concesion,
                    'placa_metropolitana' => $mega->placametro,
                ));
            }


            $tramite = $this->tramite->create(array(

                'fecha_tramite' => date('Y-m-d H:i:s'),
                'linea_captura' => $request->linea_captura,
                'importe_linea_captura' => 454656,
                'curp_solicitante' => $request->curp_solicitante ,
                'identificacion_solicitante' => $request->doc_solicitante,
                //'constancia' => $request->folio_mp ?? null,
                'costo_id' => 4,
                'persona_id' => $persona->id_persona,
                'vehiculo_id' => $vehiculo->id_vehiculo,
                'modulo_id' => 1,
                'users_id' => 555,
                'placa_id' => $placa->id_placa

            ));

            $this->mega->actualizarestatus($mega->placa, $mega->serie_vehicular);

            $pdf = /*$this->PDF->Vale_Placa($mega->placa,"Central",$request->serie,'10'.$tramite->id_tramite,"MACA9402199J9");*/'gfhfghf';
            DB::commit();

            return view('Finalizar_cambio')
                ->with("pdf_holograma",'no' )
                ->with("resultado", 'Linea de captura valida')
                ->with("placa", $mega->placa)
                ->with("imp_de", 'vale de TC')
                ->with("tramite", $tramite->id_tramite);

        }catch (\Exception $e) {

            DB::rollBack();
            report($e);

            return view('errors.error_global')
                ->with('msg', str_limit($e->getMessage()))
                ->with('code', $e->getCode());
        }
    }

}
