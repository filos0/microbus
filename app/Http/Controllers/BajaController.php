<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\VehiculosRepositorio as vehiculo;
use App\Repositories\TramiteRepositorio as tramite;
use App\Repositories\PersonaRepositorio as persona;
use App\Repositories\MegamicroRerpositorio as mega;
use App\Repositories\PlacaRerpositorio as placa;
use App\Repositories\ConcesionRepositorio as concesion;
use App\Repositories\Catalogos\CatClaseTipoVehiculoRepositorio as clase_tipo;
use App\Repositories\Catalogos\CatClaveVehicularRepositorio as clave;
use App\Http\Controllers\PDFController as pdf;

class BajaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(vehiculo $vehiculo, tramite $tramite, persona $persona, mega $mega, placa $placa, clase_tipo $clase_tipo,
                                    clave $clave, concesion $concesion, pdf $pdf)
    {
        //$this->middleware('auth');
        $this->vehiculo = $vehiculo;
        $this->tramite = $tramite;
        $this->persona = $persona;
        $this->mega = $mega;
        $this->placa = $placa;
        $this->clase_tipo =$clase_tipo;
        $this->clave = $clave;
        $this->concesion = $concesion;
        $this->PDFC = $pdf;



    }


    public function baja_robo()
    {
        return view('Baja_Robo');
    }

    public function dardebaja(Request $request){

//dd($request);
        $mega = $this->mega->findBy('id_megamicro', $request->tramite_mega);
        $clase = $this->clase_tipo->findBy('tipo', $mega->tipo);
        $clave = $this->clave->findBy('clave_vehicular', $mega->clave_vehicular);

       // if (isNull($this->vehiculo->getVehiculoActivo($request->serie)))

        if ($mega->estatus_sistemas == 1)
            $vehiculo = $this->vehiculo->findBy('serie_vehicular' , $mega->serie_vehicular);
        else
            $vehiculo = $this->vehiculo->create(array('serie_vehicular' => $mega->serie_vehicular,
            'fecha_alta' => date('Y-m-d'),
            'modelo' => $mega->modelo,
            'numero_cilindros' => $mega->cilindros ?? '-',
            'numero_motor' => $mega->numero_motor,
            'capacidad_litros' => '-',
            'capacidad_kwh' => '-',
            'numero_personas' => $mega->personas ?? '-',
            'numero_puertas' => '-',
            'folio_documento_legalizacion' => '-',
            'fecha_documento_legalizacion' => '0000-00-00',
            'fecha_factura' => '0000-00-00',
            'numero_factura' => '-',
            'importe_factura' => '-',
            'numero_repuve' => $mega->repuve ?? '-',
            'distribuidora' => '-',
            'aseguradora' => '-',
            'numero_poliza_seguro' => '-',
            'tipo_servicio_id' => 3,
            'uso_vehiculo_id' => 63,
            'clase_tipo_vehiculo_id' => $clase != false ? $clase->id_cat_clase_tipo_vehiculo : 93,
            'tipo_combustible_id' => 6,
            'pais_id' => 82,
            'estatus_id' => 2,
            'clave_vehicular_id' => $clave != false ? $clave->id_cat_clave_vehicular : 1));


      //  elseif ()

        if ($mega->estatus_sistemas == 1)
            $persona = $this->persona->findBy('rfc', $mega->rfc);

        else
            $persona = $this->persona->create(array('nombre_razon_social' => $mega->nombre . $mega->razsoc,
                'primer_apellido' => $mega->paterno,
                'segundo_apellido' => $mega->materno,
                'curp' => '-',
                'rfc' => $mega->rfc,
                'tipo_identificacion_id' => 0,
                'pais_id' => 82,
                'fecha_nacimiento' => '0000-00-00',
                'telefono' => '-',
                'calle' => '-',
                'numero_exterior' => '-',
                'numero_interior' => '-',
                'id_colonia' => 1));

        if ($mega->estatus_sistemas == 1) {
            $placa = $this->placa->findBy('placa', $mega->placa);
         //   $concesion = $this->concesion->findBy('id_concesion', $placa->concesion_id);
        }

        else {
            $concesion = $this->concesion->create(array(
                'modalidad' => 'MICRO',
                'tipo' => $mega->tipo_servicio,
                'fecha_alta' => '0000-00-00',
                'fecha_expedicion' => '0000-00-00',
                'fecha_vencimiento' => '0000-00-00',
                'ruta' => $mega->ruta,
                'modulo_id' => 54,
                'tipo_concesion_id' => 1,
                'estatus' => 1

            ));


            $placa = $this->placa->create(array(
                'placa' => $mega->placa,
                'concesion_id' => $concesion->id_concesion,
                'placa_metropolitana' => $mega->placametro,
            ));
        }
//dd($placa);

        $tramite = $this->tramite->create(array(

            'fecha_tramite' => date('Y-m-d H:i:s'),
            'linea_captura' => $request->linea_captura,
            'importe_linea_captura' => 454656,
            'curp_solicitante' => $request->curp_solicitante ,
            'identificacion_solicitante' => $request->doc_solicitante,
            //'constancia' => $request->folio_mp ?? null,
            'costo_id' => 2,
            'persona_id' => $persona->id_persona,
            'vehiculo_id' => $vehiculo->id_vehiculo,
            'modulo_id' => 1,
            'users_id' => 555,

            'placa_id' => $placa->id_placa,
            /*'Observaciones' => ,
*/
        ));

        $this->mega->actualizarestatus($mega->placa, $mega->serie_vehicular);

        $data = array(

            'folio' => $tramite->id_tramite,
            'clave_unica' => $persona->rfc,
            'nombre' => $persona->primer_apellido . " " . $persona->segundo_apellido . " " . $persona->nombre_razon_social,
            'fecha_nacimiento' => ($persona->fecha_nacimiento == "0000-00-00") ? "-" : $persona->fecha_nacimiento,
            'sexo' => $persona->sexo,
            'domicilio' => $persona->calle,
            'colonia' => $persona->colonia->colonia ?? "SIN DATO" ,
            'delegacion' => $persona->colonia->delegacion ?? "SIN DATO" ,
            'cp' => $persona->colonia->cp ?? "SIN DATO" ,
            'entidad' => 'Ciudad de México',
            'telefono' => ($persona->telefono == "") ? "-" : $persona->telefono,
            'modulo' => 'INFORMATICA',
            'serie' => $vehiculo->serie_vehicular,
            'clase' => $vehiculo->clasetipo->clase ?? "SIN DATO" ,
            'modelo' => $vehiculo->modelo,
            'tipo' => $vehiculo->clasetipo->tipo,
            'clave_vehicular' => $vehiculo->clave_vehicular->clave_vehicular ?? "SIN DATO" ,
            'marca' => $vehiculo->clave_vehicular->marca ?? "SIN DATO" ,
            'linea' => $vehiculo->clave_vehicular->linea ?? "SIN DATO" ,
            'placa' => $placa->placa,
            'placa_anterior' => $mega->placa_anterior,
            'numero_motor' => ($vehiculo->numero_motor == "") ? "SIN DATO" : $vehiculo->numero_motor,
            'origen_motor' => ($vehiculo->origen_motor == "") ? "SIN DATO" : $vehiculo->origen_motor,
            'uso_id' => $vehiculo->uso->uso_vehiculo,
            'combustible' => $vehiculo->combustible->tipo_combustible ??"SIN DATO" ,
            'puertas' => $vehiculo->numero_puertas,
            'fecha_leg' => ($vehiculo->fecha_documento_legalizacion == "0000-00-00") ? "-" : $vehiculo->fecha_documento_legalizacion,
            'pasajeros' => $vehiculo->numero_personas,
            'cilindros' => ($vehiculo->numero_cilindros == "") ? "-" : $vehiculo->numero_cilindros,
            'valor_factura' => $vehiculo->importe_factura,
            'lit' => ($vehiculo->capacidad_litros == "") ? "-" : $vehiculo->capacidad_litros,
            'bat' => ($vehiculo->capacidad_kwh == "") ? "-" : $vehiculo->capacidad_kwh,
            'repuve' => ($vehiculo->numero_repuve == "") ? "-" : $vehiculo->numero_repuve,
            'pais' => ($vehiculo->pais_id == 82) ? "MÉXICO" : $this->pais->findBy("id_pais", $vehiculo->pais_id)->pais,
            'motivo' => 'BAJA GENERAL',
            'folio_baja' => '-',
            "responsable" =>'CONSTANTINO CONTRERAS TORRES',
        );

        $nombre_pdf = $this->PDFC->hojaSeguridad($data);

        return view('Finalizar_cambio')
            ->with("pdf_holograma",$nombre_pdf )
            ->with("resultado", 'Linea de captura valida')
            ->with("placa", $mega->placa)
            ->with("imp_de", 'Hoja de baja')
            ->with("tramite", $tramite->id_tramite);


    }



}
