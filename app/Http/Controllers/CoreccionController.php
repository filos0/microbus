<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\VehiculosRepositorio as vehiculo;
use App\Repositories\TramiteRepositorio as tramite;
use App\Repositories\PersonaRepositorio as persona;
use App\Repositories\MegamicroRerpositorio as mega;
use App\Repositories\PlacaRerpositorio as placa;
use App\Repositories\ConcesionRepositorio as concesion;
use App\Repositories\Catalogos\CatClaseTipoVehiculoRepositorio as clase_tipo;
use App\Repositories\Catalogos\CatClaveVehicularRepositorio as clave;


class CoreccionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(vehiculo $vehiculo, tramite $tramite, persona $persona, mega $mega, placa $placa, clase_tipo $clase_tipo,
                                clave $clave, concesion $concesion)
    {
        $this->vehiculo = $vehiculo;
        $this->tramite = $tramite;
        $this->persona = $persona;
        $this->mega = $mega;
        $this->placa = $placa;
        $this->clase_tipo =$clase_tipo;
        $this->clave = $clave;
        $this->concesion = $concesion;
    }




    public function actualizar(Request $request)
    {

        //dd($request);


        $clave_vehicular = $this->clave->findBy('clave_vehicular', $request->clave_vehicular)->id_cat_clave_vehicular ?? 1;
        $lit = $request->lit ?? '0';
        $bat = $request->bat ?? '0';

        $mega = $this->mega->findBy('id_megamicro', $request->id_mega);

        if ($mega->estatus_sistemas == 1) {

            $this->vehiculo->update(array('serie_vehicular' => $request->serie,
                'fecha_alta' => date('Y-m-d H:i:s'),
                'modelo' => $request->ano,
                'numero_cilindros' => isset($request->cilindros) ? $request->cilindros : 0,
                'numero_motor' => $request->numero_motor,
                'capacidad_litros' => $lit,
                'capacidad_kwh' => $bat,
                'numero_personas' => $request->num_personas,
                'numero_puertas' => $request->num_puertas,
                'fecha_factura' => $request->fecha_factura,
                'importe_factura' => str_replace(',', '', $request->valor_factura),
                'numero_repuve' => $request->numero_repuve,
                'distribuidora' => $request->distribuidora,
                //  'aseguradora' => ,
                // 'numero_poliza_seguro' => '',
                'tipo_servicio_id' => 3,
                'uso_vehiculo_id' => 63,/////////////////////////////////////////////
                'clase_tipo_vehiculo_id' => 61,
                'tipo_combustible_id' => 6,
                'pais_id' => /*$request->procedencia == 1 ? 82 : $request->pais,*/
                    82,
                'estatus_id' => 1,
                'clave_vehicular_id' => $clave_vehicular), $request->serie, 'serie_vehicular');

            $vehiculo = $this->vehiculo->findBy('serie_vehicular', $request->serie);
        } else {
            $vehiculo = $this->vehiculo->create(array('serie_vehicular' => $mega->serie_vehicular,
                'fecha_alta' => date('Y-m-d H:i:s'),
                'modelo' => $request->modelo ?? $mega->modelo,
                'numero_cilindros' => isset($request->cilindros) ? $request->cilindros : 0,
                'numero_motor' => $request->numero_motor,
                'capacidad_litros' => $lit,
                'capacidad_kwh' => $bat,
                'numero_personas' => $request->num_personas,
                'numero_puertas' => $request->num_puertas,
                'fecha_factura' => $request->fecha_factura,
                'importe_factura' => str_replace(',', '', $request->valor_factura),
                'numero_repuve' => $request->numero_repuve,
                'distribuidora' => $request->distribuidora,
                //  'aseguradora' => ,
                // 'numero_poliza_seguro' => '',
                'tipo_servicio_id' => 3,
                'uso_vehiculo_id' => 63,/////////////////////////////////////////////
                'clase_tipo_vehiculo_id' => 61,
                'tipo_combustible_id' => 6,
                'pais_id' => /*$request->procedencia == 1 ? 82 : $request->pais,*/
                    82,
                'estatus_id' => 1,
                'clave_vehicular_id' => $clave_vehicular), $request->serie, 'serie_vehicular');

        }




        if ($mega->estatus_sistemas == 1) {
            $placa = $this->placa->findBy('placa', $mega->placa);
            $tramite = $this->tramite->ultimotramiteplaca($placa->placa);
            //   $concesion = $this->concesion->findBy('id_concesion', $placa->concesion_id);
        }

        else {
            $concesion = $this->concesion->create(array(
                'modalidad' => 'MICRO',
                'tipo' => $mega->tipo_servicio,
                'fecha_alta' => '0000-00-00',
                'fecha_expedicion' => '0000-00-00',
                'fecha_vencimiento' => '0000-00-00',
                'ruta' => $mega->ruta,
                'modulo_id' => 54,
                'tipo_concesion_id' => 1,
                'estatus' => 1

            ));


            $placa = $this->placa->create(array(
                'placa' => $mega->placa,
                'concesion_id' => $concesion->id_concesion,
                'placa_metropolitana' => $mega->placametro,
            ));
        }



        if ($mega->estatus_sistemas == 1){

            $persona = $this->persona->findBy('id_persona', $tramite->persona_id);

            $this->persona->update(array('nombre_razon_social' => $mega->nombre . $mega->razsoc,
                'primer_apellido' => $mega->paterno,
                'segundo_apellido' => $mega->materno,
                'curp' => '-',
                'rfc' => $mega->rfc,
                'tipo_identificacion_id' => 0,
                'pais_id' => 82,
                'fecha_nacimiento' => '0000-00-00',
                'telefono' => '-',
                'calle' => '-',
                'numero_exterior' => '-',
                'numero_interior' => '-',
                'id_colonia' => 1), 'id_persona', $persona->id_persona);


        }
        else
            $persona = $this->persona->create(array('nombre_razon_social' => $request->nombre,
                'primer_apellido' => $mega->primer_apellido,
                'segundo_apellido' => $mega->segundo_apellido,
                'rfc' => $request->rfc,
                'tipo_identificacion_id' => 0,
                'pais_id' => $request->nacionalidad,
                'fecha_nacimiento' => $request->fecha_nacimiento,
                'telefono' => $request->telefono,
                'calle' => $request->calle,
                'numero_exterior' => $request->numero_exterior,
                'numero_interior' => $request->numero_interior,
                'id_colonia' => 1));


        $tramite = $this->tramite->create(array(

            'fecha_tramite' => date('Y-m-d H:i:s'),
            'linea_captura' => $request->linea_captura,
            'importe_linea_captura' => 454656,
            'curp_solicitante' => $request->curp_solicitante ,
            'identificacion_solicitante' => $request->doc_solicitante,
            //'constancia' => $request->folio_mp ?? null,
            'costo_id' => 3,
            'persona_id' => $persona->id_persona,
            'vehiculo_id' => $vehiculo->id_vehiculo,
            'modulo_id' => 1,
            'users_id' => 555,
            'placa_id' => $placa->id_placa

        ));
        $this->mega->actualizarestatus($mega->placa, $mega->serie_vehicular);


        return view('Finalizar_cambio')
            ->with("pdf_holograma",'no' )
            ->with("resultado", 'Linea de captura valida')
            ->with("placa", $mega->placa)
            ->with("imp_de", 'Acttualización de datos')
            ->with("tramite", $tramite->id_tramite);




    }
}
