<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ConcesionRepositorio as concesion;
use App\Repositories\VehiculosRepositorio as vehiculo;
use App\Repositories\PersonaRepositorio as persona;
use App\Repositories\PersonaConcesionRepositorio as p_concesion;
use App\Repositories\PersonaVehiculoRepositorio as p_vehiculo;
use App\Repositories\Services\Lineas_Caprura as linea;
use App\Repositories\Catalogos\CatPaisVehiculoRepositorio as pais;
use App\Repositories\Catalogos\CatCombustibleVehiculoRepositorio as combustible;
use App\Repositories\Catalogos\CatClaseTipoVehiculoRepositorio as clase_tipo;
use App\Repositories\Catalogos\CatUsoVehiculoRepositorio as uso_vehiculo;
use App\Repositories\Catalogos\CatTipoIdentificacionRepositorio as tipo_identificacion;
use App\Repositories\MegamicroRerpositorio as mega;
use App\Repositories\PlacaRerpositorio as placa;
use App\Repositories\TramiteRepositorio as tramite;

class LineaCapturaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(concesion $concesion, vehiculo $vehiculo, linea $linea, p_concesion $p_concesion, uso_vehiculo $uso_vehiculo, tipo_identificacion $tipo_identificacion,
                                p_vehiculo $p_vehiculo, persona $persona, pais $pais, combustible $combustible, clase_tipo $clase_tipo, mega $mega,
                                placa $placa, tramite $tramite)
    {
        //$this->middleware('auth');
        $this->concesion = $concesion;
        $this->vehiculo = $vehiculo;
        $this->persona = $persona;
        $this->p_concesion = $p_concesion;
        $this->p_vehiculo = $p_vehiculo;
        $this->linea = $linea;
        $this->pais = $pais;
        $this->combustible = $combustible;
        $this->clase_tipo = $clase_tipo;
        $this->uso_vehiculo = $uso_vehiculo;
        $this->tipo_identificacion = $tipo_identificacion;
        $this->mega = $mega;
        $this->placa = $placa;
        $this->tramite = $tramite;
    }


    public function linea_captura()
    {
        return view('Linea_Captura');
    }

    public function linea_captura_cambio_unidad()
    {
        return view('Linea_Captura_Cambio_Unidad');
    }

    public function linea_captura_baja()
    {
        return view('Linea_Captura_Baja');
    }

    public function Comprobar(Request $request)
    {

        if ($request->linea_captura == '22222222222222222222') {

            return view('Correcion_Datos');
        }

    }

    public function comprobar_correcion(Request $request)
    {

        $mega = $this->mega->buscarporplaca($request->placa);

        if ($mega == 'Error')
            return redirect()->back()->withErrors(array('error', 'Error', 'Existen varios problemas con la informacion relacionada a esta placa'));

        elseif (!$mega)
            return redirect()->back()->withErrors(array('error', 'Sin datos', 'No existen datos, intentalo nuevamente'));

        if ($mega->estatus_sistemas == 1) {
            $placa = $this->placa->findBy('placa', $request->placa);
            if ($placa) {

                $tramite = $this->tramite->ultimotramiteplaca($placa);
                $vehiculo = $this->vehiculo->findby("id_vehiculo", $tramite->vehiculo_id);
                $persona = $this->persona->findby("id_persona", $tramite->persona_id);

                return view('Correcion_Datos')
                    ->with("pais", $this->pais->all())
                    ->with("vehiculo", $vehiculo)
                    ->with("propietario", $persona)
                    ->with("placa", $request->placa)
                    ->with("consecion", $mega);

            }

        } else

            return view('Correcion_Datos')
                ->with("pais", $this->pais->all())
                ->with("vehiculo", $mega)
                ->with("propietario", $mega)
                ->with("placa", $request->placa)
                ->with("consecion", $request->placa);

    }


    public function Comprobar_Cambio_Uunidad(Request $request)
    {
        $linea = $this->linea->consultar($request->linea_captura);

        $vehiculo_mega = $this->mega->buscarporserie($request->serie);
//dd($vehiculo_mega);
        if ($vehiculo_mega === 'Error')
            //dd($vehiculo_mega );
            return redirect()->back()->withErrors(array('error', 'Error', 'Existen varios problemas con la informacion relacionada a la serie vehicular'));

        elseif ($vehiculo_mega)
            return redirect()->back()->withErrors(array('error', 'Sin datos', 'El vehiculo nuevo ya se encuentra activo'));

        $vehiculo = $this->vehiculo->getVehiculoActivo($request->serie);

        if ($vehiculo === 1)
            return redirect()->back()->withErrors(array('error', 'Error', 'El vehiculo nuevo se encuentra activo'));

        $mega = $this->mega->buscarporplaca($request->placa);
        if ($mega === 'Error')
            return redirect()->back()->withErrors(array('error', 'Error', 'Existen varios problemas con la informacion relacionada a esta placa'));

        elseif (!$mega)
            return redirect()->back()->withErrors(array('error', 'Sin datos', 'No existen datos, intentalo nuevamente'));

        //dd($linea);

        $paises = $this->pais->all();
        $combustibles = $this->combustible->all();
        $clases = $this->clase_tipo->clases_vehiculo();
        $usos = $this->uso_vehiculo->all();
        $identificaciones = $this->tipo_identificacion->all();
        if ($vehiculo == null)

            return view('Cambio_Unidad')
                ->with('serie', $request->serie)
                ->with('mega', $mega)
                ->with('paises', $paises)
                ->with('clases', $clases)
                ->with('usos', $usos)
                ->with('combustibles', $combustibles)
                ->with('identificaciones', $identificaciones)
                ->with('linea', $request->linea_captura);
         elseif ($vehiculo == 2)
            return view('Cambio_Unidad')
                ->with('serie', $request->serie)
                ->with('mega', $mega)
                ->with('paises', $paises)
                ->with('usos', $usos)
                ->with('clases', $clases)
                ->with('combustibles', $combustibles)
                ->with('identificaciones', $identificaciones)
                ->with('linea', $request->linea_captura)
                ->with('vehiculo', $this->vehiculo->findBy('serie_vehicular', $request->serie));




    }

    public function comprobar_Baja(Request $request)
    {
//dd($request);
        $mega = $this->mega->buscarporplaca($request->placa);

        if ($mega === 'Error')
        return redirect()->back()->withErrors(array('error', 'Error', 'Existen varios problemas con la informacion relacionada a esta placa'));

    elseif (!$mega)
        return redirect()->back()->withErrors(array('error', 'Sin datos', 'No existen datos, intentalo nuevamente'));


        if ($mega->estatus_vehiculo === 'B')
            return redirect()->back()->withErrors(array('error', 'Vehiculo dado de baja', 'El vehiculo se encuentra deado de baja'));


        return view('baja')
            ->with('linea_pago', $request->linea_captura)
            ->with('todo', $mega)
            ->with('tipoidentificacion', $this->tipo_identificacion->allBy('tipo_identificacion'));

    }

    public function comprobar_reposicion(Request $request){

        $mega = $this->mega->buscarporplaca($request->placa);
        if ($mega === 'Error')
            return redirect()->back()->withErrors(array('error', 'Error', 'Existen varios problemas con la informacion relacionada a esta placa'));

        elseif (!$mega) {
            $placa = $this->placa->findBy('placa', $request->placa);
            if (!$placa)
                return redirect()->back()->withErrors(array('error', 'Sin datos', 'No existen datos, intentalo nuevamente'));
        }

        if ($mega->estatus_sistemas === 1) {

            $placa = $this->placa->findBy('placa', $request->placa);
            $tramite = $this->tramite->ultimotramite_dif_baja($placa);
            if ($tramite)
                return redirect()->back()->withErrors(array('error', 'Sin datos', 'No existen datos, intentalo nuevamente'));

            $vehiculo = $this->vehiculo->findby("id_vehiculo", $tramite->vehiculo_id);
            $persona = $this->persona->findby("id_persona", $tramite->persona_id);


        }
        else
        return view('Reposicion_Tarjeta')
            ->with('folio_mp', '')
            ->with('linea', $request->linea_captura)
            ->with('mega_id', $mega->id_megamicro)
            ->with('status', 'ok')
            ->with('tramite', 'informix')
            ->with('folio_tc', $mega->notc)
            ->with('fecha_exp', $mega->fecexped)
            ->with('vigencia_tc', '-')
            ->with('propietario', $mega->nombre.' '. $mega->razsoc.' '.$mega->paterno.' '.$mega->materno)
            ->with('vehiculo', $mega->serie_vehicular)
            ->with('tipoidentificacion', $this->tipo_identificacion->allBy('tipo_identificacion'));


    }
}
