<?php

namespace App\Http\Controllers;

use Request;
use Illuminate\Support\Facades\Input;

use App\Repositories\ConcesionRepositorio as concesion;
use App\Repositories\Catalogos\CatClaveVehicularRepositorio as ClaveVehicular;
use App\Repositories\Services\Clave_Vehicular as clave;
use App\Http\Controllers\PDFController as PDF;
use App\Repositories\LoteMaterialRepositorio as lote;
use App\Repositories\IncidencialoteRepositorio as incidencia;
use App\Repositories\Documento as documento;
use App\Repositories\TramiteRepositorio as tramite;

use App\Repositories\Catalogos\CatClaseTipoVehiculoRepositorio as clase_tipo;

class ApiController extends Controller
{
    public function __construct(concesion $concesion, clave $clave,ClaveVehicular $claveVehicular, PDF $pdf, lote $lote,
                                incidencia $incidencia, documento $documento, tramite $tramite, clase_tipo $clase_tipo)
    {
        $this->concesion = $concesion;
        $this->clave = $clave;
        $this->ClaveVehicular = $claveVehicular;
        $this->pdf = $pdf;
        $this->lotematerial = $lote;
        $this->incidencia = $incidencia;
        $this->documento = $documento;
        $this->tramite = $tramite;
        $this->clase_tipo = $clase_tipo;
    }

    public function tipo_vehiculo()
    {
        if (Request::ajax()) {

            $clave_clase = Input::get('clave_clase');
            $tipos = $this->clase_tipo->tipo_vehiculo($clave_clase);
            return $tipos;

        }
    }

    public function listaconcesion()
    {

        if (Request::ajax()) {

            $concesion = Input::get('concesion');
            $concesiones = $this->concesion->concecionesactivas($concesion);
            if ($concesiones == false) {
                return 0;
            } else {

                if (count($concesiones) > 1) {
                    // dd(count($concesiones));

                    return $concesiones;
                } else
                    return $concesiones[0]->id_concesion;

            }


        }
    }

    public function concesion()
    {

        if (Request::ajax()) {

            $concesion = Input::get('concesion');
            $concesiones = $this->concesion->findBy('id_concesion', $concesion);
            if ($concesiones == false) {
                return 0;
            } else {

                return $concesiones;

            }


        }
    }


    public
    function getClaveVehicular()
    {
        if (Request::ajax()) {

            $clave = Input::get('clave_vehicular');
            $vehiculo = $this->ClaveVehicular->findBy('clave_vehicular', $clave);

            if ($vehiculo == false) {
                $vehiculo = $this->clave->consultar($clave);

                if (is_int($vehiculo)) {
                    return false;
                }

                $marca = $vehiculo->descripcionMarca;
                $linea = $vehiculo->descripcionLinea;
                $version = $vehiculo->descripcionModelo;
                $digito = $vehiculo->claveDigito;

                if ($marca == 'null' && $linea == 'null' && $version == 'null' && $digito == 'null') {
                    return 0;
                } else {

                    $claveVeh = $this->ClaveVehicular->create(['clave_vehicular' => $clave, 'digito' => $digito, 'marca' => $marca, 'linea' => $linea, 'version' => $version]);

                    return $claveVeh;

                }


            } else {
                return $vehiculo;

            }

        }

    }

    public
    function PDF_Confirmar_Datos()
    {
        if (Request::ajax()) {



            $dd = Input::get('data');
            parse_str($dd, $array);
            $nombre_pdf = $this->pdf->confirmarDatos($array);
            return '/' . $nombre_pdf;

        }
    }


    function validar_folio_tarjeta()
    {


        if (Request::ajax()) {
            $folio_holograma = strtoupper(Input::get('folio_holograma'));
            $lote = $this->lotematerial->lotetarjetasdisponibles(1);
            $ocupado = $this->documento->findBy('folio_documento', $folio_holograma);
            $inc = $this->incidencia->findBy('folio_material', $folio_holograma);

            if ($ocupado)
                return $data[0] ="folio_ocupado";
            if ($inc)
                return $data[0] ="folio_ocupado";

            if ($lote == null) {
                return false;

            } else {
                $serie = $lote->serie;
                $rango_in = $lote->numero_rango_inicio;
                $rango_fin = $lote->numero_rango_fin;
                //$tarjetas = array();
                $a = 0;

                for ($i = $rango_in; $i <= $rango_fin; $i++) {
                    $n = $rango_in + $a;

                    if ($n <= 999999 && $n > 99999)
                        $tarjetas[$a] = $serie . $n;

                    elseif ($n <= 99999 && $n > 9999)
                        $tarjetas[$a] = $serie . '0' . $n;

                    elseif ($n <= 9999 && $n > 999)
                        $tarjetas[$a] = $serie . '00' . $n;

                    elseif ($n <= 999 && $n > 99)
                        $tarjetas[$a] = $serie . '000' . $n;

                    elseif ($n <= 99 && $n > 9)
                        $tarjetas[$a] = $serie . '0000' . $n;

                    else
                        $tarjetas[$a] = $serie . '00000' . $n;

                    $a++;
                }

                if (in_array($folio_holograma, $tarjetas)) {
                    $data[0] ="folio_disponible";
                    $data[1] = $lote->id_lote_material;
                    return $data;
                } else {
                    return $data[0] ="no_hay_lote";
                }


            }


        }
    }

    public function TC(){
        if (Request::ajax()) {

            $folio_holograma = strtoupper(Input::get('folio_holograma'));
            $impresion_estatus = Input::get('impresion_estatus');
            $lote = Input::get('id_lote');
            $tramite = Input::get('tramite');

            if ($impresion_estatus == "SI") {
                $this->documento->create([
                    'tipo_documento_id' => 2,
                    'lote_material_id' => $lote,
                    'estatus_id' => 1,
                    'folio_documento' => $folio_holograma,
                    'fecha_documento' => date("Y-m-d H:i:s"),
                    'vigencia_documento' => date("Y-m-d H:i:s", strtotime('+1 year')),
                    'fecha_expedido' => date("Y-m-d H:i:s"),
                    'numero_tarjeta' => '10'.$tramite,
                    'modulo_id' => 10,
                    'tramite_id' => $tramite

                ]);

                $this->tramite->update(['cuenta_generacion_pdf' => 1], $tramite, 'id_tramite');


                return 'tc';

            } else {
               $this->incidencia->create([

                    'folio_material' => $folio_holograma,
                    'fecha' => date("Y-m-d H:i:s"),
                    'folio_orden_administrativo' => "",
                    'lote_material_id' => $lote,
                    'tramite_id' => $tramite,

                ]);
                return "incidencia";
            }

        }
    }


    public
    function PDF_Baja()
    {
        if (Request::ajax()) {

            $dd = Input::get('data');
            parse_str($dd, $array);
            $nombre_pdf = $this->pdf->confirmarDatosBAJA($array);
            return '/' . $nombre_pdf;

        }
    }



}
