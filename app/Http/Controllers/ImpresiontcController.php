<?php

namespace App\Http\Controllers;

use App\Repositories\ConcesionRepositorio;
use Illuminate\Http\Request;
use App\Repositories\ConcesionRepositorio as concesion;
use App\Repositories\VehiculosRepositorio as vehiculo;
use App\Repositories\PersonaRepositorio as persona;
use App\Repositories\TramiteRepositorio as tramite;
use App\Repositories\PersonaConcesionRepositorio as p_concesion;
use App\Repositories\PersonaVehiculoRepositorio as p_vehiculo;
use App\Repositories\Catalogos\CatClaveVehicularRepositorio as clave;
use App\Repositories\Catalogos\CatClaseTipoVehiculoRepositorio as clase;
use App\Repositories\PlacaRerpositorio as placa;
use App\Repositories\MegamicroRerpositorio as mega;
use App\Repositories\Catalogos\CatCosto as costo;
use Dompdf\Dompdf;
use Dompdf\Options;


class ImpresiontcController extends Controller
{
    public function __construct(vehiculo $vehiculo, clave $clave, p_concesion $p_concesion, p_vehiculo $p_vehiculo,
                                concesion $concesion, persona $persona, tramite $tramite, clase $clase, placa $placa,
                                mega $mega, costo $costo)
    {
        $this->vehiculo = $vehiculo;
        $this->persona = $persona;
        $this->concesion = $concesion;
        $this->tramite = $tramite;
        $this->clave = $clave;
        $this->p_concesion = $p_concesion;
        $this->p_vehiculo = $p_vehiculo;
        $this->clase = $clase;
        $this->placa = $placa;
        $this->mega = $mega;
        $this->costo = $costo;
    }

    public function index()
    {

        return view('busca_placa');

    }

    public function index_reposicion()
    {

        return view('Linea_Captura_reposicion');

    }

    public function placa(Request $request)
    {

        $placa = $this->placa->findBy('placa', $request->placa);

        if (!$placa)
            return redirect()->back()->withErrors(array('error', 'No se encuentran datos de la placa' . $request->placa, ';)'));

        $tramite = $this->tramite->ultimotramite_dif_baja($placa->id_placa);


        if ($tramite) {

            if ($tramite->cuenta_generacion_pdf >= 0 && $tramite->cuenta_generacion_pdf != null) {
                return redirect()->back()->withErrors(array('info', 'Ya se imprimio la Tarjeta de circulacion de ' . $request->placa, ';)'));

            }


            $vehiculo = $this->vehiculo->findby("id_vehiculo", $tramite->vehiculo_id);
            $persona = $this->persona->findby("id_persona", $tramite->persona_id);
            $mega = $this->mega->placaanterior($placa->placa);
//dd($mega);
            $clave_v = $vehiculo->clave_vehicular;
            $clase_tipo = $this->clase->all()[random_int(0, 35)];
            //dd($tramite->operador);

            $data = array(

                'tc' => $tramite->modulo_id . $tramite->id_tramite,
                'placa' => $placa->placa,
                'placa_anterior' => $mega->placa_anterior,
                'clave' => $persona->rfc,
                'nombre' => $persona->primer_apellido . " " . $persona->segundo_apellido . " " . $persona->nombre_razon_social,
                'fecha_nacimiento' => $persona->fecha_nacimiento == "0000-00-00" ? "-" : $persona->fecha_nacimiento,
                'sexo' => $persona->sexo,
                'tramite' => $this->costo->findBy('id_costo', $tramite->costo_id)->tipo_tramite->tipo_tramite,
                'modulo' => 'INFORMATICA',
                'fecha' => date('Y-m-d'),
                'serie' => $vehiculo->serie_vehicular,
                'clase' => $clase_tipo->clave_clase,
                'modelo' => $vehiculo->modelo,
                'tipo' => $clase_tipo->clave_tipo,
                'clave_vehicular' => $clave_v->clave_vehicular,
                'marca' => $clave_v->marca,
                'linea' => $clave_v->linea,
                'numero_motor' => ($vehiculo->numero_motor == "") ? "SIN DATO" : $vehiculo->numero_motor,
                'origen_motor' => ($vehiculo->origen_motor == "") ? "SIN DATO" : $vehiculo->origen_motor,
                'uso_id' => $vehiculo->uso_vehiculo_id,
                'combustible' => $vehiculo->tipo_combustible_id,
                'puertas' => $vehiculo->tipo_combustible_id,
                'legalizacion' => '---',
                'pasajeros' => $vehiculo->numero_personas,
                'cilindros' => $vehiculo->numero_cilindros,
                'lit' => ($vehiculo->capacidad_litros == "") ? "-" : $vehiculo->capacidad_litros,
                'bat' => ($vehiculo->capacidad_kwh == "") ? "-" : $vehiculo->capacidad_kwh,
                'repuve' => ($vehiculo->numero_repuve == "") ? "-" : $vehiculo->numero_repuve,
                'pais' => ($vehiculo->pais_id == 82) ? "MÉXICO" : "MÉXICO",//$this->pais->findBy("id_pais"$vehiculo->,
                "responsable" => $tramite->operador->rfc
            );


            return view('Finalizar')
                ->with("pdf_holograma", $this->pdf_tc($data))
                ->with("resultado", 'Linea de captura valida')
                ->with("placa", $request->placa)
                ->with("imp_de", 'tarjeta de circulación')
                ->with("tramite", $tramite->id_tramite);


        } else
            return redirect()->back()->withErrors(array('error', 'No se encuentra un tramite ligado a ' . $request->placa, ':('));

    }


    public function reposicion(Request $request)
    {
//dd($request);




        if (isset($request->mega_id)) {
            $mega = $this->mega->findBy('id_megamicro',$request->mega_id);

            $clase = $this->clase->findBy('tipo', $mega->tipo);
            $clave = $this->clave->findBy('clave_vehicular', $mega->clave_vehicular);

            $vehiculo = $this->vehiculo->create(array('serie_vehicular' => $mega->serie_vehicular,
                'fecha_alta' => date('Y-m-d'),
                'modelo' => $mega->modelo,
                'numero_cilindros' => $mega->cilindros ?? '-',
                'numero_motor' => $mega->numero_motor,
                'capacidad_litros' => '-',
                'capacidad_kwh' => '-',
                'numero_personas' => $mega->personas ?? '-',
                'numero_puertas' => '-',
                'folio_documento_legalizacion' => '-',
                'fecha_documento_legalizacion' => '0000-00-00',
                'fecha_factura' => '0000-00-00',
                'numero_factura' => '-',
                'importe_factura' => '-',
                'numero_repuve' => $mega->repuve ?? '-',
                'distribuidora' => '-',
                'aseguradora' => '-',
                'numero_poliza_seguro' => '-',
                'tipo_servicio_id' => 3,
                'uso_vehiculo_id' => 63,
                'clase_tipo_vehiculo_id' => $clase != false ? $clase->id_cat_clase_tipo_vehiculo : 93,
                'tipo_combustible_id' => 6,
                'pais_id' => 82,
                'estatus_id' => 2,
                'clave_vehicular_id' => $clave != false ? $clave->id_cat_clave_vehicular : 1));


            $persona = $this->persona->create(array('nombre_razon_social' => $mega->nombre . $mega->razsoc,
                'primer_apellido' => $mega->paterno,
                'segundo_apellido' => $mega->materno,
                'curp' => '-',
                'rfc' => $mega->rfc,
                'tipo_identificacion_id' => 0,
                'pais_id' => 82,
                'fecha_nacimiento' => '0000-00-00',
                'telefono' => '-',
                'calle' => '-',
                'numero_exterior' => '-',
                'numero_interior' => '-',
                'id_colonia' => 1));

            $concesion = $this->concesion->create(array(
                'modalidad' => 'MICRO',
                'tipo' => $mega->tipo_servicio,
                'fecha_alta' => '0000-00-00',
                'fecha_expedicion' => '0000-00-00',
                'fecha_vencimiento' => '0000-00-00',
                'ruta' => $mega->ruta,
                'modulo_id' => 54,
                'tipo_concesion_id' => 1,
                'estatus' => 1

            ));


            $placa = $this->placa->create(array(
                'placa' => $mega->placa,
                'concesion_id' => $concesion->id_concesion,
                'placa_metropolitana' => $mega->placametro,
            ));

            $this->mega->actualizarestatus($mega->placa, $mega->serie_vehicular);
        }

        else {

            $placa = $this->placa->findBy('placa', $request->placa);
            $tramite = $this->tramite->ultimotramite_dif_baja($placa);
            $vehiculo = $this->vehiculo->findby("id_vehiculo", $tramite->vehiculo_id);
            $persona = $this->persona->findby("id_persona", $tramite->persona_id);

        }


        $tramite = $this->tramite->create(array(

            'fecha_tramite' => date('Y-m-d H:i:s'),
            'linea_captura' => $request->linea_captura,
            'importe_linea_captura' => 454656,
            'curp_solicitante' => $request->curp_solicitante ,
            'identificacion_solicitante' => $request->doc_solicitante,
            //'constancia' => $request->folio_mp ?? null,
            'costo_id' => 5,
            'persona_id' => $persona->id_persona,
            'vehiculo_id' => $vehiculo->id_vehiculo,
            'modulo_id' => 1,
            'users_id' => 555,
            'placa_id' => $placa->id_placa));



        return view('Finalizar_cambio')
            ->with("pdf_holograma",'no' )
            ->with("resultado", 'Linea de captura valida')
            ->with("placa", $placa->placa)
            ->with("imp_de", 'Reposición de TC')
            ->with("tramite", $tramite->id_tramite);




    }


    function pdf_tc($data)
    {

        $data = array_map('mb_strtoupper', $data);
        $options = new Options();
        $options->set('isRemoteEnabled', TRUE);
        $dompdf = new Dompdf($options);
        $view = \View::make('PDF/TarjetaCirculacion', compact('data', 'date', 'invoice'))->render();

        $dompdf->loadHTML($view);
        $customPaper = array(0, 0, 240, 150);
        $dompdf->setPaper($customPaper, 'portrait');
        $dompdf->render();
        $output = $dompdf->output();
        $nombre_pdf = $data['placa'] . '.pdf';
        file_put_contents($nombre_pdf, $output);
        return $nombre_pdf;
    }

}
