<?php

namespace App\Http\Controllers;


use Auth;

use App\Repositories\Catalogos\CatCombustibleVehiculoRepositorio as combustible;
use App\Repositories\Catalogos\CatPaisVehiculoRepositorio as pais;
use App\Repositories\Catalogos\CatClaseTipoVehiculoRepositorio as clase_tipo;
use App\Repositories\Catalogos\CatUsoVehiculoRepositorio as uso_vehiculo;
use App\Repositories\VehiculosRepositorio as vehiculo;
use App\Repositories\TramiteRepositorio as tramite;

class PDFController extends Controller
{

    public function __construct(pais $pais, combustible $combustible, clase_tipo $clase_tipo, uso_vehiculo $uso_vehiculo,
                                vehiculo $vehiculo, tramite $tramite)
    {
        $this->pais = $pais;
        $this->combustible = $combustible;
        $this->uso_vehiculo = $uso_vehiculo;
        $this->clase_tipo = $clase_tipo;
        $this->Vehiculo = $vehiculo;
        $this->Tramite = $tramite;

    }

    public function confirmarDatos($datos)
    {
//dd($datos);
        $domicilio = $datos["calle"] ?? '' . ", Número Exterior " . $datos["num_exterior"] ?? ''. ", Número Interior" . $datos["num_interior"]?? '';

        $datos["primer_apellido_solicitante"] =$datos["primer_apellido_solicitante"] ?? $datos["primer_apellido"];
        $datos["segundo_apellido_solicitante"] = $datos["segundo_apellido_solicitante"] ?? $datos["segundo_apellido"];
        $datos["nombre_solicitante"] = $datos["nombre_solicitante"] ?? $datos['nombre'];

        $data = array(
            'tipo_movimiento' => 'CAMBIO DE UNIDAD',
            'placa_id' => "-",
            'placa_anterior' => "-",

            'nombre' => $datos["nombre"],
            'clave_unica' => $datos["curp"] ?? $datos["rfc"],
            'fecha_nacimiento' => $datos["fecha_nacimiento"],
            'sexo' => $datos["sexo"],
            'calle' => $domicilio,
            'colonia' => $datos["colonia"],
            'cp' => $datos["cp"] ?? '-',
            'delegacion' => $datos["delegacion"],
            'telefono' => $datos["telefono"],

            'entidad' => 'CIUDAD DE MÉXICO',
            'serie' => $datos["serie"],

            'clase' => $this->clase_tipo->findBy("id_cat_clase_tipo_vehiculo", $datos["clase_tipo_vehiculo_id"] ?? 0)->clase ?? $datos['clase'],
            'tipo' => $this->clase_tipo->findBy("id_cat_clase_tipo_vehiculo", $datos["clase_tipo_vehiculo_id"] ?? 0)->tipo ?? $datos['tipo'],
            'uso_id' => $this->uso_vehiculo->findBy("id_uso_vehiculo", $datos["uso_vehiculo"] ?? 0)->uso_vehiculo ?? $datos['uso'],


            'marca' => $datos["marca"],
            'linea' => $datos["linea"],

            'numero_motor' => $datos["numero_motor"],
            'origen_motor' => 'SIN DATO',

            'combustible' => $this->combustible->findBy("id_tipo_combustible", $datos["combustible"]??0)->tipo_combustible ?? 'SIN DATO',
            'puertas' => $datos["puertas"] ?? 'SIN DATO',
            'fecha_leg' => '-',
            'pasajeros' => $datos["pasajeros"] ?? 'SIN DATO',
            'clave_vehicular' => $datos["clave_vehicular"],
            'lit' => $datos["lit"] ?? 'SIN DATO',
            'valor_factura' => $datos["valor_factura"],
            'bat' => 'SIN DATO',
            'repuve' => $datos["repuve"] ?? 'SIN DATO',
            'pais' => 'SIN DATO',
            "responsable" => "MACA9402199J9",
            'nombre_solicitante' => $datos["primer_apellido_solicitante"] ?? $datos["primer_apellido"]. " " . $datos["segundo_apellido_solicitante"] ?? $datos["segundo_apellido"]. " " . $datos["nombre_solicitante"] ?? $datos['nombre']
        );


        $data = array_map('mb_strtoupper', $data);
        $view = \View::make('PDF/ConfirmarDatos', compact('data'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $output = $pdf->output();
        $numerito = random_int(1, 5000);
        $nombre_pdf = $numerito . '.pdf';
        file_put_contents($nombre_pdf, $output);
        return $nombre_pdf;
        //return view('Contenedor_PDF')->with('pdf', $nombre_pdf);

    }

    public function Vale_Placa($placa, $modulo, $serie_vehicular, $folio_tramite, $operador)
    {

        $view = \View::make('PDF/ValeTC', compact('placa','modulo','serie_vehicular','folio_tramite','operador'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $output = $pdf->output();
        $numerito = random_int(1, 5000);
        $nombre_pdf = $numerito . '.pdf';
        file_put_contents($nombre_pdf, $output);
        return $nombre_pdf;
        //return $pdf->stream();
    }


    public function confirmarDatosBAJA($data)
    {
        //d($data);

        $vehiculo = $this->Vehiculo->findBy('serie_vehicular', $data["serie"]);
        if ($vehiculo) {
            $tramite = $this->Tramite->tramiteplacaactiva($vehiculo->id_vehiculo);
            $propietario = $tramite->propietario;
        }
    else{
            $vehiculo = json_decode($data['todo']);
            $tramite = $vehiculo;
            $propietario = $vehiculo;
        }



        if ($propietario->sexo == 'M') {
            $propietario->sexo = "Masculino";
        } elseif ($propietario->sexo == 'F') {
            $propietario->sexo = "Femenino";
        } else {
            $propietario->sexo = "-";
        }





        $data = array(

            'clave_unica' => $propietario->rfc,
            'nombre' => $propietario->paterno . " " . $propietario->materno . " " . $propietario->nombre. ''. $propietario->razsoc,
            'fecha_nacimiento' => $propietario->fecnac ?? '-',
            'sexo' => $propietario->sexo,
            'calle' => "SIN DATO",
            'colonia' => "SIN DATO",
            'delegacion' =>"SIN DATO",
            'cp' => "SIN DATO",
            'entidad' => 'Ciudad de México',
            'telefono' => $propietario->telefono ?? "-" ,
            'tipo_movimiento' => $data['tipo_movimiento'],
            'serie' => $vehiculo->serie_vehicular,
            'clase' => $vehiculo->clase,
            'modelo' => $vehiculo->modelo,
            'tipo' => $vehiculo->tipo,
            'clave_vehicular' => $vehiculo->clave_vehicular,
            'marca' => $vehiculo->marca,
            'linea' => $vehiculo->linea,
            'placa_id' => $vehiculo->placa,
            'placa_anterior' => $vehiculo->placa_anterior,
            'numero_motor' => $vehiculo->numero_motor  ?? "SIN DATO" ,
            'origen_motor' => $vehiculo->origen_motor ?? "-",
            'uso_id' => $vehiculo->uso,
            'combustible' => "SIN DATO",
            'puertas' => "SIN DATO",
            'fecha_leg' => '-',
            'pasajeros' => $vehiculo->personas ?? '-',
            'poliza' => "SIN DATO",
            'cilindros' => $vehiculo->numero_cilindros ?? "-" ,
            'valor_factura' => "SIN DATO",
            'lit' => "SIN DATO",
            'bat' => "SIN DATO",
            'repuve' => "SIN DATO",
            'pais' => ($vehiculo->origen == 'NACIONAL') ? "MÉXICO" : "SIN DATO",
            'nombre_solicitante' => $data["primer_apellido_solicitante"] . ' ' . $data["segundo_apellido_solicitante"] . ' ' . $data["nombre_solicitante"],
            "responsable" => 'MACA9402199J9',
        );

        $data = array_map('mb_strtoupper', $data);
        $view = \View::make('PDF/ConfirmarDatos', compact('data', 'invoice'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $output = $pdf->output();
        $nombre_pdf = $vehiculo->placa . '.pdf';
        file_put_contents($nombre_pdf, $output);
        return $nombre_pdf;
        return view('Contenedor_PDF')->with('pdf', $nombre_pdf);

    }


    public function confirmarDatosActualizacion($data)
    {
        dd($data);

        $vehiculo = $this->Vehiculo->findBy('serie_vehicular', $data["serie"]);
        if ($vehiculo) {
            $tramite = $this->Tramite->tramiteplacaactiva($vehiculo->id_vehiculo);
            $propietario = $tramite->propietario;
        }
        else{
            $vehiculo = json_decode($data['todo']);
            $tramite = $vehiculo;
            $propietario = $vehiculo;
        }



        if ($propietario->sexo == 'M') {
            $propietario->sexo = "Masculino";
        } elseif ($propietario->sexo == 'F') {
            $propietario->sexo = "Femenino";
        } else {
            $propietario->sexo = "-";
        }





        $data = array(

            'clave_unica' => $propietario->rfc,
            'nombre' => $propietario->paterno . " " . $propietario->materno . " " . $propietario->nombre. ''. $propietario->razsoc,
            'fecha_nacimiento' => $propietario->fecnac ?? '-',
            'sexo' => $propietario->sexo,
            'calle' => "SIN DATO",
            'colonia' => "SIN DATO",
            'delegacion' =>"SIN DATO",
            'cp' => "SIN DATO",
            'entidad' => 'Ciudad de México',
            'telefono' => $propietario->telefono ?? "-" ,
            'tipo_movimiento' => $data['tipo_movimiento'],
            'serie' => $vehiculo->serie_vehicular,
            'clase' => $vehiculo->clase,
            'modelo' => $vehiculo->modelo,
            'tipo' => $vehiculo->tipo,
            'clave_vehicular' => $vehiculo->clave_vehicular,
            'marca' => $vehiculo->marca,
            'linea' => $vehiculo->linea,
            'placa_id' => $vehiculo->placa,
            'placa_anterior' => $vehiculo->placa_anterior,
            'numero_motor' => $vehiculo->numero_motor  ?? "SIN DATO" ,
            'origen_motor' => $vehiculo->origen_motor ?? "-",
            'uso_id' => $vehiculo->uso,
            'combustible' => "SIN DATO",
            'puertas' => "SIN DATO",
            'fecha_leg' => '-',
            'pasajeros' => $vehiculo->personas ?? '-',
            'poliza' => "SIN DATO",
            'cilindros' => $vehiculo->numero_cilindros ?? "-" ,
            'valor_factura' => "SIN DATO",
            'lit' => "SIN DATO",
            'bat' => "SIN DATO",
            'repuve' => "SIN DATO",
            'pais' => ($vehiculo->origen == 'NACIONAL') ? "MÉXICO" : "SIN DATO",
            'nombre_solicitante' => $data["primer_apellido_solicitante"] . ' ' . $data["segundo_apellido_solicitante"] . ' ' . $data["nombre_solicitante"],
            "responsable" => 'MACA9402199J9',
        );

        $data = array_map('mb_strtoupper', $data);
        $view = \View::make('PDF/ConfirmarDatos', compact('data', 'invoice'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $output = $pdf->output();
        $nombre_pdf = $vehiculo->placa . '.pdf';
        file_put_contents($nombre_pdf, $output);
        return $nombre_pdf;
        return view('Contenedor_PDF')->with('pdf', $nombre_pdf);

    }



    public function hojaSeguridad($data)
    {

            $date = date('Y-m-d');
            $invoice = "2222";
            $data = array_map('mb_strtoupper', $data);
            $view = \View::make('PDF/HojaSeguridad', compact('data', 'date', 'invoice'))->render();
            $pdf = \App::make('dompdf.wrapper');
            $pdf->loadHTML($view);
            $output = $pdf->output();
            $nombre_pdf = random_int(1, 5000) . '.pdf';
            file_put_contents($nombre_pdf, $output);
            return $nombre_pdf;
            //return view('Contenedor_PDF')->with('pdf', $nombre_pdf)->with('id_tramite', $tramite_baja->id_tramite);


    }




}
