<?php

namespace App\Models\Carga;

use Illuminate\Database\Eloquent\Model;

class Incidenciasmodel extends Model
{
    protected $connection = 'mysql_carga';

    protected $table = 'incidencia_lote';

    protected $fillable = ['folio_material', 'fecha', 'folio_orden_administrativa', 'lote_material_id','tramite_id'];

    protected $primaryKey = 'id_incidencia_lote';

    public $timestamps = false;

}
