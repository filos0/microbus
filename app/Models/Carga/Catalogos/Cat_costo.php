<?php

namespace App\Models\Carga\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_costo extends Model
{
    protected $connection = 'mysql_carga';

    protected $table = 'cat_costo';

    protected $fillable = ['id_costo', 'costo', 'anio_fiscal', 'tipo_tramite_id', 'tipo_vehiculo_pago_id', 'estatus_id',];

    protected $primaryKey = 'id_costo';

    public $timestamps = false;

    public function tramites(){
        return $this->hasMany('App\Models\Tramite');
    }

    public function tipo_tramite(){
        return $this->belongsTo('App\Models\Catalogos\Cat_tipo_tramite', 'tipo_tramite_id');
    }

    public function tipo_vehiculo_costo(){
        return $this->belongsTo('App\Models\Catalogos\Cat_tipo_vehiculo_pago', 'tipo_vehiculo_pago_id');
    }


}
