<?php

namespace App\Models\Carga\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_clave_vehicular extends Model
{
    protected $connection = 'mysql_carga';

    protected $table = 'cat_clave_vehicular';

    protected $fillable = ['clave_vehicular', 'digito', 'marca','linea','version'];

    protected $primaryKey = 'id_cat_clave_vehicular';

    public function vehiculo()
    {
        return $this->hasMany('App\Models\Catalogos\Vehiculo');

    }

}
