<?php

namespace App\Models\Carga\Catalogos;

use Illuminate\Database\Eloquent\Model;

class CatMotivoBajaAutomovil extends Model
{

    protected $connection = 'mysql_carga';

    protected $table = 'cat_motivo_baja_automovil';
}
