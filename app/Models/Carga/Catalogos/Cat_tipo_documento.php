<?php

namespace App\Models\Carga\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_tipo_documento extends Model
{
    protected $connection = 'mysql_carga';

    protected $table = 'cat_tipo_documento';

    protected $fillable = ['id_tipo_documento', 'tipo_documento',];

    public $timestamps = false;
}
