<?php

namespace App\Models\Carga\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_modulos extends Model
{
    protected $connection = 'mysql_carga';

    protected $table = 'cat_modulo';

     public $timestamps = false;

    protected $fillable = ['clave_modulo', 'modulo', 'cat_estatus_id'];

    protected $primaryKey = 'id_modulo';


    public function placa(){

        return $this->hasMany('App\Models\Placas');
    }

    public function user(){

        return $this->hasMany('App\Models\User');
    }

    public function lote_material(){

        return $this->hasMany('App\Models\LoteMaterial');
    }
    public function documento(){

        return $this->hasMany('App\Models\Documento');
    }


}
