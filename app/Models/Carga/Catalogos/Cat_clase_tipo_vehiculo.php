<?php

namespace App\Models\Carga\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_clase_tipo_vehiculo extends Model
{
    protected $connection = 'mysql_carga';

    protected $table = 'cat_clase_tipo_vehiculo';

    protected $fillable = ['clave_clase', 'clase', 'clave_tipo','tipo'];

    protected $primaryKey = 'id_cat_clase_tipo_vehiculo';

    public function vehiculo()
    {
        return $this->hasMany('App\Models\Catalogos\Vehiculo');

    }


}