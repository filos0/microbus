<?php

namespace App\Models\Carga\Catalogos;

use Illuminate\Database\Eloquent\Model;

class CatTipoCombustible extends Model
{
    protected $connection = 'mysql_carga';

    protected $table = 'cat_tipo_combustible';

    protected $fillable = ['id_tipo_combustible', 'clave_tipo_combustible', 'tipo_combustible'];

    protected $timestamp = false;

}
