<?php

namespace App\Models\Carga\Catalogos;

use Illuminate\Database\Eloquent\Model;

class CatStatus extends Model
{
    protected $connection = 'mysql_carga';

    protected $table = 'cat_Status';

    protected $fillable = ['nombre', 'descripcion'];

    public function placa(){

        return $this->hasMany('App\Models\Placas');
    }

    public function vehiculo(){
        return$this->hasMany('App\Models\Vehiculo');

    }

    public function domicilio(){
        return$this->hasMany('App\Models\Domicilio');

    }
}
