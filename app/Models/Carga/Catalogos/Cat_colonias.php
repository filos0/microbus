<?php

namespace App\Models\Carga\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_colonias extends Model
{
    protected $connection = 'mysql_carga';

    protected $table = 'cat_colonia';

    protected $fillable = ['cp', 'colonia', 'delegacion','entidad_federativa'];

    protected $primaryKey = 'id_colonia';



    public function propietario()
    {

        return $this->hasMany('App\Models\Propietario');
    }



}
