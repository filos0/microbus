<?php

namespace App\Models\Carga\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_tipo_identificacion extends Model
{
    protected $connection = 'mysql_carga';

    protected $table ='cat_tipo_identificacion';

    protected $fillable = ['clave_tipo_identificacion', 'tipo_identificacion'];

    protected $primaryKey = 'id_tipo_identificacion';

    public function propietario(){

        return $this->hasMany('App\Models\Propietario');

    }
}
