<?php

namespace App\Models\Carga\Catalogos;
use Illuminate\Database\Eloquent\Model;

class CatDocLegalVehiculo extends Model
{
    protected $connection = 'mysql_carga';

    protected $table = 'cat_tipo_documento';

    protected $primaryKey = 'id_tipo_documento';
}
