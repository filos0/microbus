<?php

namespace App\Models\Carga;

use Illuminate\Database\Eloquent\Model;

class Propietario extends Model
{
    protected $connection = 'mysql_carga';

    protected $table = 'propietario';

    protected $fillable = ['nombre_razon_social', 'primer_apellido', 'segundo_apellido', 'clave_unica', 'tipo_identificacion_id',
        'pais_id', 'id_colonia', 'fecha_nacimiento', 'sexo', 'telefono', 'tipo_propietario_id','calle', 'numero_exterior',
        'numero_interior'];

    protected $primaryKey = 'id_propietario';

    public $timestamps = false;


    public function tramites()
    {
        return $this->hasMany('App\Models\Tramite');
    }

    public function mini()
    {
        return $this->hasMany('App\Models\minimodel','id');
    }

    public function mega()
    {
        return $this->hasOne('App\Models\Tramite', 'rfc', 'clave_unica');
    }

    public function colonia(){

        return $this->belongsTo('App\Models\Catalogos\Cat_colonias', 'id_colonia');
    }

    public function pais(){

        return $this->belongsTo('App\Models\Catalogos\Cat_pais', 'pais_id');

    }
    public function identificacion(){

        return $this->belongsTo('App\Models\Catalogos\Cat_tipo_identificacion', 'tipo_identificacion_id');

    }


}
