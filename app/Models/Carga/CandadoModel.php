<?php

namespace App\Models\Carga;

use Illuminate\Database\Eloquent\Model;

class CandadoModel extends Model
{
    protected $connection = 'mysql_carga';

    protected $table = 'candado';

    protected $fillable = ['serievh', 'placa', 'feccand', 'oficio_no', 'oficio_nob', 'usr_candado', 'usr_candadob', 'fecbaja', 'estatus'];


    public $timestamps = false;
}
