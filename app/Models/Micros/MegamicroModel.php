<?php

namespace App\Models\Micros;

use Illuminate\Database\Eloquent\Model;

class MegamicroModel extends Model
{

    protected $table = 'megamicro2';

    protected $primaryKey ='id_megamicro';

    protected $fillable = ['id_megamicro','marca','linea','version','modelo','serie_vehicular','placa','rfc',
        'placa_anterior','placametro','numero_motor','uso','tipo_servicio','subservicio','nombre','razsoc','paterno',
        'materno','sexo','clave_vehicular','fecnac','notc','fecexped','ruta','estatus_tc','fecha_solicitud',
        'clave_movimiento','tramite','fila','vigencia','clave_clase','clase','clave_tipo','tipo','cilindros','personas',
        'clave_origen','origen','repuve','folio_legalizacion','fecha_alta','estatus_vehiculo','estatus_sistemas'];

    public $timestamps =false;

}
