<?php

namespace App\Models\Micros;

use Illuminate\Database\Eloquent\Model;

class Tramite extends Model
{

    protected $table = 'tramite';

    protected $fillable = ['fecha_tramite','linea_captura','importe_linea_captura','curp_solicitante',
        'identificacion_solicitante','constancia','costo_id','persona_id','vehiculo_id','modulo_id',
        'users_id','tipo_constancia_id','Observaciones','cuenta_generacion_pdf', 'placa_id'];

    protected $primaryKey = 'id_tramite';

    public $timestamps = false;

    public function documentos()
    {

        return $this->hasMany('App\Models\Micros\Documento');
    }

    public function propietario()
    {
        return $this->belongsTo('App\Models\Micros\Propietario', 'propietario_id');
    }

    public function vehiculo()
    {
        return $this->belongsTo('App\Models\Micros\Vehiculo', 'vehiculo_id');
    }

    public function costo(){
        return $this->belongsTo('App\Models\Micros\Catalogos\Cat_costo', 'costo_id');
    }
    public function operador(){
        return $this->belongsTo('App\Models\User', 'users_id');
    }


    public function personavehiculo(){

        return $this->belongsTo('App\Models\Micros\PersonaVehiculoModel', 'persona_vehiculo');


    }
    public function personaconcesion(){

        return $this->belongsTo('App\Models\Micros\PersonaConcesionModel', 'persona_concesion');


    }

}
