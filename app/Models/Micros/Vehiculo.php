<?php

namespace App\Models\Micros;

use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model
{

    protected $table = 'vehiculo';

    protected $fillable = ['id_vehiculo','serie_vehicular','fecha_alta','modelo','numero_cilindros','numero_motor',
        'origen_motor','capacidad_litros','capacidad_kwh','numero_personas','numero_puertas','folio_documento_legalizacion',
        'fecha_documento_legalizacion','fecha_factura','numero_factura','importe_factura','numero_repuve','distribuidora',
        'aseguradora','numero_poliza_seguro','tipo_servicio_id','uso_vehiculo_id','clase_tipo_vehiculo_id',
        'tipo_combustible_id','pais_id','estatus_id','clave_vehicular_id'];

    protected $primaryKey = 'id_vehiculo';

    /**
     * @param boolean $timestamps
     */
    public $timestamps = false;



    public function clasetipo(){

        return $this->belongsTo('App\Models\Micros\Catalogos\Cat_clase_tipo_vehiculo', 'clase_tipo_vehiculo_id');
        //return $this->belongsTo('App\Models\Micros\Catalogos\Cat_clase_tipo_vehiculo', 'clase_tipo_vehiculo_id');
    }
    public function tipo_servicio(){

        return $this->belongsTo('App\Models\Micros\Catalogos\Cat_Tipo_Servicio', 'tipo_servicio_id');
    }
    public function uso(){

        return $this->belongsTo('App\Models\Micros\Catalogos\Cat_uso_vehiculo',  'uso_vehiculo_id');
    }
    public function clave_vehicular(){

        return $this->belongsTo('App\Models\Micros\Catalogos\Cat_clave_vehicular', 'clave_vehicular_id');
    }

    public function tipo_combustible(){

        return $this->belongsTo('App\Models\Micros\Catalogos\CatCombustibleVehiculo' ,'tipo_combustible_id' );
    }


    public function servicio_vehiculo(){

        return $this->belongsTo('App\Models\Micros\Catalogos\CatServicioVehiculo');
    }


    public function nacionalidad(){

        return $this->belongsTo('App\Models\Micros\Catalogos\Cat_pais','pais_id');
    }

    public function tramites()
   {
       return $this->hasMany('App\Models\Micros\Tramite');
   }

    public function mini()
    {
        return $this->hasMany('App\Models\Micros\minimodel', 'serievh', 'serie_vehicular');
    }



}
