<?php

namespace App\Models\Micros;

use Illuminate\Database\Eloquent\Model;

class CandadoModel extends Model
{

    protected $table = 'candado';

    protected $fillable = ['serievh', 'placa', 'feccand', 'oficio_no', 'oficio_nob', 'usr_candado', 'usr_candadob', 'fecbaja', 'estatus'];


    public $timestamps = false;
}
