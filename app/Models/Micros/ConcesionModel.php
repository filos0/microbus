<?php

namespace App\Models\Micros;

use Illuminate\Database\Eloquent\Model;

class ConcesionModel extends Model
{
    protected $table = 'concesion';

    protected $primaryKey = 'id_concesion';

    protected $fillable = ['clave_concesion','modalidad','tipo','fecha_alta','fecha_expedicion', 'fecha_vencimiento',
        'encierro','ruta','fecbaja','modulo_id','tipo_concesion_id','vigencia','estatus'];

    public $timestamps = false;

    public function personaconcesion(){

        return $this->hasMany('App\Models\Micros\PlacaModel','concesion_id', 'id_concesion');
    }


}
