<?php

namespace App\Models\Micros;

use Illuminate\Database\Eloquent\Model;

class PlacaModel extends Model
{
    protected $table = 'placa';

    protected $primaryKey = 'id_placa';

    protected $fillable = ['placa','concesion_id','placa_metropolitana','clave_concesion'];

    public $timestamps = false;
/*
    public function personaconcesion(){

        return $this->hasMany('App\Models\Micros\PersonaConcesionModel'',''concesion_id'',' 'id_concesion');
    }
*/
}
