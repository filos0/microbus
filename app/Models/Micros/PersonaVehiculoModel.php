<?php

namespace App\Models\Micros;

use Illuminate\Database\Eloquent\Model;

class PersonaVehiculoModel extends Model
{
    protected $table = 'persona_vehiculo';

    protected $primaryKey = 'id_persona_vehiculo';

    protected $fillable = ['id_persona_vehiculo', 'persona_id', 'vehiculo_id', 'fecha_inicio', 'fecha_fin', 'status'];

    public $timestamps = false;

    public function persona(){

        return $this->belongsTo('App\Models\Micros\Persona', 'persona_id', 'id_persona');
    }

    public function vehiculo(){

        return $this->belongsTo('App\Models\Micros\Vehiculo', 'vehiculo_id', 'id_vehiculo');

    }

    public function tramite(){

        return $this->hasMany('App\Models\Micros\Tramite', 'persona_vehiculo');

    }
}
