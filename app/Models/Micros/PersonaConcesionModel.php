<?php

namespace App\Models\Micros;

use Illuminate\Database\Eloquent\Model;

class PersonaConcesionModel extends Model
{
    protected $table = 'persona_concesion';

    protected $primaryKey = 'id_persona_concesion';

    protected $fillable = ['id_persona_concesion', 'persona_id', 'concesion_id', 'fecha_inicio', 'fecha_fin', 'status'];

    public $timestamps = false;

    public function persona(){

        return $this->belongsTo('App\Models\Micros\Persona', 'persona_id', 'id_persona');
    }

    public function concesion(){

        return $this->belongsTo('App\Models\Micros\Concesion', 'concesion_id', 'id_concesion');

    }

    public function tramite(){

        return $this->hasMany('App\Models\Micros\Tramite', 'persona_concesion');

    }
}
