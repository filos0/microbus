<?php

namespace App\Models\Micros;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{

    protected $table = 'persona';

    protected $primaryKey = 'id_persona';

    protected $fillable = ['nombre_razon_social','primer_apellido','segundo_apellido','curp','rfc', 'tipo_identificacion_id',
        'pais_id','fecha_nacimiento','sexo','telefono','tipo_persona_id','calle','numero_exterior',
        'numero_interior','id_colonia'];

    public $timestamps = false;


    public function tramites()
    {
        return $this->hasMany('App\Models\Micros\Tramite');
    }

    public function personavehiculo()
    {
        return $this->hasMany('App\Models\Micros\PersonaVehiculoModel', 'id_persona_vehiculo');
    }

    public function mini()
    {
        return $this->hasMany('App\Models\Micros\minimodel','id');
    }

    public function mega()
    {
        return $this->hasOne('App\Models\Micros\Tramite', 'rfc', 'clave_unica');
    }

    public function colonia(){

        return $this->belongsTo('App\Models\Micros\Catalogos\Cat_colonias', 'id_colonia');
    }

    public function nacionalidad(){

        return $this->belongsTo('App\Models\Micros\Catalogos\Cat_pais', 'pais_id');

    }
    public function identificacion(){

        return $this->belongsTo('App\Models\Micros\Catalogos\Cat_tipo_identificacion', 'tipo_identificacion_id');

    }

    public function tipo_persona(){

        return $this->belongsTo('App\Models\Micros\Catalogos\Cat_Tipo_Persona','tipo_persona_id');

    }


}
