<?php

namespace App\Models\Micros\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_colonias extends Model
{

    protected $table = 'cat_colonia';

    protected $fillable = ['cp', 'colonia', 'delegacion','entidad_federativa'];

    protected $primaryKey = 'id_colonia';



    public function propietario()
    {

        return $this->hasMany('App\Models\Oklahoma\Propietario');
    }



}
