<?php

namespace App\Models\Micros\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_Tipo_Servicio extends Model
{

    protected $table = 'cat_tipo_servicio';

    protected $fillable = [ 'clave_tipo_servicio','tipo_servicio'];

    protected $primaryKey = 'id_tipo_servicio';

    public $timestamps = false;
}
