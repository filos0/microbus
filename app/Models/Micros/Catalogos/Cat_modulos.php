<?php

namespace App\Models\Micros\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_modulos extends Model
{

    protected $table = 'cat_modulo';

     public $timestamps = false;

    protected $fillable = ['clave_modulo', 'modulo', 'cat_estatus_id'];

    protected $primaryKey = 'id_modulo';


    public function placa(){

        return $this->hasMany('App\Models\Oklahoma\Placas');
    }

    public function user(){

        return $this->hasMany('App\Models\User');
    }

    public function lote_material(){

        return $this->hasMany('App\Models\Oklahoma\LoteMaterial');
    }
    public function documento(){

        return $this->hasMany('App\Models\Oklahoma\Documento');
    }


}
