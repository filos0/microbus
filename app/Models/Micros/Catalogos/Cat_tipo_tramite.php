<?php

namespace App\Models\Micros\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_tipo_tramite extends Model
{

    protected $table = 'cat_tipo_tramite';

    protected $fillable = ['id_tipo_tramite', 'tipo_tramite',];

    protected $primaryKey = 'id_tipo_tramite';

    public $timestamps = false;
}
