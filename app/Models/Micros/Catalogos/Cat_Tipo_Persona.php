<?php

namespace App\Models\Micros\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_Tipo_Persona extends Model
{

    protected $table = 'cat_tipo_persona';
    protected $primaryKey = 'id_tipo_persona';
    protected $fillable = [ 'clave_tipo_persona', 'tipo_persona'];

    protected $timestamp = false;

}
