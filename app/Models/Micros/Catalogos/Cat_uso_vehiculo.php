<?php

namespace App\Models\Micros\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_uso_vehiculo extends Model
{

    protected $table = 'cat_uso_vehiculo';

    protected $fillable = ['id_uso_vehiculo', 'clave_uso', 'uso_vehiculo'];

    protected $primaryKey = 'id_uso_vehiculo';


    public $timestamps = false;

    public function vehiculo()
    {
        return $this->hasMany('App\Models\Catalogos\Vehiculo');

    }
}
