<?php

namespace App\Models\Micros\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_Tipo_Identificacion extends Model
{

    protected $table = 'cat_tipo_identificacion';
    protected $primaryKey = 'id_tipo_identificacion';
    protected $fillable = [ 'clave_tipo_identificacion', 'tipo_identificacion'];

    protected $timestamp = false;

}
