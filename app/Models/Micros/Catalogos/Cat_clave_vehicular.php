<?php

namespace App\Models\Micros\Catalogos;

use Illuminate\Database\Eloquent\Model;

class Cat_clave_vehicular extends Model
{

    protected $table = 'cat_clave_vehicular';

    protected $fillable = ['clave_vehicular', 'digito', 'marca','linea','version'];

    protected $primaryKey = 'id_cat_clave_vehicular';

    public $timestamps = false;

    public function vehiculo()
    {
        return $this->hasMany('App\Models\Oklahoma\Catalogos\Vehiculo');

    }

}
