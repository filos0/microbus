<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 06/03/2018
 * Time: 02:23 PM
 */

namespace App\Repositories;


class PersonaVehiculoRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Micros\PersonaVehiculoModel';
    }

    function alta($persona, $vehiculo){

        $p_v = $this->model->create(array(
            'persona_id' => $persona,
            'vehiculo_id' => $vehiculo,
            'fecha_inicio' => date('Y-d-m'),
            'status' => 1
            ));

        return $p_v->id_persona_vehiculo;

    }

    function baja($persona_vehiculo){
        $this->model->where('id_persona_vehiculo', $persona_vehiculo)->update(array(
            'fecha_fin' => date('Y-d-m'),
            'estatus' => 2));
    }

}
