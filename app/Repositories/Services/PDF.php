<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 07/02/2018
 * Time: 05:29 PM
 */

namespace App\Repositories\Services;
use Dompdf\Dompdf;
use Dompdf\Options;

class PDF
{

    public function Vale_Placa($placa, $modulo, $serie_vehicular, $folio_tramite, $operador)
    {
        $view = \View::make('PDF/ValeTC', compact('placa','modulo','serie_vehicular','folio_tramite','operador'))->render();
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        dd($pdf);
        $numerito = random_int(1, 5000);
        $output = $pdf->output();
        $nombre_pdf = $numerito. '.pdf';
        file_put_contents($nombre_pdf, $output);
        return $nombre_pdf;
    }
}