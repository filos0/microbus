<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 07/02/2018
 * Time: 05:28 PM
 */

namespace App\Repositories\Services;
use GuzzleHttp\Client;


class Lineas_Caprura
{
    protected $client;
    public function __construct()
    {
        $this->client= new Client([
            'base_uri' => 'http://128.222.200.178:8113',
            'timeout' => 3.0,
        ]);

    }

    public function consultar($linea)
    {
        $response = $this->client->request('POST', '/ConsultaLC',['json' => ["linea" => $linea]]);

        return json_decode($response->getBody()->getContents());

    }
}