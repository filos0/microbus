<?php

namespace App\Repositories\Services;

use GuzzleHttp\Client;
use App\Repositories\VehiculosRepositorio as Vehiculo;
use App\Repositories\PropietarioRepositorio as Propietario;
use App\Repositories\MiniatablaRerpositorio as Mini;
use App\Repositories\Catalogos\CatClaveVehicularRepositorio as ClaveVehicular;

use Illuminate\Support\Facades\DB;

class Informix
{
    protected $client;
    public function __construct(Vehiculo $vehiculo , Propietario $propietario, Mini $mini, ClaveVehicular $clave)
    {

        $this->Vehiculo = $vehiculo;
        $this->Propietario = $propietario;
        $this->mini = $mini;
        $this->ClaveVehicular =$clave;

        $this->client= new Client([
            'base_uri' => 'http://128.222.200.41:8888',
            'timeout' => 15.0,
        ]);

    }

    private function token(){
        $response = $this->client->request('POST', '/smeargle/altas/tenencia/token');

        return json_decode($response->getBody()->getContents())->token;
    }


    public function consultar($placa_serie, $parte ='todo')
    {


        try {
            $placa_serie = trim(strtoupper($placa_serie));
            $response = $this->client->request('POST', '/smeargle/consulta/'.$parte,['json' => ["consulta" => $placa_serie]]);
            $status = $response->getStatusCode();


            if($status == 200){
                return json_decode($response->getBody()->getContents());

            }
            else{

                return 500;
            }

        } catch (\GuzzleHttp\Exception\ConnectException $e) {

            return 500;

        }




    }

    public function seg_fase($serie)
    {
        $response = $this->client->request('POST', '/smeargle/consulta/lineas_captura',['json' => ["serievh" => $serie]]);

        return json_decode($response->getBody()->getContents());

    }

    public function existe_lc($lc)
    {
        $response = $this->client->request('POST', '/smeargle/comprueba/linea_captura',['json' => ["linea" => $lc]]);

        return json_decode($response->getBody()->getContents());

    }


    public function comprobar_vehiculo($placa_serie)
    {
        try {
            DB::BeginTransaction();
            $placa_serie = trim(strtoupper($placa_serie));
            $todo = $this->consultar($placa_serie);
            //dd($todo->vehiculo[0]->fecha_documento_legalizacion ?? null);
            if (isset($todo->vehiculo[0])) {
                if (!$this->Vehiculo->findBy('serie_vehicular', $todo->vehiculo[0]->serie_vehicular)) {
                    $todo->vehiculo[0]->clave_vehicular_id = $this->ClaveVehicular->findBy('clave_vehicular', $todo->vehiculo[0]->clave_vehicular_id)->id_cat_clave_vehicular;
                    $this->Vehiculo->create(array(
                        'serie_vehicular' => $todo->vehiculo[0]->serie_vehicular,
                        'modelo' => $todo->vehiculo[0]->modelo,
                        'numero_cilindros' => $todo->vehiculo[0]->numero_cilindros,
                        'numero_motor' => $todo->vehiculo[0]->numero_motor,
                        'origen_motor' => $todo->vehiculo[0]->origen_motor,
                        'numero_personas' => $todo->vehiculo[0]->numero_personas,
                        'capacidad_kwh' => $todo->vehiculo[0]->capacidad_kwh,
                        'capacidad_litros' => $todo->vehiculo[0]->capacidad_litros,
                        'fecha_documento_legalizacion' => $todo->vehiculo[0]->fecha_documento_legalizacion,
                        'pais_id' => $todo->vehiculo[0]->pais_id,
                        'uso_vehiculo_id' => $todo->vehiculo[0]->uso_vehiculo_id,
                        'clase_tipo_vehiculo_id' => $todo->vehiculo[0]->clase_tipo_vehiculo_id,
                        'tipo_combustible_id' => $todo->vehiculo[0]->tipo_combustible_id,
                        'clave_vehicular_id' => $todo->vehiculo[0]->clave_vehicular_id,
                        'fecha_alta' => $todo->vehiculo[0]->fecha_alta,
                        'folio_documento_legalizacion' => $todo->vehiculo[0]->folio_documento_legalizacion == '' ? null : $todo->vehiculo[0]->folio_documento_legalizacion,
                        'fecha_factura' => $todo->vehiculo[0]->fecha_factura,
                        'numero_factura' => $todo->vehiculo[0]->numero_factura,
                        'importe_factura' => $todo->vehiculo[0]->importe_factura,
                        'numero_repuve' => $todo->vehiculo[0]->numero_repuve,
                        'aseguradora' => $todo->vehiculo[0]->aseguradora,
                        'numero_poliza_seguro' => $todo->vehiculo[0]->numero_poliza_seguro,
                        'distribuidora' => $todo->vehiculo[0]->distribuidora,
                        'numero_puertas' => $todo->vehiculo[0]->numero_puertas,
                        'tipo_servicio_id' => $todo->vehiculo[0]->tipo_servicio_id,
                        'estatus_id' => $todo->vehiculo[0]->estatus_id));


                    if (isset($todo->propietario[0])) {
                        foreach ($todo->propietario as $propietario) {
                            if (!$this->Propietario->findBy('clave_unica', $propietario->clave_unica)) {
                                $this->Propietario->create(array(
                                    'nombre_razon_social' => $propietario->nombre_razon_social,
                                    'primer_apellido' => $propietario->primer_apellido,
                                    'segundo_apellido' => $propietario->primer_apellido,
                                    'clave_unica' => $propietario->clave_unica,
                                    'tipo_identificacion_id' => $propietario->tipo_identificacion_id,
                                    'pais_id' => $propietario->pais_id,
                                    'fecha_nacimiento' => $propietario->fecha_nacimiento,
                                    'sexo' => $propietario->sexo,
                                    'telefono' => $propietario->telefono,
                                    'tipo_propietario_id' => $propietario->tipo_propietario_id,
                                    //'calle' => $propietario->calle,
                                    //'numero_exterior' => $propietario->numero_exterior,
                                    //'numero_interior' => $propietario->numeero_interior,
                                    'id_colonia' => $propietario->id_colonia));

                            }
                        }
                        if (isset($todo->tramite[0])) {

                            foreach ($todo->tramite as $tramite) {
                                $tramite->propietario = $this->Propietario->findBy('clave_unica', $tramite->propietario)->id_propietario;
                                $this->mini->create(array(
                                    "tipoplaca",
                                    "folioteso" => $tramite->folioteso,
                                    "notc" => $tramite->notc,
                                    "propietario" => $tramite->propietario,
                                    "modulo_t" => $tramite->modulo_t,
                                    "placaant" => $tramite->placaant,
                                    "revisor" => $tramite->revisor,
                                    "movimiento_t" => $tramite->movimiento_t,
                                    "estatustc" => $tramite->estatustc,
                                    "operador_mov" => $tramite->operador_mov,
                                    "fecha_expedicion" => $tramite->fecha_expedicion,
                                    "imptenencia" => $tramite->imptenencia,
                                    "placa" => $tramite->placa,
                                    "serievh" => $tramite->serievh,
                                    "estatusg" => $tramite->estatusg));
                            }
                        }

                    }


                }
                DB::commit();
                return 'ok';

            } else {
                return 'no existe';
            }
        } catch (\Exception $e) {

            DB::rollBack();
            \Log::error('smeargle' . $e);
            return 'error';
        }
    }

    public function lc_altas($lc){
        $response = $this->client->request('POST', '/smeargle/altas/tenencia/datos',['json' => ["linea" => $lc , "token" => $this->token()]]);
        return json_decode($response->getBody()->getContents())->datos;

    }
}