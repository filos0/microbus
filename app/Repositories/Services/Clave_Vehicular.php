<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 07/02/2018
 * Time: 05:30 PM
 */

namespace App\Repositories\Services;

use GuzzleHttp\Client;

class Clave_Vehicular
{
    protected $client;
    public function __construct()
    {
        $this->client= new Client([
            'base_uri' => 'http://128.222.200.178:8252/',
            'timeout' => 3.0,
        ]);

    }

    public function consultar($clave)
    {


        try {
            $response = $this->client->request('POST', '/ClaveVehicularFinanzas',['json' => ["numero" => $clave]]);
            $status = $response->getStatusCode();


            if($status == 200){
                return json_decode($response->getBody()->getContents());

            }
            else{

                return 500;
            }

        } catch (\GuzzleHttp\Exception\ConnectException $e) {

            return 500;

        }





    }
}