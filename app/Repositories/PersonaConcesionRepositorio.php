<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 06/03/2018
 * Time: 02:23 PM
 */

namespace App\Repositories;


class PersonaConcesionRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Micros\PersonaConcesionModel';
    }

    function alta($persona, $concesion){

        $p_v = $this->model->create(array(
            'persona_id' => $persona,
            'concesion_id' => $concesion,
            'fecha_inicio' => date('Y-d-m'),
            'status' => 1
        ));

        return $p_v->id_persona_concesion;

    }

    function baja($persona_concesion){
        $this->model->where('id_persona_concesion', $persona_concesion)->update(array(
            'fecha_fin' => date('Y-d-m'),
            'estatus' => 2));
    }

    function activas($concesion_id){

        return $this->model->where('status', 1)->where('concesion_id', $concesion_id)->get();

    }



}