<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 06/03/2018
 * Time: 02:22 PM
 */

namespace App\Repositories;


class ConcesionRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Micros\ConcesionModel';
    }















    /**
     * @param $concesion
     * @return bool
     *
     */



    public function concecionesactivas($concesion){

        $c = $this->model->where('placa', $concesion)->where('estatus', 'A')->whereNotNull('status_actual')->where('status_actual', 1)
            ->orWhere('placa', $concesion)->where('estatus', 'A')->whereNull('status_actual')->get();
  //dd($c);
        if (!isset($c[0])){

            return false;

        }

        else{

            return $c;

        }
    }

    public function correccionconcecionesactivas($placa_concesion, $id_concesion_buena){

        $c = $this->model->where('placa', $placa_concesion)->where('id_concesion', '<>', $id_concesion_buena)
            ->update(array('fecbaja' => date('Y-m-d'), 'status_actual' => 2));

        $c2 = $this->model->where('id_concesion', $id_concesion_buena)->update(array('status_actual' => 1));

            return $c2;

    }

    /***
     *                       Solo para impresion de TC
     * @param $concesion
     * @return bool
     *
     */
    public function concecion($concesion){

        $c = $this->model->where('clave_concesion', $concesion)->where('status_actual', 1)
            ->Orwhere('placa', $concesion)->where('status_actual', 1)->get();
        //dd($c);
        if ((count($c) > 1 )||( count($c) == 0)){

            return false;

        }
        else{

            return $c[0];

        }
    }


}