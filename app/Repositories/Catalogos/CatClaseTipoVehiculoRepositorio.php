<?php
/**
 * Created by PhpStorm.
 * User: 4naka
 * Date: 24/10/2016
 * Time: 02:02 PM
 */

namespace App\Repositories\Catalogos;


use App\Repositories\Repositorio;

class CatClaseTipoVehiculoRepositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Micros\Catalogos\Cat_clase_tipo_vehiculo';
    }


    public function findAllby($attribute, $value) {
        $faw = $this->model->where($attribute, '=', $value)->get();
        return $faw;
    }
    public function clases_vehiculo(){
        $clases =  $this->model->distinct()->get(['clave_clase','clase']);
        return $clases;
    }
    public function tipo_vehiculo($clave_clase){
        $clases =  $this->model->where("clave_clase",$clave_clase)->get();
        return $clases;
    }

}