<?php
/**
 * Created by PhpStorm.
 * User: Hp
 * Date: 29/03/2017
 * Time: 02:03 PM
 */

namespace App\Repositories;


class MegamicroRerpositorio extends Repositorio
{
    function model()
    {
        return 'App\Models\Micros\MegamicroModel';
    }

    public function buscarporserie($serie)
    {
        $F = $this->model->where('serie_vehicular', '=', $serie)->whereNotNull('estatus_vehiculo')->orderBy('fecexped', 'desc')->get();
        if (isset($F[0])) {

            if ($F[0]->estatus_sistemas == 1)
                return 1;
            else {

                if (count($F) > 1)
                    return 'Error';
                else
                    return $F[0];
            }
        }
        else
            return 0;

    }

    public function buscarporplaca($placa)
    {
        $F = $this->model->where('placa', $placa)->whereNotNull('estatus_vehiculo')->orderBy('fecexped', 'desc')->get();
        if (isset($F[0])) {

            if ($F[0]->estatus_sistemas == 1)
                return 1;
            else {

                if (count($F) > 1)
                    return 'Error';
                else
                    return $F[0];
            }
        }
        else
            return 0;

    }


    public function placaanterior($placa)
    {
       return $F = $this->model->where('placa', $placa)->whereNotNull('estatus_vehiculo')->where('estatus_sistemas', 1)->orderBy('fecexped', 'desc')->first();

    }

    public function actualizarestatus($placa, $serie){

        return $F = $this->model->where('placa', $placa)->where('serie_vehicular', $serie)->update(array('estatus_sistemas' => 1));

    }





}


