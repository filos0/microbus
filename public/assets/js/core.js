$(document).ready(function () {

    $(document).on('focus', ':input', function () {
        $(this).attr('autocomplete', 'off');
    });

    var Max = moment().format('YYYY-MM-DD');
    var Min = moment().format('1900-01-01');

    $('input[type ="date"]').each(function () {
        $(this).prop("max", Max).prop("min", Min);
    }).keyup('change', function () {

        var fecha = $(this).val();


        var res_fecha = validar_Fechas(fecha);

        if (res_fecha == true) {
            $(this).css("background-color", "#FFFFFF");// normal
        }

        else if (res_fecha == 'futuro') {
            $(this).val('');
            $(this).focus();
            $(this).css("background-color", "#FFFFFF");// normal
            swal({
                title: "",
                text: "No puedes seleccionar una fecha posterior de hoy.",
                confirmButtonText: "Aceptar",
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(function () {
                $(this).focus();
            });

        }
        else {

            $(this).css("background-color", "#FF0000"); //rojo

        }

    });  // Valida Fechas

    $(':input').addClass('text-uppercase')
        .focus(function () {
            $(this).css("box-shadow", "0 0 0 500px rgba(0, 187, 236, 0.4) inset");
        })
        .blur(function () {
            $(this).css("box-shadow", "none");
        });

    $('.mayusculas').keyup(function () {
        cadena = $(this).val().toString().toUpperCase();
        cadena = cadena.match(/[0-9a-zA-ZñÑ\s.,-]+/);
        $(this).val(cadena);

    });
    $('.placa_text').keyup(function () {
        cadena = $(this).val().toString().toUpperCase();
        cadena = cadena.match(/[0-9a-zA-ZñÑ]+/);
        $(this).val(cadena);

    });
    $('.alfanumerico').keyup(function () {
        cadena = $(this).val().toString();
        cadena = cadena.match(/[0-9a-zA-ZñÑ@\s.,-]+/);
        $(this).val(cadena);
    });
    $('.folio_text').keyup(function () {
        cadena = $(this).val().toString();
        cadena = cadena.match(/[0-9a-zA-ZñÑ]+/);
        $(this).val(cadena);
    });
    $('.numeros').keyup(function () {
        cadena = $(this).val().toString();
        cadena = cadena.match(/[0-9]+/);
        $(this).val(cadena);

    });


    $("[name = clave_vehicular]").on('change', function () {
        $('[name = marca]').val('');
        $('[name = linea]').val('');
        $('[name = version]').val('');
        var clave = $("[name = clave_vehicular]").val();

        var expreg = new RegExp('[0-9A-Z]{7}');

        if (!expreg.test(clave)) {

            swal({
                title: "",
                type: "error",
                text: "El formato de la clave vehícular es incorrecto.",
                confirmButtonText: "Aceptar",
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(function () {
                $("[name = clave_vehicular]").focus();
            });

            $('[name = marca]').val('');
            $('[name = linea]').val('');
            $('[name = version]').val('');


        }
        else {
            $("[name = clave_vehicular]").css("border-color", "rgb(221, 221, 221)");
            $.ajax({
                url: serie_vehicular_db,
                type: "post",
                data: {"clave_vehicular": clave},
                success: function (data) {
                    console.log(data);
                    var marca = data['marca'];
                    var linea = data['linea'];
                    var version = data ['version'];


                    if (data === 0) {
                        swal({
                            title: "",
                            type: "error",
                            text: "No se encontró registro de la Clave Vehicular " + clave + ".",
                            confirmButtonText: "Aceptar",
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        }).then(function () {
                            $("[name = clave_vehicular]").focus();
                        });


                    }
                    if (data === 1) {
                        $("[name = clave_vehicular]").val('');
                        //$("[name = clave_vehicular]").val(clave).change();


                    }
                    else {

                        $('[name = marca]').val(data.marca);
                        $('[name = linea]').val(data.linea);
                        $('[name = version]').val(data.version);
                        $('[name = uso_vehiculo]').focus();
                    }


                },
                statusCode: {
                    500: function () {
                        swal({
                            title: "",
                            type: "error",
                            text: "El servidor de FINANZAS no responde. Contacta a Sistemas",
                            confirmButtonText: "Aceptar",
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        }).then(function () {
                            $("[name = clave_vehicular]").focus();
                        });
                    }
                }

            });


        }

    }); ///Checa  BD y luego WebService de Finanzas

    $('[name = clase_vehiculo]').on('change', function () {

        var clase_id = $(' [name = clase_vehiculo ]').val();

        $.ajax({
            url: tipovehiculo,
            type: "post",
            data: {"clave_clase": clase_id},
            success: function (data) {
                //console.log(data);
                $('[name = clase_tipo_vehiculo_id]').empty();
                $('[name = clase_tipo_vehiculo_id]').append('<option class="form-control"></option>');
                $.each(data, function (index, value) {
                    $('[name = clase_tipo_vehiculo_id]').append('<option class="form-control" value="' + value.id_cat_clase_tipo_vehiculo + '">' + value.tipo + '</option>');
                });
            }
        });

    });

    $('[name = combustible]').on('change', function () {
        var id = $('[name = combustible ]').val();
        if (id == 5) { //Electrico
            $('[name = bat ]').val('').prop("disabled", false).prop("required", true).css("border-color", "red");
            $('[name = lit ]').val('').prop("disabled", true).prop("required", false).css("border-color", "rgb(221, 221, 221)");
            $('[name = cilindros ]').val('').prop("disabled", true).prop("required", false).css("border-color", "rgb(221, 221, 221)");
            $(this).css("border-color", "rgb(221, 221, 221)");

        }
        else if (id == 7) { //Manual
            $('[name = bat ]').val('').prop("disabled", true);
            $('[name = lit ]').val('').prop("disabled", true);
            $('[name = cilindros ]').val('').prop("disabled", true);
            $(this).css("border-color", "rgb(221, 221, 221)");
        }
        else if (id == 8) { //No usa
            $('[name = bat ]').val('').prop("disabled", true);
            $('[name = lit ]').val('').prop("disabled", true);
            $('[name = cilindros ]').val('').prop("disabled", true);
            $(this).css("border-color", "rgb(221, 221, 221)");
        }
        else if (id == 9) { //hibrido
            $('[name = bat ]').val('').prop("disabled", false).prop("required", true).css("border-color", "red");
            $('[name = lit ]').val('').prop("disabled", false).prop("required", true).css("border-color", "red");
            $('[name = cilindros ]').val('').prop("disabled", false).prop("required", true).css("border-color", "red");
            $(this).css("border-color", "rgb(221, 221, 221)");
        }
        else {
            $('[name = bat ]').val('').prop("disabled", true);
            $('[name = lit ]').val('').prop("disabled", false);
            $('[name = cilindros ]').val('').prop("disabled", false);
            $(this).css("border-color", "rgb(221, 221, 221)");
        }

    });
    $("[name = repuve]").on('change', function () {
        var repuve = $(this).val();
        if (repuve != '') {
            if (repuve.length < 8) {
                swal({
                    title: "",
                    text: "El número de REPUVE no puede tener menos de 8 digitos.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = repuve]").val('').focus();
                });

            }
            else if (repuve < 0) {
                return false
            }
        }
    });
    $("[name = pasajeros]").keyup(function () {
        var pasajeros = $(this).val();
        if (pasajeros != '') {
            if (pasajeros > 90) {
                swal({
                    title: "",
                    text: "No puede transportar más de 90 pasajeros.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = pasajeros]").val('').focus();
                });

            }
            if (pasajeros < 5) {
                swal({
                    title: "",
                    text: "No puede transportar menos de 5 pasajero.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = pasajeros]").val('').focus();
                });
            }
        }
    });
    $("[name = cilindros]").keyup(function () {
        var cilindros = $(this).val();
        if (cilindros != '') {
            if (cilindros > 12) {
                swal({
                    title: "",
                    text: "No puede tener más de 12 cilindros.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = cilindros]").val('').focus();
                });

            }
            if (cilindros < 1) {
                swal({
                    title: "",
                    text: "No puede tener menos de 1 cilindro.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = cilindros]").val('').focus();
                });
            }
        }
    });
    $("[name = puertas]").keyup(function () {
        var puertas = $(this).val();

        if (puertas != '') {
            if (puertas > 8) {
                swal({
                    title: "",
                    text: "No puede tener más de 8 puertas.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = puertas]").val('').focus();
                });
            }
            if (puertas < 1) {
                swal({
                    title: "",
                    text: "No puede tener 0 puertas.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = puertas]").val('').focus();
                });
            }
        }

    });
    $("[name = lit]").keyup(function () {
        var lit = $(this).val();

        if (lit != '') {
            if (lit > 1200) {
                swal({
                    title: "",
                    text: "No puede tener más de 1200 lit.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = lit]").val('').focus();
                });

            }
            if (lit < 1) {
                swal({
                    title: "",
                    text: "No puede tener menos de 1 lit.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false

                }).then(function () {
                    $("[name = lit]").val('').focus();
                });
            }
        }

    });

    $('#procedencia').on('change', function () {

        if ($("[name = procedencia]").val() == 1) {
            //nacional
            $('[name = pais]').val('').attr("required", false).prop("disabled", true);
            $('[name = fecha_legalizacion]').val('').attr("required", false).prop("disabled", true);
            $('[name = folio_legalizacion]').val('').attr("required", false).prop("disabled", true);
            $('[id = vehiculo_extranjero]').prop("hidden", true);

        }
        else {

            $('[name = fecha_legalizacion]').val('').prop("disabled", false).attr("required", true);
            $('[name = folio_legalizacion]').val('').prop("disabled", false).attr("required", true);
            $('[name = pais]').val('').prop("disabled", false).attr("required", true);
            $('[id = vehiculo_extranjero]').prop("hidden", false);


        }
    });


    $("[name = cp]").on('change', function () {
        $('[name = colonia]').empty();
        $('[name = delegacion]').empty();
        var serie = $("[name = cp]").val();
        var expreg = new RegExp('[0-9]{5}');

        if (!expreg.test(serie)) {

            $("[name = delegacion]").prop("readonly", true);
            $("[name = colonia]").prop("disabled", true);
            $("[name = delegacion]").val('');
            $("[name = colonia]").val('');

            swal({
                title: "",
                text: "El Código Postal es incorrecto.",
                confirmButtonText: "Aceptar",
                allowEscapeKey: false,
                allowOutsideClick: false
            }).then(function () {
                $("[name = cp]").val('').focus();
            });

        }
        else {
            $.ajax({
                url: cp,
                type: "post",
                data: {"cp": serie},
                success: function (data) {
                    if (data.length == 0) {

                        swal({
                            title: "",
                            text: "No hay Colonias ni Delegaciones en esté Código Postal",
                            confirmButtonText: "Aceptar",
                            allowEscapeKey: false,
                            allowOutsideClick: false
                        }).then(function () {
                            $("[name = cp]").val('').focus();
                        });


                    }
                    else {
                        $.each(data, function (key, value) {
                            $('[name = delegacion]').val(value.delegacion);
                            $('[name = delegacion]').prop("readonly", true);
                            $('[name = colonia]').prop("disabled", false);
                            $('[name = colonia]').append('<option class="form-control" value="' + value.id_colonia + '">' + value.colonia + '</option>');
                            $('[name = colonia]').val(value.colonia);
                        });
                        $('[name = delegacion]').css("border-color", "rgb(221, 221, 221)").prop("required", false);
                        $('[name = cp]').css("border-color", "rgb(221, 221, 221)").prop("required", true);
                        $('[name = colonia]').css("border-color", "red").prop("required", true).focus();
                        //if (top.location.pathname === '/Alta/Nuevo/' || top.location.pathname === '/Alta/Usado/') {
                        //  $('[name = colonia]').append('<option value="0000" class="form-control" style="background-color: #ff190c">Agregar Colonia</option>');

                        //  }
                    }
                }

            });


        }

    });
    $("[name = valor_factura]").keyup(function () {
        var valActual = $(this).val();
        var nuevoValor = conComas(valActual);
        $(this).val(nuevoValor);
    });
    $("[name = curp_solicitante ]").on('change', function () {

        var curp = $(this).val();

        if (curp != null) {

            curp = curp.toUpperCase();

            var expreg = new RegExp('^[A-Z]{4}[0-9]{6}[HM]{1}(AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE)[A-Z]{3}[0-9]{2}|[A-Z]{4}[0-9]{6}[A-Z0-9]{3}$');

            if (!expreg.test(curp)) {
                swal({
                    title: "",
                    text: "CURP o RFC es incorrecto.",
                    confirmButtonText: "Aceptar",
                    allowEscapeKey: false,
                    allowOutsideClick: false
                }).then(function () {


                    $("[name = folio_doc_solicitante]").prop("readonly", true);
                    $("[name = doc_solicitante]").prop("disabled", true);
                    $("[name = curp_solicitante]").val('').focus();

                });

            }
            else {


                $("[name = folio_doc_solicitante]").prop("readonly", false);
                $("[name = doc_solicitante]").prop("disabled", false).focus();

            }
        }
    });
///////////////////////////////////////////////////////////////////////////////////////////////////////////
    $(' [name = folio_doc_solicitante ]').on('keyup', function () {
        var folio = $(this).val();

        if (folio != '') {
            if (folio.length > 7) {
                $('[name = datos]').prop("disabled", false);

            }
            else {
                $('[name = datos]').prop("disabled", true);
            }

        }

    });

});

$(' [name = folio_doc_solicitante ]').on('change', function () {
    var folio = $(this).val();

    if (folio.length > 8) {
        $('[name = datos]').prop("disabled", false);

    }

    else {

        swal({
            title: "",
            text: "El folio debe tener más de 8 caracteres.",
            confirmButtonText: "Aceptar",
            allowEscapeKey: false,
            allowOutsideClick: false
        }).then(function () {
            $('[name = folio_doc_solicitante ]').focus();
        });


    }


});


function conComas(valor) {

    var nums = new Array();
    var simb = ","; //Éste es el separador
    valor = valor.toString();
    valor = valor.replace(/\D/g, "");   //Ésta expresión regular solo permitira ingresar números
    nums = valor.split(""); //Se vacia el valor en un arreglo
    var long = nums.length - 1; // Se saca la longitud del arreglo
    var patron = 3; //Indica cada cuanto se ponen las comas
    var prox = 2; // Indica en que lugar se debe insertar la siguiente coma
    var res = "";

    while (long > prox) {
        nums.splice((long - prox), 0, simb); //Se agrega la coma
        prox += patron; //Se incrementa la posición próxima para colocar la coma
    }

    for (var i = 0; i <= nums.length - 1; i++) {
        res += nums[i]; //Se crea la nueva cadena para devolver el valor formateado
    }

    return res;
}


function validar_Fechas(fecha) {

    var rxDatePattern = /^([1-2][0-9]{3})(\-)(\d{2})(\-)(\d{2})$/;

    var fecha_comp = fecha.match(rxDatePattern);

    if (fecha_comp == null)
        return false;

    var dtArray = fecha.split('-');

    Mes = dtArray[1];
    Dia = dtArray[2];
    Ano = dtArray[0];


    var fecha_hoy = moment();
    var fecha_asig = moment(fecha);


    if (Ano < 1900)
        return false;
    if (Mes < 1 || Mes > 12)
        return false;
    else if (Dia < 1 || Dia > 31)
        return false;
    else if ((Mes == 4 || Mes == 6 || Mes == 9 || Mes == 11) && Dia == 31)
        return false;
    else if (Mes == 2) {
        var isleap = (Ano % 4 == 0 && (Ano % 100 != 0 || Ano % 400 == 0));
        if (Dia > 29 || (Dia == 29 && !isleap))
            return false;
    }

    if (fecha_asig.diff(fecha_hoy) >= 0) {
        return 'futuro';
    }

    return true;


}


function fecha_actual() {
    moment.locale('es');
    var fecha = moment().format('L') + " " + moment().format('LTS');
    $('#hora').text(fecha);
}

setInterval(fecha_actual, 1000);