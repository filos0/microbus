<?php

Auth::routes();


Route::get('/', 'HomeController@index');
Route::get('/modelos', 'HomeController@modelos');
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');


Route::get('/Linea_Captura/Cambio', 'LineaCapturaController@linea_captura_cambio_unidad');
Route::get('/Linea_Captura/Baja', 'LineaCapturaController@linea_captura_baja');
Route::get('/Linea_Captura', 'LineaCapturaController@linea_captura');


Route::post('/Linea_Captura/Comprobar/Cambio', 'LineaCapturaController@Comprobar_Cambio_Uunidad');
Route::post('/Procesar/Alta', 'AltasController@Dardealta');


Route::post('/cambiar/unidad', 'CambioUnidadController@CambiarUnidad');


Route::post('/Correcion/Datos', 'LineaCapturaController@comprobar_correcion');
Route::post('/Comprobar/Baja', 'LineaCapturaController@comprobar_Baja');
Route::post('/Reposicion/TC', 'LineaCapturaController@comprobar_reposicion');

Route::get('buscar/', 'ImpresiontcController@index');
Route::get('/Validar/LineaPago', 'ImpresiontcController@index_reposicion');



Route::post('/buscar/placa', 'ImpresiontcController@placa');
Route::post('Reposicion/Tarjeta/Validar', 'ImpresiontcController@reposicion');



Route::post('/API/ClaveVehicular','ApiController@getClaveVehicular');
Route::post('/API/concesionactiva','ApiController@listaconcesion');
Route::post('/API/concesion','ApiController@concesion');
Route::post('/API/PDF/Confirmar_Datos','ApiController@PDF_Confirmar_Datos');
Route::post('/API/Validar_Folio_TC/', 'ApiController@validar_folio_tarjeta');
Route::post('/API/TC/', 'ApiController@TC');
Route::post('/API/TipoVehiculo','ApiController@tipo_vehiculo');
Route::post('/API/PDF/Baja','ApiController@PDF_Baja');
Route::post('/API/PDF/Confirmar_Datos','ApiController@PDF_Confirmar_Datos');



Route::get('/Baja/Robo', 'BajaController@baja_robo');
Route::post('/Baja/Vehiculo', 'BajaController@dardebaja');


Route::post('/Actualizar/Datos', 'CoreccionController@actualizar');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
